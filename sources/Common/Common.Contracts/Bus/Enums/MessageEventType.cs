﻿namespace Common.Contracts.Bus.Enums
{
    /// <summary>
    /// Тип сообщения для накопителя.
    /// </summary>
    public enum MessageEventType
    {
        /// <summary>
        /// В случае удачного выполнения.
        /// </summary>
        OnSuccess = 1,

        /// <summary>
        /// В случае ошибки.
        /// </summary>
        OnError = 2,

        /// <summary>
        /// В любом случае.
        /// </summary>
        Always = 3
    }
}
