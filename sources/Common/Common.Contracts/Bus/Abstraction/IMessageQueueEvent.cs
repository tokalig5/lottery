﻿namespace Common.Contracts.Bus.Abstraction
{
    /// <summary>
    /// Интерфейс события для шины данных (рассылка события на подписанные очереди)
    /// </summary>
    public interface IMessageQueueEvent : IMessageQueueBaseMessage
    {
    }
}
