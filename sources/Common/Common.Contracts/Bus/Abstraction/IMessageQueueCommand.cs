﻿namespace Common.Contracts.Bus.Abstraction
{
    /// <summary>
    /// Интерфейс команды для шины данных (отправка команды в конкретную очередь)
    /// </summary>
    public interface IMessageQueueCommand : IMessageQueueBaseMessage
    {
    }
}
