﻿using System;

namespace Common.Contracts.DataAccess
{
    /// <summary>
    /// Базовый класс сущности БД.
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Идентификатор сущности БД.
        /// </summary>
        public Guid Id { get; set; }
    }
}
