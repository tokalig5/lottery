﻿using System;

namespace Common.Contracts.WebApi.Models
{
    /// <summary>
    /// Модель ошибки работы публичного API.
    /// </summary>
    public class PublicApiError
    {
        /// <summary>
        /// Идентификатор ошибки (трассировки запроса).
        /// </summary>
        public Guid TraceId { get; }

        /// <summary>
        /// Сообщение об ошибке.
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="PublicApiError"/>.
        /// </summary>
        public PublicApiError(Guid traceId, string message)
        {
            TraceId = traceId;
            Message = message;
        }
    }
}
