using System;
using System.Net;

namespace Common.Contracts.WebApi.Models;

/// <summary>
/// Модель ошибки работы API.
/// </summary>
[Serializable]
public class ApiError
{
    /// <summary>
    /// Статус-код ошибки.
    /// </summary>
    public HttpStatusCode StatusCode { get; }
    
    /// <summary>
    /// Сообщение об ошибке.
    /// </summary>
    public string Message { get; }
    
    /// <summary>
    /// Исключение.
    /// </summary>
    public Exception Exception { get; }

    /// <summary>
    /// Инициализация экземпляра <see cref="ApiError"/>.
    /// </summary>
    public ApiError(HttpStatusCode statusCode, string message, Exception exception)
    {
        StatusCode = statusCode;
        Message = message;
        Exception = exception;
    }
}