using System;

namespace Common.Contracts.WebApi.Exceptions;

/// <summary>
/// ����������, ����������� � ������ ������������� ��������� ��������.
/// ��������, ������������� ���������� �������� ��� ��������� ��-�� �� ������������������ ��������� �� ������ ������� ����������.
/// </summary>
[Serializable]
public class UnprocessableEntityException : Exception
{
    private const string DefaultMessage = "�������� �� ����� ���� ����������. {0}";

    /// <summary>
    /// ������������� ���������� <see cref="UnprocessableEntityException"/>.
    /// </summary>
    public UnprocessableEntityException(string message) : base(string.Format(DefaultMessage, message))
    {
    }

    /// <summary>
    /// ������������� ���������� <see cref="AccessDenyException"/>.
    /// </summary>
    public UnprocessableEntityException(string message, Exception innerException) : base(string.Format(DefaultMessage, message), innerException)
    {
    }
}