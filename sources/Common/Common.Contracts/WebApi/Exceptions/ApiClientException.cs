using System;
using System.Net;

namespace Common.Contracts.WebApi.Exceptions;

/// <summary>
/// Класс ошибки вызова сервиса через АПИ клиент.
/// </summary>
public abstract class ApiClientException : Exception
{
    /// <summary>
    /// Статус-код ошибки.
    /// </summary>
    public HttpStatusCode StatusCode { get; }

    /// <summary>
    /// Инициализация экземпляра <see cref="ApiClientException"/>.
    /// </summary>
    /// <param name="statusCode">HTTP-код ошибки запроса.</param>
    /// <param name="message">Сообщение ошибки запроса.</param>
    /// <param name="innerException">Внутреннее исключение.</param>
    protected ApiClientException(HttpStatusCode statusCode, string message, Exception innerException) : base(message, innerException)
    {
        StatusCode = statusCode;
    }
}