using System;

namespace Common.Contracts.WebApi.Exceptions;

/// <summary>
/// ����������, �����������, ���� ������������ �� �����������.
/// </summary>
[Serializable]
public class UnauthorizedException : Exception
{
    private const string DefaultMessage = "������������ �� �����������. {0}";

    /// <summary>
    /// ������������� ���������� <see cref="UnauthorizedException"/>.
    /// </summary>
    public UnauthorizedException(string message) : base(string.Format(DefaultMessage, message))
    {
    }

    /// <summary>
    /// ������������� ���������� <see cref="UnauthorizedException"/>.
    /// </summary>
    public UnauthorizedException(string message, Exception innerException) : base(string.Format(DefaultMessage, message), innerException)
    {
    }
}