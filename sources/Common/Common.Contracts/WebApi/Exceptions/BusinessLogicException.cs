﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Contracts.WebApi.Exceptions
{
    public class BusinessLogicException : Exception
    {
        private const string DefaultMessage = "Ошибка бизнес-логики. {0}";

        /// <summary>
        /// Инициализация экземпляра <see cref="BusinessLogicException"/>.
        /// </summary>
        public BusinessLogicException(string message) : base(string.Format(DefaultMessage, message))
        {
        }

        /// <summary>
        /// Инициализация экземпляра <see cref="BusinessLogicException"/>.
        /// </summary>
        public BusinessLogicException(string message, Exception innerException) : base(string.Format(DefaultMessage, message), innerException)
        {
        }
    }
}
