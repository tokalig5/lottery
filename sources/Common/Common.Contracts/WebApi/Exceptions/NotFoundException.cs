using System;

namespace Common.Contracts.WebApi.Exceptions;

/// <summary>
/// ����������, ����������� ��� ���������� ������������� ��������.
/// </summary>
[Serializable]
public class NotFoundException : Exception
{
    private const string DefaultMessage = "������ �� ������. {0}";

    /// <summary>
    /// ������������� ���������� <see cref="NotFoundException"/>.
    /// </summary>
    public NotFoundException(string message) : base(string.Format(DefaultMessage, message))
    {
    }

    /// <summary>
    /// ������������� ���������� <see cref="NotFoundException"/>.
    /// </summary>
    public NotFoundException(string message, Exception innerException) : base(string.Format(DefaultMessage, message), innerException)
    {
    }
}