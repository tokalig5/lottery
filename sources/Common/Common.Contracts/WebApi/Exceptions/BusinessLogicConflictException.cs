using System;

namespace Common.Contracts.WebApi.Exceptions;

/// <summary>
/// ����������, ����������� ��� ������ ������-������.
/// ��������, �������������� �������� �������� ��������� ����-���� � ������� � ���������� ������, ������� ��� ���� ������� �� ������� (������� ����� ������� ������ �� 1000 ������, � �����-�� ������� �������� 1000000).
/// </summary>
[Serializable]
public class BusinessLogicConflictException : Exception
{
    private const string DefaultMessage = "������ ��������� ������-������. {0}";

    /// <summary>
    /// ������������� ���������� <see cref="BusinessLogicConflictException"/>.
    /// </summary>
    public BusinessLogicConflictException(string message) : base(string.Format(DefaultMessage, message))
    {
    }

    /// <summary>
    /// ������������� ���������� <see cref="BusinessLogicConflictException"/>.
    /// </summary>
    public BusinessLogicConflictException(string message, Exception innerException) : base(string.Format(DefaultMessage, message), innerException)
    {
    }
}