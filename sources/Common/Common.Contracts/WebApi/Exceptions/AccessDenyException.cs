using System;

namespace Common.Contracts.WebApi.Exceptions;

/// <summary>
/// ����������, ����������� ��� ���������� ������� � ��������.
/// </summary>
[Serializable]
public class AccessDenyException : Exception
{
    private const string DefaultMessage = "����������� ������ � ��������. {0}";

    /// <summary>
    /// ������������� ���������� <see cref="AccessDenyException"/>.
    /// </summary>
    public AccessDenyException(string message) : base(string.Format(DefaultMessage, message))
    {
    }

    /// <summary>
    /// ������������� ���������� <see cref="AccessDenyException"/>.
    /// </summary>
    public AccessDenyException(string message, Exception innerException) : base(string.Format(DefaultMessage, message), innerException)
    {
    }
}