﻿namespace Common.Contracts.WebApi.Constants
{
    /// <summary>
    /// Константы хэдеров запроса HTTP-контекста.
    /// </summary>
    public static class HttpContextHeader
    {
        /// <summary>
        /// Идентификатор трассировки запроса.
        /// </summary>
        public const string TraceId = "X-Trace-Id";

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public const string UserId = "X-User-Id";
        
        /// <summary>
        /// IP адрес клиента.
        /// </summary>
        public const string ClientIP = "Client-IP";

        /// <summary>
        /// Наименование файла.
        /// </summary>
        public const string FileName = "File-Name";
    }
}
