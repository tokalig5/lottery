﻿using Microsoft.Extensions.Logging;
using System;

namespace Common.Contracts.Logger
{
    /// <summary>
    /// Модель подробного описания состояния системы.
    /// </summary>
    public class JsonLoggerDescriptor
    {
        /// <summary>
        /// Наименование сервиса.
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Уровень лога.
        /// </summary>
        public LogLevel LogLevelEnum { get; set; }

        /// <summary>
        /// Уровень лога.
        /// </summary>
        public string Level => LogLevelEnum.ToString();

        /// <summary>
        /// Трассировочный идентификатор запроса.
        /// </summary>
        public Guid TraceId { get; set; }

        /// <summary>
        /// Текст сообщения.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Дата и время записи.
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Идентификатор текущего пользователя.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Цепочка вызова в случае ошибки.
        /// </summary>
        public string StackTrace { get; set; }

        /// <summary>
        /// Стенд, на котором хостится сервис.
        /// </summary>
        public string Environment { get; set; }

        /// <summary>
        /// Адрес запроса.
        /// </summary>
        public string Path { get; set; }
    }
}
