﻿using System;

namespace Common.Contracts.Enums.Attributes
{
    /// <summary>
    /// Атрибут пользовательского сообщения.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class UserMessageAttribute : Attribute
    {
        /// <summary>
        /// Сообщение.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Инициализация экземпляра <see cref="UserMessageAttribute"/>.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        public UserMessageAttribute(string message)
        {
            Message = message;
        }
    }
}
