﻿using System;
using System.Net;

namespace Common.Contracts.Enums.Attributes
{
    /// <summary>
    /// Атрибут HTTP статус-кода.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class StatusCodeAttribute : Attribute
    {
        /// <summary>
        /// Статус-код http-запроса.
        /// </summary>
        public HttpStatusCode StatusCode { get; private set; }

        /// <summary>
        /// Инициализация экземпляра <see cref="StatusCodeAttribute"/>.
        /// </summary>
        /// <param name="statusCode">Статус-код http-запроса.</param>
        public StatusCodeAttribute(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }
    }
}
