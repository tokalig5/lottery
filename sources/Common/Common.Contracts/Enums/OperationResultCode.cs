﻿using Common.Contracts.Enums.Attributes;
using System.Net;

namespace Common.Contracts.Enums
{
    /// <summary>
    /// Код выполнения операции.
    /// </summary>
    public enum OperationResultCode
    {
        /// <summary>
        /// Удачное выполнение запроса.
        /// </summary>
        [StatusCode(HttpStatusCode.OK)]
        Ok = 0,

        /// <summary>
        /// Удачное создание сущности.
        /// </summary>
        [StatusCode(HttpStatusCode.Created)]
        Created = 1,

        /// <summary>
        /// Ошибка верификации (тестовая ошибка).
        /// </summary>
        [UserMessage("Ошибка верификации (тестовая ошибка)!")]
        [StatusCode(HttpStatusCode.BadRequest)]
        UserVerificationError = 10001
    }
}
