﻿using Common.Contracts.Enums;

namespace Common.Contracts.Models
{
    /// <summary>
    /// Результат выполнения операции.
    /// </summary>
    /// <typeparam name="T">Тип результата выполнения запроса.</typeparam>
    public class OperationResult<T> : BaseOperationResult
    {
        /// <summary>
        /// Результат выполнения запроса.
        /// </summary>
        public T Result { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="OperationResult"/>.
        /// </summary>
        public OperationResult(T result) : base() { Result = result; }

        /// <summary>
        /// Инициализация экземпляра <see cref="OperationResult"/>.
        /// </summary>
        /// <param name="code">Код выполнения операции.</param>
        /// <param name="result">Модель результата.</param>
        public OperationResult(OperationResultCode code, T result) : base(code) { Result = result; }

        /// <summary>
        /// Инициализация экземпляра <see cref="OperationResult"/>.
        /// </summary>
        /// <param name="code">Код выполнения операции.</param>
        /// <param name="message">Сообщение сервиса о результатах выполнения операции.</param>
        public OperationResult(OperationResultCode code, string message) : base(code, message) { }

    }

    /// <summary>
    /// Результат выполнения операции.
    /// </summary>
    public class OperationResult : BaseOperationResult
    {
        /// <summary>
        /// Инициализация экземпляра <see cref="OperationResult"/>.
        /// </summary>
        public OperationResult() : base() { }

        /// <summary>
        /// Инициализация экземпляра <see cref="OperationResult"/>.
        /// </summary>
        /// <param name="code">Код выполнения операции.</param>
        public OperationResult(OperationResultCode code) : base(code) { }

        /// <summary>
        /// Инициализация экземпляра <see cref="OperationResult"/>.
        /// </summary>
        /// <param name="code">Код выполнения операции.</param>
        /// <param name="message">Сообщение сервиса о результатах выполнения операции.</param>
        public OperationResult(OperationResultCode code, string message) : base(code, message) { }
    }
}
