﻿using Common.Contracts.Enums;
using System.Collections.Generic;

namespace Common.Contracts.Models
{
    /// <summary>
    /// Базовая модель результата выполнения операции.
    /// </summary>
    public abstract class BaseOperationResult
    {
        /// <summary>
        /// Успешность выполнения операции.
        /// </summary>
        public bool IsSuccess { get; } = true;

        /// <summary>
        /// Код результата обработки запроса сервиса.
        /// </summary>
        public OperationResultCode Code { get; } = OperationResultCode.Ok;

        /// <summary>
        /// Сообщение сервиса о результатах выполнения.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Инициализация экземпляра <see cref="BaseOperationResult"/>.
        /// </summary>
        protected BaseOperationResult() { }

        /// <summary>
        /// Инициализация экземпляра <see cref="BaseOperationResult"/>.
        /// </summary>
        /// <param name="code">Код выполнения операции.</param>
        protected BaseOperationResult(OperationResultCode code)
        {
            Code = code;
            IsSuccess = _successCodes.Contains(code);
        }

        /// <summary>
        /// Инициализация экземпляра <see cref="BaseOperationResult"/>.
        /// </summary>
        /// <param name="code">Код выполнения операции.</param>
        /// <param name="message">Сообщение сервиса о результатах выполнения операции.</param>
        protected BaseOperationResult(OperationResultCode code, string message) : this(code)
        {
            Message = message;
        }

        /// <summary>
        /// Список успешных кодов выполнения операций.
        /// </summary>
        private readonly List<OperationResultCode> _successCodes = new()
        {
            OperationResultCode.Ok,
            OperationResultCode.Created
        };
    }
}
