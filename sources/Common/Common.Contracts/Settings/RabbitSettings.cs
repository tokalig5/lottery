﻿using System.Collections.Generic;

namespace Common.Contracts.Settings
{
    /// <summary>
    /// RabbitMQ settings
    /// </summary>
    public class RabbitSettings
    {
        /// <summary>
        /// Host
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Virtual host
        /// </summary>
        public string VirtualHost { get; set; }

        /// <summary>
        /// Port
        /// </summary>
        public string Port { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Массив адресов нод кластера шины
        /// </summary>
        public List<string> NodesAddresses { get; set; } = new();
    }
}