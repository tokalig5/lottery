﻿using System;

namespace Common.Contracts.Settings
{
    /// <summary>
    /// Модель настроек MongoDB.
    /// </summary>
    public class MongoSettings
    {
        /// <summary>
        /// Хост.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Порт.
        /// </summary>
        public string Port { get; set; }

        /// <summary>
        /// Имя пользователя.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Наименование базы.
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Получение строки подключения к MongoDB.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public string GetConnectionString()
        {
            if (string.IsNullOrWhiteSpace(UserName) || string.IsNullOrWhiteSpace(Password) || string.IsNullOrWhiteSpace(Port) || string.IsNullOrWhiteSpace(Host))
            {
                throw new ArgumentNullException("Некорректная конфигурация MongoDB!");
            }
            return $"mongodb://{UserName}:{Password}@{Host}:{Port}";
        }
    }
}
