﻿namespace Common.Contracts.Settings
{
    /// <summary>
    /// Settings for proxies
    /// </summary>
    public class ProxySettings
    {
        /// <summary>
        /// Messenger service Url
        /// </summary>
        public string MessengerServiceUrl { get; set; }

        /// <summary>
        /// File service Url
        /// </summary>
        public string FileServiceUrl { get; set; }

        /// <summary>
        /// Finance service Url
        /// </summary>
        public string FinanceServiceUrl { get; set; }

        /// <summary>
        /// Grid registry service Url
        /// </summary>
        public string GridRegistryServiceUrl { get; set; }

        /// <summary>
        /// Lottery service Url
        /// </summary>
        public string LotteryServiceUrl { get; set; }

        /// <summary>
        /// Notification service Url
        /// </summary>
        public string NotificationServiceUrl { get; set; }

        /// <summary>
        /// SignalR service Url
        /// </summary>
        public string SignalRServiceUrl { get; set; }

        /// <summary>
        /// User service Url
        /// </summary>
        public string UserServiceUrl { get; set; }
    }
}