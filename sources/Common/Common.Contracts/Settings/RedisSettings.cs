﻿namespace Common.Contracts.Settings
{
    /// <summary>
    /// Модель настроек Redis.
    /// </summary>
    public class RedisSettings
    {
        /// <summary>
        /// Строка подключения.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Префикс канала.
        /// </summary>
        public string ChannelPrefix { get; set; }

        /// <summary>
        /// Индекс БД.
        /// </summary>
        public int DefaultDatabase { get; set; }
    }
}
