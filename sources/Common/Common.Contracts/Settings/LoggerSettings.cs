﻿namespace Common.Contracts.Settings
{
    /// <summary>
    /// Модель настроек логгера.
    /// </summary>
    public class LoggerSettings
    {
        /// <summary>
        /// Наименование сервиса.
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Максимальная длина тела запроса.
        /// </summary>
        public int RequestMaxSize { get; set; } = 1024;

        /// <summary>
        /// Максимальная длина тела ответа.
        /// </summary>
        public int ResponseMaxSize { get; set; } = 1024;
    }
}
