﻿using Common.Contracts.WebApi.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using System;
using System.Linq;
using System.Net.Http;

namespace Common.Infrastructure.Utils.Extensions
{
    /// <summary>
    /// Методы расширения для работы с HTTP-контекстом.
    /// </summary>
    public static class HttpContextExtensions
    {
        /// <summary>
        /// Получение идентификатора трассировки запроса.
        /// </summary>
        /// <param name="httpContext">HTTP-контекст.</param>
        public static Guid GetTraceId(this HttpContext httpContext)
        {
            return httpContext.GetGuidFromHeader(HttpContextHeader.TraceId);
        }

        /// <summary>
        /// Получение идентификатора пользователя.
        /// </summary>
        /// <param name="httpContext">HTTP-контекст.</param>
        public static Guid GetUserId(this HttpContext httpContext)
        {
            return httpContext.GetGuidFromHeader(HttpContextHeader.UserId);
        }

        /// <summary>
        /// Получение ContentType ответа.
        /// </summary>
        /// <param name="response">Ответ на запрос.</param>
        public static string GetContentType(this HttpResponseMessage response)
        {
            return response.GetHeaderFromResponseContent(HeaderNames.ContentType);
        }

        /// <summary>
        /// Получение наименование файла из заголовков ответа.
        /// </summary>
        /// <param name="response">Ответ на запрос.</param>
        public static string GetFileName(this HttpResponseMessage response)
        {
            return response.GetHeaderFromResponse(HttpContextHeader.FileName);
        }

        /// <summary>
        /// Получение ContentLength ответа.
        /// </summary>
        /// <param name="response">Ответ на запрос.</param>
        public static long GetContentLength(this HttpResponseMessage response)
        {
            long.TryParse(response.GetHeaderFromResponseContent(HeaderNames.ContentLength), out var result);
            return result;
        }

        /// <summary>
        /// Получение ContentDisposition ответа.
        /// </summary>
        /// <param name="response">Ответ на запрос.</param>
        public static string GetContentDisposition(this HttpResponseMessage response)
        {
            return response.GetHeaderFromResponseContent(HeaderNames.ContentDisposition);
        }

        /// <summary>
        /// Установка идентификатора трассировки запроса.
        /// </summary>
        /// <param name="httpContext">HTTP-контекст.</param>
        /// <param name="traceId">Идентификатор трассировки запроса.</param>
        public static void SetTraceId(this HttpContext httpContext, Guid traceId)
        {
            httpContext.SetHeaderValueToRequest(HttpContextHeader.TraceId, traceId.ToString());
        }

        /// <summary>
        /// Установка идентификатора пользователя.
        /// </summary>
        /// <param name="httpContext">HTTP-контекст.</param>
        /// <param name="userId">Идентификатор пользователя.</param>
        public static void SetUserId(this HttpContext httpContext, Guid userId)
        {
            httpContext.SetHeaderValueToRequest(HttpContextHeader.UserId, userId.ToString());
        }

        private static Guid GetGuidFromHeader(this HttpContext httpContext, string headerName)
        {
            var stringValue = httpContext.GetStringFromHeader(headerName);
            Guid.TryParse(stringValue, out var result);
            return result;
        }

        private static string GetStringFromHeader(this HttpContext httpContext, string headerName)
        {
            var request = httpContext.Request;
            var containsKey = request?.Headers?.ContainsKey(headerName);
            return containsKey == true ? (string)request.Headers[headerName] : null;
        }

        private static string GetHeaderFromResponseContent(this HttpResponseMessage response, string headerName)
        {
            var containsKey = response?.Content?.Headers?.Contains(headerName);
            return containsKey == true ? response.Content.Headers.GetValues(headerName).FirstOrDefault() : null;
        }

        private static string GetHeaderFromResponse(this HttpResponseMessage response, string headerName)
        {
            var containsKey = response?.Headers?.Contains(headerName);
            return containsKey == true ? response.Headers.GetValues(headerName).FirstOrDefault() : null;
        }

        private static void SetHeaderValueToRequest(this HttpContext httpContext, string headerName, string headerValue)
        {
            var containsKey = httpContext.Request?.Headers?.ContainsKey(headerName);
            if (containsKey == true)
            {
                httpContext.Request.Headers[headerName] = headerValue;
            }
            else
            {
                httpContext.Request?.Headers?.Add(headerName, headerValue);
            }
        }
    }
}
