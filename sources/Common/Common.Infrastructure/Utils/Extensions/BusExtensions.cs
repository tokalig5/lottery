﻿using Common.Contracts.Bus.Abstraction;
using Common.Infrastructure.Bus.Logger;
using GreenPipes;
using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using MassTransit.RabbitMqTransport;
using System;
using System.Linq;
using System.Reflection;

namespace Common.Infrastructure.Utils.Extensions
{
    /// <summary>
    /// Методы расширения для работы с шиной данных.
    /// </summary>
    public static class BusExtensions
    {
        /// <summary>
        /// Использование логгера.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="configurator"></param>
        /// <param name="busRegistrationContext"></param>
        public static void UseLogger<T>(this IPipeConfigurator<T> configurator, IBusRegistrationContext busRegistrationContext) where T : class, PipeContext
        {
            configurator.AddPipeSpecification(new LoggerSpecification<T>(busRegistrationContext));
        }


        /// <summary>
        /// Add publish endpoint to bus.
        /// </summary>
        /// <typeparam name="TEvent">Exent.</typeparam>
        /// <typeparam name="TConsumer">Consumer.</typeparam>
        /// <param name="cfg">Configuration.</param>
        /// <param name="context">Registration context.</param>
        /// <param name="useRetry">Using retry.</param>
        public static void AddPublishEndpoint<TEvent, TConsumer>(this IRabbitMqBusFactoryConfigurator cfg, IBusRegistrationContext context, bool useRetry = true) where TEvent : class, IMessageQueueEvent where TConsumer : class, IConsumer<TEvent>
        {
            var endpointUrl = $"{Assembly.GetEntryAssembly().GetName().Name}:{typeof(TEvent).FullName}";
            cfg.AddBaseEndpoint<TEvent, TConsumer>(context, endpointUrl, useRetry);
        }

        /// <summary>
        /// Add send endpoint to bus.
        /// </summary>
        /// <typeparam name="TCommand">Command.</typeparam>
        /// <typeparam name="TConsumer">Consumer.</typeparam>
        /// <param name="cfg">Configuration.</param>
        /// <param name="context">Registration context.</param>
        /// <param name="useRetry">Using retry.</param>
        public static void AddSendEndpoint<TCommand, TConsumer>(this IRabbitMqBusFactoryConfigurator cfg, IBusRegistrationContext context, bool useRetry = true) where TCommand : class, IMessageQueueCommand where TConsumer : class, IConsumer<TCommand>
        {
            var endpointUrl = $"{typeof(TCommand).FullName}";
            cfg.AddBaseEndpoint<TCommand, TConsumer>(context, endpointUrl, useRetry);
        }

        /// <summary>
        /// Добавление консьюмеров.
        /// </summary>
        /// <param name="configurator"></param>
        /// <returns></returns>
        public static IServiceCollectionBusConfigurator AddConsumers(this IServiceCollectionBusConfigurator configurator)
        {
            var consumers = Assembly.GetEntryAssembly().GetTypes().Where(types => typeof(IConsumer).IsAssignableFrom(types)).ToList();
            consumers.ForEach(consumer => configurator.AddConsumer(consumer));
            return configurator;
        }

        private static void AddBaseEndpoint<TMessage, TConsumer>(this IRabbitMqBusFactoryConfigurator cfg, IBusRegistrationContext context, string endpointUrl, bool useRetry) where TMessage : class, IMessageQueueBaseMessage where TConsumer : class, IConsumer<TMessage>
        {
            cfg.ReceiveEndpoint(endpointUrl, e =>
            {
                e.Consumer<TConsumer>(context);
                if (useRetry)
                {
                    e.UseRetry(configurator =>
                    {
                        configurator.Interval(5, TimeSpan.FromSeconds(5));
                    });
                }
            });
        }
    }
}