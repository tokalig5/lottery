﻿using Common.Contracts.WebApi.Models;
using File.Contracts;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.Utils.Extensions
{
    /// <summary>
    /// Методы расширения для работы с HTTP-запросами.
    /// </summary>
    public static class HttpClientExtensions
    {
        #region POST

        /// <summary>
        /// Отправка POST-запроса без тела запроса.
        /// </summary>
        /// <param name="client">Текущий HttpClient.</param>
        /// <param name="url">Адрес отправки запроса.</param>
        /// <param name="errorAction">Действие при ошибке.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        public static async Task PostAsync(this HttpClient client, string url, Action<HttpStatusCode, string, Exception> errorAction, CancellationToken cancellationToken)
        {
            using var httpResponse = await client.PostAsync(url, null, cancellationToken);
            await CheckHttpResponseAsync(httpResponse, url, errorAction, cancellationToken);
        }

        /// <summary>
        /// Отправка POST-запроса с телом запроса.
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса.</typeparam>
        /// <param name="client">Текущий HttpClient.</param>
        /// <param name="url">Адрес отправки запроса.</param>
        /// <param name="request">Модель запроса.</param>
        /// <param name="errorAction">Действие при ошибке.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        public static async Task PostAsync<TRequest>(this HttpClient client, string url, TRequest request, Action<HttpStatusCode, string, Exception> errorAction, CancellationToken cancellationToken)
        {
            using var httpResponse = await client.PostAsJsonAsync(url, request, cancellationToken);
            await CheckHttpResponseAsync(httpResponse, url, errorAction, cancellationToken);
        }

        /// <summary>
        /// Отправка POST-запроса с телом запроса.
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса.</typeparam>
        /// <typeparam name="TResponse">Тип ответа.</typeparam>
        /// <param name="client">Текущий HttpClient.</param>
        /// <param name="url">Адрес отправки запроса.</param>
        /// <param name="request">Модель запроса.</param>
        /// <param name="errorAction">Действие при ошибке.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        public static async Task<TResponse> PostAsync<TRequest, TResponse>(this HttpClient client, string url, TRequest request, Action<HttpStatusCode, string, Exception> errorAction, CancellationToken cancellationToken)
        {
            using var httpResponse = await client.PostAsJsonAsync(url, request, cancellationToken);
            await CheckHttpResponseAsync(httpResponse, url, errorAction, cancellationToken);
            return await httpResponse.Content.ReadFromJsonAsync<TResponse>(cancellationToken: cancellationToken);
        }

        /// <summary>
        /// Отправка POST-запроса с телом запроса в виде <see cref="MultipartFormDataContent"/>.
        /// </summary>
        /// <param name="client">Текущий HttpClient.</param>
        /// <param name="url">Адрес отправки запроса.</param>
        /// <param name="multipartContent">Тело запроса.</param>
        /// <param name="errorAction">Действие при ошибке.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        public static async Task<string> PostFileAsync(this HttpClient client, string url, MultipartFormDataContent multipartContent, Action<HttpStatusCode, string, Exception> errorAction, CancellationToken cancellationToken)
        {
            using var httpResponse = await client.PostAsync(url, multipartContent, cancellationToken);
            await CheckHttpResponseAsync(httpResponse, url, errorAction, cancellationToken);
            return await httpResponse.Content.ReadAsStringAsync(cancellationToken: cancellationToken);
        }

        #endregion

        #region GET

        /// <summary>
        /// Отправка GET-запроса.
        /// </summary>
        /// <typeparam name="TResponse">Тип ответа.</typeparam>
        /// <param name="client">Текущий HttpClient.</param>
        /// <param name="url">Адрес отправки запроса.</param>
        /// <param name="errorAction">Действие при ошибке.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        public static async Task<TResponse> GetAsync<TResponse>(this HttpClient client, string url, Action<HttpStatusCode, string, Exception> errorAction, CancellationToken cancellationToken)
        {
            using var httpResponse = await client.GetAsync(url, cancellationToken);
            await CheckHttpResponseAsync(httpResponse, url, errorAction, cancellationToken);
            return await httpResponse.Content.ReadFromJsonAsync<TResponse>(cancellationToken: cancellationToken);
        }

        /// <summary>
        /// Отправка GET-запроса.
        /// </summary>
        /// <param name="client">Текущий HttpClient.</param>
        /// <param name="url">Адрес отправки запроса.</param>
        /// <param name="errorAction">Действие при ошибке.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        public static async Task<FileStreamModel> GetFileStreamAsync(this HttpClient client, string url, Action<HttpStatusCode, string, Exception> errorAction, CancellationToken cancellationToken)
        {
            var httpResponse = await client.GetAsync(url, cancellationToken);
            await CheckHttpResponseAsync(httpResponse, url, errorAction, cancellationToken);
            var result = new FileStreamModel
            {
                Info = new FileInfoDto
                {
                    ContentType = httpResponse.GetContentType(),
                    Length = httpResponse.GetContentLength(),
                    Name = Uri.UnescapeDataString(httpResponse.GetFileName())
                },
                Stream = await httpResponse.Content.ReadAsStreamAsync(cancellationToken: cancellationToken)
            };
            return result;
        }

        #endregion

        #region DELETE

        /// <summary>
        /// Отправка DELETE-запроса.
        /// </summary>
        /// <param name="client">Текущий HttpClient.</param>
        /// <param name="url">Адрес отправки запроса.</param>
        /// <param name="errorAction">Действие при ошибке.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        public static async Task DeleteAsync(this HttpClient client, string url, Action<HttpStatusCode, string, Exception> errorAction, CancellationToken cancellationToken)
        {
            using var httpResponse = await client.DeleteAsync(url, cancellationToken);
            await CheckHttpResponseAsync(httpResponse, url, errorAction, cancellationToken);
        }

        #endregion

        #region Private methods

        private static async Task CheckHttpResponseAsync(HttpResponseMessage httpResponse, string url, Action<HttpStatusCode, string, Exception> errorAction, CancellationToken cancellationToken)
        {
            if (!httpResponse.IsSuccessStatusCode)
            {
                var apiError = await httpResponse.Content.ReadFromJsonAsync<ApiError>(cancellationToken: cancellationToken);
                if (apiError == null)
                {
                    throw new HttpRequestException(GetHttpErrorMessage(url, httpResponse.StatusCode, httpResponse.ReasonPhrase));
                }

                errorAction?.Invoke(apiError.StatusCode, apiError.Message, apiError.Exception);
            }
        }

        private static string GetHttpErrorMessage(string url, HttpStatusCode statusCode, string errorMessage)
        {
            return $"Ошибка отправки http-запроса. Url: {url}. StatusCode: {statusCode}. Message: {errorMessage}";
        }

        #endregion
    }
}