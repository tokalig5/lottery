﻿using Common.Contracts.Enums.Attributes;
using System;
using System.ComponentModel;
using System.Linq;
using System.Net;

namespace Common.Infrastructure.Utils.Extensions
{
    /// <summary>
    /// Методы расширения для работы с перечислениями.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Получить значение Description атрибута у перечисления.
        /// </summary>
        /// <param name="obj">Перечисление.</param>
        /// <returns>Значение Description.</returns>
        public static string GetDescription(this Enum obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }

            var attribute = obj.GetType().GetField(obj.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false).Cast<DescriptionAttribute>().FirstOrDefault();
            return attribute?.Description ?? obj.ToString();
        }

        /// <summary>
        /// Получение значения HTTP статус-кода поля перечисления.
        /// </summary>
        /// <param name="obj">Перечисление.</param>
        /// <returns>HTTP статус-код.</returns>
        public static HttpStatusCode GetStatusCode(this Enum obj)
        {
            var attribute = obj.GetAttribute<StatusCodeAttribute>();
            return attribute?.StatusCode ?? HttpStatusCode.OK;
        }

        /// <summary>
        /// Получение пользовательского сообщения.
        /// </summary>
        /// <param name="obj">Перечисление.</param>
        /// <returns>Пользовательское сообщение.</returns>
        public static string GetUserMessage(this Enum obj)
        {
            var attribute = obj.GetAttribute<UserMessageAttribute>();
            return attribute?.Message;
        }

        private static T GetAttribute<T>(this Enum obj) where T : Attribute
        {
            var fieldInfo = obj.GetType().GetField(obj.ToString());
            return fieldInfo?.GetCustomAttributes(typeof(T), false).Cast<T>().FirstOrDefault();
        }
    }
}