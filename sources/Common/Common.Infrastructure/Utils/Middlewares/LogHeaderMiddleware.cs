﻿using Common.Infrastructure.Utils.Extensions;
using Microsoft.AspNetCore.Http;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Common.Infrastructure.Utils.Middlewares
{
    /// <summary>
    /// Мидлвейр работы с хедерами запроса АПИ.
    /// </summary>
    public class LogHeaderMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Инициализация экземпляра <see cref="LogHeaderMiddleware"/>.
        /// </summary>        
        public LogHeaderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <inheritdoc/>
        public async Task Invoke(HttpContext context)
        {
            CheckTraceId(context);
            CheckUserId(context);
            await _next(context);
        }

        private static void CheckTraceId(HttpContext context)
        {
            var traceId = context.GetTraceId();
            if (traceId.Equals(Guid.Empty))
            {
                context.SetTraceId(Guid.NewGuid());
            }
        }

        private static void CheckUserId(HttpContext context)
        {
            var userId = context.GetUserId();
            if (userId.Equals(Guid.Empty))
            {
                var identifier = context.User?.Claims?.Where(c => c.Type == JwtRegisteredClaimNames.Sub)?.Select(c => c.Value)?.FirstOrDefault();
                Guid.TryParse(identifier, out var result);
                context.SetUserId(result);
            }
        }
    }
}
