﻿using Common.Contracts.WebApi.Constants;
using Common.Infrastructure.Utils.Extensions;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.Utils.Middlewares
{
    /// <summary>
    /// Предобработчик Http запросов.
    /// </summary>
    public class HttpRequestHandler : DelegatingHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Инициализация экземпляра <see cref="HttpRequestHandler"/>.
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        public HttpRequestHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <inheritdoc/>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.Headers.Add(HttpContextHeader.TraceId, _httpContextAccessor.HttpContext.GetTraceId().ToString());
            request.Headers.Add(HttpContextHeader.UserId, _httpContextAccessor.HttpContext.GetUserId().ToString());
            return base.SendAsync(request, cancellationToken);
        }
    }
}
