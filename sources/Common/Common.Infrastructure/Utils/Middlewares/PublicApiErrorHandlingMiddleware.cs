﻿using Common.Contracts.WebApi.Exceptions;
using Common.Contracts.WebApi.Models;
using Common.Infrastructure.Utils.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Common.Infrastructure.Utils.Middlewares
{
    /// <summary>
    /// Мидлвейр, обрабатывающий ошибки работы публичного API.
    /// </summary>
    public class PublicApiErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<PublicApiErrorHandlingMiddleware> _logger;

        /// <summary>
        /// Инициализация экземпляра <see cref="PublicApiErrorHandlingMiddleware"/>.
        /// </summary>
        /// <param name="next">Делегат обработки HTTP-запроса.</param>
        public PublicApiErrorHandlingMiddleware(RequestDelegate next, ILogger<PublicApiErrorHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        /// <summary>
        /// Тело мидлвейра.
        /// </summary>
        /// <param name="context">Контекст запроса.</param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (ApiClientException exception)
            {
                context.Response.StatusCode = (int)exception.StatusCode;
                await WriteApiErrorResponseAsync(context, exception.StatusCode, exception);
            }
            catch (Exception exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                await WriteApiErrorResponseAsync(context, HttpStatusCode.InternalServerError, exception);
            }
        }

        private async Task WriteApiErrorResponseAsync(HttpContext context, HttpStatusCode statusCode, Exception exception)
        {
            var apiError = new PublicApiError(context.GetTraceId(), exception.Message);
            _logger.LogError(exception, "Ошибка выполнения запроса!");
            await context.Response.WriteAsync(JsonConvert.SerializeObject(apiError));
        }
    }
}
