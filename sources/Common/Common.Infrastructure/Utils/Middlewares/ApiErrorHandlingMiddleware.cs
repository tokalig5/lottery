﻿using Common.Contracts.WebApi.Exceptions;
using Common.Contracts.WebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Common.Infrastructure.Utils.Middlewares
{
    /// <summary>
    /// Мидлвейр, обрабатывающий ошибки работы API.
    /// </summary>
    public class ApiErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ApiErrorHandlingMiddleware> _logger;

        /// <summary>
        /// Инициализация экземпляра <see cref="ApiErrorHandlingMiddleware"/>.
        /// </summary>
        /// <param name="next">Делегат обработки HTTP-запроса.</param>
        public ApiErrorHandlingMiddleware(RequestDelegate next, ILogger<ApiErrorHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        /// <summary>
        /// Тело мидлвейра.
        /// </summary>
        /// <param name="context">Контекст запроса.</param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (ApiClientException exception)
            {
                context.Response.StatusCode = (int)exception.StatusCode;
                await WriteApiErrorResponseAsync(context, exception.StatusCode, exception);
            }
            catch (AccessDenyException exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                await WriteApiErrorResponseAsync(context, HttpStatusCode.Forbidden, exception);
            }
            catch (BusinessLogicConflictException exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.Conflict;
                await WriteApiErrorResponseAsync(context, HttpStatusCode.Conflict, exception);
            }
            catch (NotFoundException exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                await WriteApiErrorResponseAsync(context, HttpStatusCode.NotFound, exception);
            }
            catch (UnauthorizedException exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await WriteApiErrorResponseAsync(context, HttpStatusCode.Unauthorized, exception);
            }
            catch (UnprocessableEntityException exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.UnprocessableEntity;
                await WriteApiErrorResponseAsync(context, HttpStatusCode.UnprocessableEntity, exception);
            }

            catch (Exception exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                await WriteApiErrorResponseAsync(context, HttpStatusCode.InternalServerError, exception);
            }
        }

        private async Task WriteApiErrorResponseAsync(HttpContext context, HttpStatusCode statusCode, Exception exception)
        {
            var apiError = new ApiError(statusCode, exception.Message, exception);
            _logger.LogError(exception, "Ошибка выполнения запроса!");
            await context.Response.WriteAsync(JsonConvert.SerializeObject(apiError));
        }
    }
}
