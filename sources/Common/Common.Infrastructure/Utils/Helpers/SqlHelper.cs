﻿using System;
using System.IO;
using System.Reflection;

namespace Common.Infrastructure.Utils.Helpers
{
    /// <summary>
    /// Класс для работы со скриптами SQL.
    /// </summary>
    public static class SqlHelper
    {
        /// <summary>
        /// Получение строки-запроса SQL.
        /// </summary>
        /// <param name="sqlFileName">Имя файла с SQL-скриптом.</param>
        /// <returns>Строка SQL.</returns>
        public static string GetSqlQuery(string sqlFileName)
        {
            var assembly = Assembly.GetEntryAssembly();
            using var stream = assembly?.GetManifestResourceStream($"{assembly.GetName().Name}.{sqlFileName}");
            if (stream == null)
            {
                throw new NullReferenceException($"Файл {sqlFileName} не найден.");
            }

            using var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }
    }
}
