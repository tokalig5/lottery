﻿using System;
using System.Threading.Tasks;

namespace Common.Infrastructure.Utils.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с тасковыми методами
    /// </summary>
    public static class TaskHelper
    {
        /// <summary>
        /// Выполняет задачу в синхронном режиме.
        /// </summary>
        /// <typeparam name="T">Тип возвращаемого значения задачи.</typeparam>
        /// <param name="function">Выполняемое действие, содержащее задачу.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public static T RunAsSync<T>(Func<Task<T>> function)
        {
            return Task.Run(async () => await function()).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Выполняет задачу в синхронном режиме.
        /// </summary>
        /// <param name="function">Выполняемое действие, содержащее задачу.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public static void RunAsSync(Func<Task> function)
        {
            Task.Run(async () => await function()).ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}
