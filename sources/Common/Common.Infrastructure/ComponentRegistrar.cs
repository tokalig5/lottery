﻿using Common.Contracts.Settings;
using Common.Infrastructure.Bus.Services.BusEventContainer;
using Common.Infrastructure.Bus.Services.BusService;
using Common.Infrastructure.DataAccess.Repositories;
using Common.Infrastructure.Logger;
using Common.Infrastructure.Mediatr.Pipelines;
using Common.Infrastructure.Utils.Extensions;
using MassTransit;
using MassTransit.RabbitMqTransport;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Common.Infrastructure
{
    /// <summary>
    /// Регистрация компонентов.
    /// </summary>
    public static class ComponentRegistrar
    {
        private const string RabbitUrlPrefix = "rabbitmq";

        /// <summary>
        /// Add and configure bus service
        /// </summary>
        /// <param name="services">ServiceCollection</param>
        /// <param name="settings">Rabbit settings</param>
        /// <param name="endpointsRegistration">Consumers</param>
        public static IServiceCollection AddBusService(this IServiceCollection services, RabbitSettings settings, IList<Action<IRabbitMqBusFactoryConfigurator, IBusRegistrationContext>> endpointsRegistration = null)
        {
            services.AddScoped<IBusService, BusService>();
            services.AddScoped<IBusEventContainer, BusEventContainer>();
            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    var url = new Uri($"{RabbitUrlPrefix}://{settings.Host}:{settings.Port}/{settings.VirtualHost}");
                    cfg.Host(url, host =>
                    {
                        host.Username(settings.UserName);
                        host.Password(settings.Password);
                        host.Heartbeat(600);
                        host.RequestedConnectionTimeout((int)TimeSpan.FromSeconds(30).TotalMilliseconds);
                        if (settings.NodesAddresses.Any())
                        {
                            host.UseCluster(c =>
                            {
                                settings.NodesAddresses.ForEach(serviceHost => c.Node(serviceHost));
                            });
                        }
                    });

                    if (endpointsRegistration?.Count > 0)
                    {
                        foreach (var endpoint in endpointsRegistration)
                        {
                            endpoint?.Invoke(cfg, context);
                        }
                    }

                    cfg.UseLogger(context);
                });
                x.AddConsumers();
            });

            services.AddMassTransitHostedService(waitUntilStarted: true);
            return services;
        }

        /// <summary>
        /// Добавление в проект медиатора с пайплайнами
        /// </summary>
        /// <param name="services">Список сервисов.</param>
        /// <param name="assembly">Наименование сборки для регистрации обработчиков.</param>
        public static IServiceCollection AddMediator(this IServiceCollection services, Assembly assembly)
        {
            services.AddMediatR(assembly);
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingPipeline<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(EventSourcePipeline<,>));
            return services;
        }

        /// <summary>
        /// Конфигурирование подключения к БД.
        /// </summary>
        /// <typeparam name="TContext">Контекст чтения-записи.</typeparam>
        /// <typeparam name="TReadOnlyContext">Контекст чтения.</typeparam>
        /// <param name="services">Список сервисов.</param>
        /// <param name="configuration">Конфигурация проекта.</param>
        public static IServiceCollection AddDbConfiguration<TContext, TReadOnlyContext>(this IServiceCollection services, IConfiguration configuration) where TContext : DbContext where TReadOnlyContext : DbContext
        {
            var connectionString = configuration.GetConnectionString("ConnectionString");
            var readOnlyConnectionString = configuration.GetConnectionString("ReadOnlyConnectionString");

            services.AddDbContext<TContext>(options => options.UseNpgsql(connectionString));
            services.AddDbContext<TReadOnlyContext>(options => options.UseNpgsql(readOnlyConnectionString));

            services.AddScoped(typeof(IReadOnlyRepository<,>), typeof(ReadOnlyRepository<,>));
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            return services;
        }

        /// <summary>
        /// Конфигурирование контекстного логгера.
        /// </summary>
        /// <param name="services">Список сервисов.</param>
        /// <param name="configuration">Конфигурация проекта.</param>
        public static IServiceCollection AddContextLogger(this IServiceCollection services, IConfiguration configuration)
        {
            var loggerSettings = new LoggerSettings();
            configuration.Bind("Logging", loggerSettings);
            services.AddSingleton(loggerSettings);

            services.AddHttpContextAccessor();
            services.AddSingleton(typeof(ILogger<>), typeof(ContextLogger<>));
            return services;
        }
    }
}
