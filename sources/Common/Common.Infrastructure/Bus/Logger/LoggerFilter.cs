﻿using GreenPipes;
using MassTransit;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Common.Infrastructure.Bus.Logger
{
    /// <summary>
    /// Logger pipeline
    /// </summary>
    /// <typeparam name="T">Context type</typeparam>
    public class LoggerFilter<T> : IFilter<T> where T : class, PipeContext
    {
        private readonly ILogger<LoggerFilter<T>> _logger;

        public LoggerFilter(IBusRegistrationContext busRegistrationContext)
        {
            _logger = busRegistrationContext.GetRequiredService<ILogger<LoggerFilter<T>>>();
        }

        /// <summary>
        /// Send is called for each context that is sent through the pipeline
        /// </summary>
        /// <param name="context">The context sent through the pipeline</param>
        /// <param name="next">The next filter in the pipe, must be called or the pipe ends here</param>
        public async Task Send(T context, IPipe<T> next)
        {
            try
            {
                _logger.LogDebug($"Consuming is started. Assembly: {Assembly.GetEntryAssembly()?.GetName().Name}. BusMessage: {context.GetType().FullName}");
                await next.Send(context);
                _logger.LogDebug($"Consuming is finished. Assembly: {Assembly.GetEntryAssembly()?.GetName().Name}. BusMessage: {context.GetType().FullName}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error during consuming {JsonConvert.SerializeObject(context)}. Assembly: {Assembly.GetEntryAssembly()?.GetName().Name}. BusMessage: {context.GetType().FullName}. ErrorMessage: {ex.Message}. Stack trace: {ex.StackTrace}");
                throw;
            }
        }

        public void Probe(ProbeContext context)
        {
            context.CreateScope("logger-pipeline");
        }
    }
}