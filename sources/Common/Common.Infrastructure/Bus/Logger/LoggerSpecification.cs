﻿using GreenPipes;
using MassTransit;
using System.Collections.Generic;
using System.Linq;

namespace Common.Infrastructure.Bus.Logger
{
    public class LoggerSpecification<T> : IPipeSpecification<T> where T : class, PipeContext
    {
        private readonly IBusRegistrationContext _busRegistrationContext;

        public LoggerSpecification(IBusRegistrationContext busRegistrationContext)
        {
            _busRegistrationContext = busRegistrationContext;
        }

        public IEnumerable<ValidationResult> Validate()
        {
            return Enumerable.Empty<ValidationResult>();
        }

        public void Apply(IPipeBuilder<T> builder)
        {
            builder.AddFilter(new LoggerFilter<T>(_busRegistrationContext));
        }
    }
}