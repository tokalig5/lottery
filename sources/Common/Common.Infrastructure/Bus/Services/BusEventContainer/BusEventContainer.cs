﻿using Common.Contracts.Bus.Abstraction;
using Common.Contracts.Bus.Enums;
using System.Collections.Generic;

namespace Common.Infrastructure.Bus.Services.BusEventContainer
{
    /// <summary>
    /// Контейнер-накопитель сообщений для шины.
    /// </summary>
    public class BusEventContainer : IBusEventContainer
    {
        /// <inheritdoc/>
        public List<IMessageQueueBaseMessage> OnSuccessMessages { get; } = new();

        /// <inheritdoc/>
        public List<IMessageQueueBaseMessage> OnErrorMessages { get; } = new();

        /// <inheritdoc/>
        public List<IMessageQueueBaseMessage> AnyWayMessages { get; } = new();

        /// <inheritdoc/>
        public void Add(IMessageQueueBaseMessage message, MessageEventType messageEventType)
        {
            switch (messageEventType)
            {
                case MessageEventType.OnSuccess:
                    OnSuccessMessages.Add(message);
                    break;
                case MessageEventType.OnError:
                    OnErrorMessages.Add(message);
                    break;
                case MessageEventType.Always:
                    AnyWayMessages.Add(message);
                    break;
            }
        }

        /// <inheritdoc/>
        public void Clear()
        {
            OnSuccessMessages.Clear();
            OnErrorMessages.Clear();
            AnyWayMessages.Clear();
        }
    }
}
