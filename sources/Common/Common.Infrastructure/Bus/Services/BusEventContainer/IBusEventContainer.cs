﻿using Common.Contracts.Bus.Abstraction;
using Common.Contracts.Bus.Enums;
using System.Collections.Generic;

namespace Common.Infrastructure.Bus.Services.BusEventContainer
{
    /// <summary>
    /// Контейнер-накопитель сообщений для шины.
    /// </summary>
    public interface IBusEventContainer
    {
        /// <summary>
        /// Сообщения в случае успеха.
        /// </summary>
        List<IMessageQueueBaseMessage> OnSuccessMessages { get; }

        /// <summary>
        /// Сообщения в случае ошибки.
        /// </summary>
        List<IMessageQueueBaseMessage> OnErrorMessages { get; }

        /// <summary>
        /// Сообщения, отправляемые в любом случае.
        /// </summary>
        List<IMessageQueueBaseMessage> AnyWayMessages { get; }

        /// <summary>
        /// Добавить сообщение в накопитель.
        /// </summary>
        /// <param name="message">Сообщение для шины данных.</param>
        /// <param name="messageEventType">Тип события.</param>
        void Add(IMessageQueueBaseMessage message, MessageEventType messageEventType);

        /// <summary>
        /// Очистить накопитель сообщений.
        /// </summary>
        void Clear();
    }
}
