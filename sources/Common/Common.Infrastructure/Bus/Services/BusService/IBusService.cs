﻿using Common.Contracts.Bus.Abstraction;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.Bus.Services.BusService
{
    /// <summary>
    /// Message queue service
    /// </summary>
    public interface IBusService
    {
        /// <summary>
        /// Send message to queue
        /// </summary>
        /// <param name="command">Message command</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <typeparam name="TCommand">Message command type</typeparam>
        Task SendAsync<TCommand>(TCommand command, CancellationToken cancellationToken) where TCommand : IMessageQueueCommand;

        /// <summary>
        /// Publish message
        /// </summary>
        /// <param name="event">Message event</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <typeparam name="TEvent">Message event type</typeparam>
        Task PublishAsync<TEvent>(TEvent @event, CancellationToken cancellationToken) where TEvent : IMessageQueueEvent;
    }
}