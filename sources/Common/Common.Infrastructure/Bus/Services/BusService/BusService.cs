﻿using Common.Contracts.Bus.Abstraction;
using MassTransit;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.Bus.Services.BusService
{
    /// <summary>
    /// Message queue service
    /// </summary>
    public class BusService : IBusService
    {
        private readonly ISendEndpointProvider _sendEndpointProvider;
        private readonly IPublishEndpoint _publishEndpoint;
        private const string QueueNamePrefix = "queue";

        public BusService(ISendEndpointProvider sendEndpointProvider, IPublishEndpoint publishEndpoint)
        {
            _sendEndpointProvider = sendEndpointProvider;
            _publishEndpoint = publishEndpoint;
        }

        /// <inheritdoc/>
        public async Task SendAsync<TCommand>(TCommand command, CancellationToken cancellationToken) where TCommand : IMessageQueueCommand
        {
            var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri($"{QueueNamePrefix}:{typeof(TCommand).FullName}"));
            await endpoint.Send(command, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task PublishAsync<TEvent>(TEvent @event, CancellationToken cancellationToken) where TEvent : IMessageQueueEvent
        {
            await _publishEndpoint.Publish(@event, cancellationToken);
        }
    }
}