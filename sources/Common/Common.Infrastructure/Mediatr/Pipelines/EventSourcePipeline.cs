﻿using Common.Contracts.Bus.Abstraction;
using Common.Infrastructure.Bus.Services.BusEventContainer;
using Common.Infrastructure.Bus.Services.BusService;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.Mediatr.Pipelines
{
    /// <summary>
    /// Пайплайн работы с очередью сообщений для Mediator.
    /// </summary>
    /// <typeparam name="TRequest">Запрос</typeparam>
    /// <typeparam name="TResponse">Ответ на запрос</typeparam>
    public class EventSourcePipeline<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly IBusEventContainer _messageQueueContainer;
        private readonly IBusService _busService;

        public EventSourcePipeline(IBusService busService, IBusEventContainer messageQueueContainer)
        {
            _busService = busService;
            _messageQueueContainer = messageQueueContainer;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            try
            {
                var response = await next();

                if (_messageQueueContainer.OnErrorMessages.Any())
                {
                    await ProcessMessagesAsync(_messageQueueContainer.OnErrorMessages, cancellationToken);
                    return response;
                }

                await ProcessMessagesAsync(_messageQueueContainer.OnSuccessMessages, cancellationToken);
                return response;
            }
            catch
            {
                await ProcessMessagesAsync(_messageQueueContainer.OnErrorMessages, cancellationToken);
                throw;
            }
            finally
            {
                await ProcessMessagesAsync(_messageQueueContainer.AnyWayMessages, cancellationToken);
                _messageQueueContainer.Clear();
            }
        }

        private async Task ProcessMessagesAsync(IList<IMessageQueueBaseMessage> messages, CancellationToken cancellationToken)
        {
            var tasks = new List<Task>();
            foreach (var message in messages)
            {
                if (message is IMessageQueueEvent @event)
                {
                    tasks.Add(_busService.PublishAsync(@event, cancellationToken));
                }
                else if (message is IMessageQueueCommand command)
                {
                    tasks.Add(_busService.SendAsync(command, cancellationToken));
                }
            }

            await Task.WhenAll(tasks);
        }
    }
}
