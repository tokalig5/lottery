﻿using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.Mediatr.Pipelines
{
    /// <summary>
    /// Пайплайн логгирования для Mediator.
    /// </summary>
    /// <typeparam name="TRequest">Запрос</typeparam>
    /// <typeparam name="TResponse">Ответ на запрос</typeparam>
    public class LoggingPipeline<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly ILogger<LoggingPipeline<TRequest, TResponse>> _logger;

        /// <summary>
        /// Инициализация экземпляра <see cref="LoggingPipeline"/>.
        /// </summary>
        public LoggingPipeline(ILogger<LoggingPipeline<TRequest, TResponse>> logger)
        {
            _logger = logger;
        }

        /// <inheritdoc/>
        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            try
            {
                var result = await next();
                return result;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"Ошибка выполнения операции. Запрос: {JsonConvert.SerializeObject(request)}. Ошибка: {exception.Message}");
                throw;
            }
        }
    }
}
