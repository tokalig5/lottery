﻿using Common.Contracts.Logger;
using Common.Contracts.Settings;
using Common.Infrastructure.Utils.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;

namespace Common.Infrastructure.Logger
{
    /// <summary>
    /// Декоратор контекстного логгирования.
    /// </summary>
    /// <typeparam name="T">Тип логгера.</typeparam>
    public class ContextLogger<T> : ILogger<T>
    {
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly LoggerSettings _loggerSettings;
        private readonly ILogger _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Инициализация экземпляра <see cref="ContextLogger{T}"/>.
        /// </summary>
        public ContextLogger(ILoggerFactory loggerFactory, LoggerSettings loggerSettings, IHttpContextAccessor httpContextAccessor)
        {
            _logger = loggerFactory.CreateLogger(typeof(T));
            _loggerSettings = loggerSettings;
            _httpContextAccessor = httpContextAccessor;

            _serializerSettings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        /// <inheritdoc/>
        public IDisposable BeginScope<TState>(TState state)
        {
            return _logger.BeginScope(state);
        }

        /// <inheritdoc/>
        public bool IsEnabled(LogLevel logLevel)
        {
            return _logger.IsEnabled(logLevel);
        }

        /// <inheritdoc/>
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            var httpContext = _httpContextAccessor.HttpContext;
            var descriptor = new JsonLoggerDescriptor
            {
                ApplicationName = _loggerSettings.ApplicationName,
                LogLevelEnum = logLevel,
                DateTime = DateTime.UtcNow,
                Message = formatter(state, exception),
                StackTrace = exception?.StackTrace,
                Environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"),
                TraceId = httpContext.GetTraceId(),
                Path = httpContext.Request.Path.HasValue ? httpContext.Request.Path.Value : string.Empty,
                UserId = httpContext.GetUserId()
            };

            var message = JsonConvert.SerializeObject(descriptor, _serializerSettings);
            _logger.Log(logLevel, eventId, exception, message);
        }
    }
}
