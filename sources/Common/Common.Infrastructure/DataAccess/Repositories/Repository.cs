﻿using Common.Contracts.DataAccess;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.DataAccess.Repositories
{
    /// <inheritdoc cref="IRepository"/>
    public class Repository<TEntity, TContext> : ReadOnlyRepository<TEntity, TContext>, IRepository<TEntity, TContext> where TEntity : BaseEntity where TContext : DbContext
    {
        /// <summary>
        /// Инициализация экземпляра <see cref="Repository"/>
        /// </summary>
        /// <param name="context"></param>
        public Repository(TContext context) : base(context)
        {
        }

        /// <inheritdoc/>
        public async Task AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            await EntitySet.AddAsync(entity, cancellationToken);
            await SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public async Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            await EntitySet.AddRangeAsync(entities, cancellationToken);
            await SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public Task UpdateAsync(TEntity entity, CancellationToken cancellationToken)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                Context.Attach(entity);
            }

            EntitySet.Update(entity);
            return SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public async Task DeleteAsync(object id, CancellationToken cancellationToken)
        {
            var entity = await EntitySet.FindAsync(id, cancellationToken);
            if (entity != null)
            {
                EntitySet.Remove(entity);
                await SaveChangesAsync(cancellationToken);
            }
        }

        /// <inheritdoc/>
        public Task SaveChangesAsync(CancellationToken cancellationToken)
        {
            return Context.SaveChangesAsync(cancellationToken);
        }
    }
}
