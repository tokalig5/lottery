﻿using Common.Contracts.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.DataAccess.Repositories
{
    /// <inheritdoc cref="IReadOnlyRepository"/>
    public class ReadOnlyRepository<TEntity, TContext> : IReadOnlyRepository<TEntity, TContext> where TEntity : BaseEntity where TContext : DbContext
    {
        protected readonly TContext Context;
        protected readonly DbSet<TEntity> EntitySet;

        /// <summary>
        /// Инициализирует экземпляр <see cref="ReadOnlyRepository"/>
        /// </summary>
        /// <param name="context">Контекст подключения.</param>
        public ReadOnlyRepository(TContext context)
        {
            Context = context;
            EntitySet = Context.Set<TEntity>();
        }

        /// <inheritdoc/>
        public Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken)
        {
            return EntitySet.AnyAsync(predicate, cancellationToken);
        }

        /// <inheritdoc/>
        public IQueryable<TEntity> AsQueryable()
        {
            return EntitySet;
        }

        /// <inheritdoc/>
        public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken)
        {
            return EntitySet.CountAsync(predicate, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<TEntity> GetByIdAsync(object id, CancellationToken cancellationToken)
        {
            return await EntitySet.FindAsync(new[] { id }, cancellationToken);
        }
    }
}
