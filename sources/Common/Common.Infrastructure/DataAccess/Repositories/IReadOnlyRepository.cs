﻿using Common.Contracts.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий чтения данных.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности.</typeparam>
    /// <typeparam name="TContext">Тип контекста.</typeparam>
    public interface IReadOnlyRepository<TEntity, TContext> where TEntity : BaseEntity where TContext : DbContext
    {
        /// <summary>
        /// Получение сущности по ее идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор сущности.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Сущность <see cref="TEntity"/></returns>
        Task<TEntity> GetByIdAsync(object id, CancellationToken cancellationToken);

        /// <summary>
        /// Получение количества элементов.
        /// </summary>
        /// <param name="predicate">Предикат выборки.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Количество элементов.</returns>
        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken);

        /// <summary>
        /// Проверка наличия сущности.
        /// </summary>
        /// <param name="predicate">Предикат.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Результат наличия сущности.</returns>
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken);

        /// <summary>
        /// Возвращение нематериализованного запроса.
        /// </summary>
        /// <returns>Нематериализованный запрос.</returns>
        IQueryable<TEntity> AsQueryable();
    }
}