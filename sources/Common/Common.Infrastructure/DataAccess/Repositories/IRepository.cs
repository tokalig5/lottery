﻿using Common.Contracts.DataAccess;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Infrastructure.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий чтения-записи данных.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности БД.</typeparam>
    /// <typeparam name="TContext">Тип контекста БД.</typeparam>
    public interface IRepository<TEntity, TContext> : IReadOnlyRepository<TEntity, TContext> where TEntity : BaseEntity where TContext : DbContext
    {
        /// <summary>
        /// Добавление сущности.
        /// </summary>
        /// <param name="entity">Сущность.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        Task AddAsync(TEntity entity, CancellationToken cancellationToken);

        /// <summary>
        /// Добавление массива сущностей.
        /// </summary>
        /// <param name="entities">Массив сущностей.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns></returns>
        Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken);

        /// <summary>
        /// Обновление сущности.
        /// </summary>
        /// <param name="entity">Сущность.</param>
        /// <param name="cancellationToken">токен отмены.</param>
        Task UpdateAsync(TEntity entity, CancellationToken cancellationToken);

        /// <summary>
        /// Удаление сущности.
        /// </summary>
        /// <param name="id">Идентификатор сущности.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        Task DeleteAsync(object id, CancellationToken cancellationToken);

        /// <summary>
        /// Сохранение изменений.
        /// </summary>
        /// <param name="cancellationToken">Токен отмены.</param>
        Task SaveChangesAsync(CancellationToken cancellationToken);
    }
}
