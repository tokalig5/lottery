﻿using File.ApiClient.Clients;
using File.Contracts;
using Microsoft.AspNetCore.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Gateway.AppServices.File
{
    /// <inheritdoc cref="IFileService"/>.
    public class FileService : IFileService
    {
        private readonly IFileApiClient _apiClient;

        /// <summary>
        /// Инициализация экземпляра <see cref="FileService"/>.
        /// </summary>
        public FileService(IFileApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        /// <inheritdoc/>
        public Task<string> UploadAsync(IFormFile file, CancellationToken cancellationToken)
        {
            return _apiClient.UploadAsync(file, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<FileStreamModel> DownloadFileAsync(string id, CancellationToken cancellationToken)
        {
            return _apiClient.DownloadFileAsync(id, cancellationToken);
        }

        /// <inheritdoc/>
        public Task DeleteAsync(string id, CancellationToken cancellationToken)
        {
            return _apiClient.DeleteAsync(id, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<FileInfoDto> GetFileInfoAsync(string id, CancellationToken cancellationToken)
        {
            return _apiClient.GetFileInfoAsync(id, cancellationToken);
        }
    }
}
