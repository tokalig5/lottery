﻿using AutoMapper;
using Gateway.Contracts.Application;
using Lottery.ApiClient.Clients;
using System.Threading;
using System.Threading.Tasks;
using LotteryContracts = Lottery.Contracts.Models;

namespace Gateway.AppServices.Application
{
    /// <inheritdoc cref="IApplicationService"/>
    public class ApplicationService : IApplicationService
    {
        private readonly IMapper _mapper;
        private readonly ILotteryApiClient _lotteryApiClient;

        public ApplicationService(ILotteryApiClient lotteryApiClient, IMapper mapper)
        {
            _lotteryApiClient = lotteryApiClient;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public Task CreateApplicationAsync(ApplicationCreateModel model, CancellationToken cancellationToken)
        {
            var requestModel = _mapper.Map<LotteryContracts.ApplicationCreateModel>(model);
            return _lotteryApiClient.CreateApplicationAsync(requestModel, cancellationToken);
        }
    }
}
