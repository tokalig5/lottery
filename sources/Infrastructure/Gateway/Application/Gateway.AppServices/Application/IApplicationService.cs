﻿using Gateway.Contracts.Application;
using System.Threading;
using System.Threading.Tasks;

namespace Gateway.AppServices.Application
{
    /// <summary>
    /// Сервис по работе с заявками.
    /// </summary>
    public interface IApplicationService
    {
        /// <summary>
        /// Создание заявки на участие.
        /// </summary>
        /// <param name="model">Модель создания заявки на участие.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        Task CreateApplicationAsync(ApplicationCreateModel model, CancellationToken cancellationToken);
    }
}
