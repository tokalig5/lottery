﻿using Gateway.Contracts.Lot;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Gateway.AppServices.Lot
{
    /// <summary>
    /// Сервис по работе с лотереями.
    /// </summary>
    public interface ILotService
    {
        /// <summary>
        /// Создание лотереи.
        /// </summary>
        /// <param name="model">Модель создания лотереи.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        Task<Guid> CreateLotAsync(LotCreateModel model, CancellationToken cancellationToken);
    }
}
