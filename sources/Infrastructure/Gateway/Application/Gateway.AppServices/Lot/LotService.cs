﻿using AutoMapper;
using Gateway.Contracts.Lot;
using Lottery.ApiClient.Clients;
using System;
using System.Threading;
using System.Threading.Tasks;
using LotteryContracts = Lottery.Contracts.Models;

namespace Gateway.AppServices.Lot
{
    /// <inheritdoc cref="ILotService"/>
    public class LotService : ILotService
    {
        private readonly IMapper _mapper;
        private readonly ILotteryApiClient _lotteryApiClient;

        public LotService(ILotteryApiClient lotteryApiClient, IMapper mapper)
        {
            _lotteryApiClient = lotteryApiClient;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public Task<Guid> CreateLotAsync(LotCreateModel model, CancellationToken cancellationToken)
        {
            var requestModel = _mapper.Map<LotteryContracts.LotCreateModel>(model);
            return _lotteryApiClient.CreateLotAsync(requestModel, cancellationToken);
        }
    }
}
