﻿using Gateway.AppServices.Lot;
using Gateway.Contracts.Lot;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using User.Contracts;

namespace Gateway.Api.Controllers
{
    /// <summary>
    /// Контроллер для работы с лотереями.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class LotController : ControllerBase
    {
        private readonly ILotService _lotService;

        /// <inheritdoc cref="LotController"/>
        public LotController(ILotService lotService)
        {
            _lotService = lotService;
        }

        /// <summary>
        /// Создание лотереи.
        /// </summary>
        /// <param name="model">Модель создания лотереи.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        [HttpPost]
        //[Authorize]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status201Created)]
        public async Task<IActionResult> Post([Required] LotCreateModel model, CancellationToken cancellationToken)
        {
            var result = await _lotService.CreateLotAsync(model, cancellationToken);
            return StatusCode((int)HttpStatusCode.Created, result);
        }

        [HttpGet("2")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public async Task<IActionResult> Post23(CancellationToken cancellationToken)
        {
            return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
        }
    }
}
