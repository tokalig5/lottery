﻿using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace Gateway.Api.Controllers
{
    /// <summary>
    /// Notification controller
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class NotificationController : ControllerBase
    {
        /// <summary>
        /// Get notification controller info
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get(CancellationToken cancellationToken)
        {
            return Ok($"{nameof(NotificationController)} is working");
        }
    }
}
