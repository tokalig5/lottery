﻿using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace Gateway.Api.Controllers
{
    /// <summary>
    /// Finance controller
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class FinanceController : ControllerBase
    {
        /// <summary>
        /// Get finance controller info
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get(CancellationToken cancellationToken)
        {
            return Ok($"{nameof(FinanceController)} is working");
        }
    }
}
