﻿using Common.Contracts.WebApi.Models;
using File.Contracts;
using Gateway.AppServices.File;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Gateway.Api.Controllers
{
    /// <summary>
    /// File controller
    /// </summary>
    [ApiController]
    [ProducesResponseType(typeof(PublicApiError), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(PublicApiError), StatusCodes.Status500InternalServerError)]
    [Route("api/v1/[controller]")]
    public class FileController : ControllerBase
    {
        private readonly IFileService _fileService;

        /// <summary>
        /// Инициализация экземпляра <see cref="FileController"/>.
        /// </summary>        
        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }

        /// <summary>
        /// Загрузка файла в систему.
        /// </summary>
        /// <param name="file">Файл.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        [HttpPost]
        [ProducesResponseType(typeof(string), StatusCodes.Status201Created)]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = long.MaxValue)]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Upload(IFormFile file, CancellationToken cancellationToken)
        {
            var result = await _fileService.UploadAsync(file, cancellationToken);
            return StatusCode((int)HttpStatusCode.Created, result);
        }

        /// <summary>
        /// Скачивание файла по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Файл в виде потока.</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(byte[]), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(PublicApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Download(string id, CancellationToken cancellationToken)
        {
            var result = await _fileService.DownloadFileAsync(id, cancellationToken);
            Response.ContentLength = result.Info.Length;
            return File(result.Stream, result.Info.ContentType, result.Info.Name, true);
        }

        /// <summary>
        /// Удаление файла по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(PublicApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(string id, CancellationToken cancellationToken)
        {
            await _fileService.DeleteAsync(id, cancellationToken);
            return NoContent();
        }

        /// <summary>
        /// Получение информации о файле по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Информация о файле.</returns>
        [HttpGet("{id}/info")]
        [ProducesResponseType(typeof(FileInfoDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(PublicApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetInfoById(string id, CancellationToken cancellationToken)
        {
            var result = await _fileService.GetFileInfoAsync(id, cancellationToken);
            return Ok(result);
        }
    }
}
