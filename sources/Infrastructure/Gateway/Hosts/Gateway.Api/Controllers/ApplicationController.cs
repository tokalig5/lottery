﻿using Gateway.AppServices.Application;
using Gateway.Contracts.Application;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace Gateway.Api.Controllers
{
    /// <summary>
    /// Контроллер для работы с заявками.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ApplicationController : ControllerBase
    {
        private readonly IApplicationService _applicationService;

        /// <inheritdoc cref="ApplicationController"/>
        public ApplicationController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        /// <summary>
        /// Создание заявки.
        /// </summary>
        /// <param name="model">Модель создания заявки на участие в лотерее.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        [HttpPost]
        public async Task<IActionResult> Post([Required] ApplicationCreateModel model, CancellationToken cancellationToken)
        {
            await _applicationService.CreateApplicationAsync(model, cancellationToken);
            return Ok();
        }
    }
}
