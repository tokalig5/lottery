﻿using AutoMapper;
using Common.Contracts.Settings;
using Common.Infrastructure;
using Common.Infrastructure.Utils.Middlewares;
using File.ApiClient;
using Gateway.AppServices.Application;
using Gateway.AppServices.File;
using Gateway.AppServices.Lot;
using Gateway.ComponentRegistrar.MapProfiles;
using Gateway.Contracts.Settings;
using IdentityServer4.AccessTokenValidation;
using Lottery.ApiClient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using System.Security.Claims;

namespace Gateway.ComponentRegistrar
{
    public static class GatewayRegistrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                           .ConfigureSwaggerDoc()
                           .ConfigureServices()
                           .ConfigureAuthorization(configuration)
                           .ConfigureAutoMapper()
                           .AddApiClients(configuration)
                           .AddContextLogger(configuration);
        }

        private static IServiceCollection ConfigureSwaggerDoc(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Lottery API",
                    Description = "Lottery service Web API",
                    Contact = new OpenApiContact
                    {
                        Name = "Aleksei Tokarev",
                        Email = "tokalig5@gmail.com",
                        Url = new Uri("https://www.linkedin.com/in/aleksei-tokarev-093a1b177/"),
                    }
                });

                var xmlFile = $"{Assembly.GetEntryAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            return services;
        }

        private static IServiceCollection ConfigureAuthorization(this IServiceCollection services, IConfiguration configuration)
        {
            var identitySettings = new IdentitySettings();
            configuration.Bind("IdentityServer", identitySettings);

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .AddIdentityServerAuthentication(options =>
                    {
                        options.Authority = identitySettings.Authority;
                        options.ApiName = identitySettings.ApiName;
                        options.ApiSecret = identitySettings.ApiSecret;
                        options.EnableCaching = identitySettings.EnableCaching;
                        options.LegacyAudienceValidation = true;
                        options.RoleClaimType = ClaimTypes.Role;
                        options.RequireHttpsMetadata = true;
                    });

            // IS выдает токен для конкретного клиента с конкретными скоупами, и тут проверяется с какими именно скоупами сюда могут приходить
            services.AddAuthorization(options =>
            {
                options.AddPolicy(identitySettings.Policy, policy =>
                {
                    policy.RequireScope(identitySettings.Scope);
                });
            });
            return services;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddTransient<IApplicationService, ApplicationService>();
            services.AddTransient<ILotService, LotService>();
            services.AddTransient<IFileService, FileService>();
            return services;
        }

        private static IServiceCollection ConfigureAutoMapper(this IServiceCollection services)
        {
            return services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
        }

        private static IServiceCollection AddApiClients(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<HttpRequestHandler>();

            var proxySettings = new ProxySettings();
            configuration.Bind(nameof(ProxySettings), proxySettings);

            services.AddLotteryApiClient(proxySettings);
            services.AddFileApiClient(proxySettings);
            return services;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<LotProfile>();
                cfg.AddProfile<ApplicationProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}