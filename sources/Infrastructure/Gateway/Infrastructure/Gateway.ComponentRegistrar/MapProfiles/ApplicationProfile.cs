﻿using AutoMapper;

namespace Gateway.ComponentRegistrar.MapProfiles
{
    /// <summary>
    /// Профайл для маппинга моделей заявок.
    /// </summary>
    public class ApplicationProfile : Profile
    {
        public ApplicationProfile()
        {
            CreateMap<Contracts.Application.ApplicationCreateModel, Lottery.Contracts.Models.ApplicationCreateModel>();
        }
    }
}
