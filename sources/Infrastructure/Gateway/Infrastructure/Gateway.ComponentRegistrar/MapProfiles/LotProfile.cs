﻿using AutoMapper;

namespace Gateway.ComponentRegistrar.MapProfiles
{
    /// <summary>
    /// Профайл для маппинга моделей лотереи.
    /// </summary>
    public class LotProfile : Profile
    {
        public LotProfile()
        {
            CreateMap<Contracts.Lot.LotCreateModel, Lottery.Contracts.Models.LotCreateModel>();
        }
    }
}
