﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gateway.Contracts.Application
{
    /// <summary>
    /// Модель создания заявки на участие в лотерее.
    /// </summary>
    public class ApplicationCreateModel
    {
        /// <summary>
        /// Идентификатор лотереи.
        /// </summary>
        [Required]
        public Guid LotId { get; set; }
    }
}
