﻿using System.ComponentModel.DataAnnotations;

namespace Gateway.Contracts.Lot
{
    /// <summary>
    /// Модель создания лотереи.
    /// </summary>
    public class LotCreateModel
    {
        /// <summary>
        /// Наименование лотереи.
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
