﻿namespace Gateway.Contracts.Settings
{
    /// <summary>
    /// Настройки для подключения к IdentityServer.
    /// </summary>
    public class IdentitySettings
    {
        /// <summary>
        /// Адрес IdentityServer.
        /// </summary>
        public string Authority { get; set; }

        /// <summary>
        /// Наименование API.
        /// </summary>
        public string ApiName { get; set; }

        /// <summary>
        /// Секрет.
        /// </summary>
        public string ApiSecret { get; set; }

        /// <summary>
        /// Policy.
        /// </summary>
        public string Policy { get; set; }

        /// <summary>
        /// Scope.
        /// </summary>
        public string Scope { get; set; }

        /// <summary>
        /// Включить кэширование.
        /// </summary>
        public bool EnableCaching { get; set; }
    }
}
