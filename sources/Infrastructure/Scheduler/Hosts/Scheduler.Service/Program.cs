using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;

namespace Scheduler.Service
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await CreateWebHost(args).Build().RunAsync();
        }

        private static IHostBuilder CreateWebHost(string[] args) => Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder =>
        {
            webBuilder.ConfigureAppConfiguration((hostingContext, config) =>
            {
                config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                config.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true);
                config.AddJsonFile("secrets/appsettings.secrets.json", optional: true);
                config.AddEnvironmentVariables();
                config.AddCommandLine(args);
            }).UseKestrel().UseStartup<Startup>();
        });
    }
}