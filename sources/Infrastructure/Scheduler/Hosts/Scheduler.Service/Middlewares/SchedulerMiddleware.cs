﻿using CrystalQuartz.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Scheduler.AppServices.Scheduler.Services;

namespace Scheduler.Service.Middlewares
{
    /// <summary>
    /// Middleware работы сервиса планировщика.
    /// </summary>
    public static class SchedulerMiddleware
    {
        /// <summary>
        /// Использование планировщика.
        /// </summary>
        /// <param name="builder">Билдер приложения.</param>        
        public static IApplicationBuilder UseScheduler(this IApplicationBuilder builder)
        {
            using var scope = builder.ApplicationServices.CreateScope();
            var scheduler = scope.ServiceProvider.GetService<ISchedulerService>().CreateScheduler();
            builder.UseCrystalQuartz(() => scheduler);
            return builder;
        }
    }
}
