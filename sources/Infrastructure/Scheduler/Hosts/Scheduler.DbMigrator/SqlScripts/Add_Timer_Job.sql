-------------------------------------------------
--ДОБАВЛЕНИЕ РАСШИРЕНИЯ ДЛЯ ФОРМИРОВАНИЯ ГУИДОВ--
-------------------------------------------------
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-----------------------------------
--ДОБАВЛЕНИЕ ЗАДАЧИ В ПЛАНИРОВЩИК--
-----------------------------------

DO $$
DECLARE jobId uuid := uuid_generate_v4();
DECLARE jobTriggerId uuid := uuid_generate_v4();

BEGIN
  INSERT INTO "Job" ("Id", "Name", "Group", "Url", "Description", "Created", "Disabled") VALUES 
  (jobId, 'Time updating on client side', 'Time', 'SignalRServiceUrl/api/v1/time/notify', 'Time updating on client side using SignalR every 5 seconds', NOW() AT TIME ZONE 'UTC', false) ON CONFLICT DO NOTHING;

  INSERT INTO "JobTrigger" ("Id", "Name", "Description", "Cron", "JobId") VALUES 
  (jobTriggerId, 'Time updating trigger', 'Time updates every 5 secondes using SignalR', '0/5 * * * * ?', jobId) ON CONFLICT DO NOTHING;
END $$;