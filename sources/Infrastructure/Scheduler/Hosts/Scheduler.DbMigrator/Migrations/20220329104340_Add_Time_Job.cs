﻿using Common.Infrastructure.Utils.Helpers;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Scheduler.DbMigrator.Migrations
{
    public partial class Add_Time_Job : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = SqlHelper.GetSqlQuery("SqlScripts.Add_Timer_Job.sql");
            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
