﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Scheduler.DbMigrator.Migrations
{
    public partial class Rename_Exists_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobTrigger_Job_JobId",
                table: "JobTrigger");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JobTrigger",
                table: "JobTrigger");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Job",
                table: "Job");

            migrationBuilder.RenameTable(
                name: "JobTrigger",
                newName: "JobTriggers");

            migrationBuilder.RenameTable(
                name: "Job",
                newName: "Jobs");

            migrationBuilder.RenameIndex(
                name: "IX_JobTrigger_JobId",
                table: "JobTriggers",
                newName: "IX_JobTriggers_JobId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JobTriggers",
                table: "JobTriggers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Jobs",
                table: "Jobs",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JobTriggers_Jobs_JobId",
                table: "JobTriggers",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobTriggers_Jobs_JobId",
                table: "JobTriggers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JobTriggers",
                table: "JobTriggers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Jobs",
                table: "Jobs");

            migrationBuilder.RenameTable(
                name: "JobTriggers",
                newName: "JobTrigger");

            migrationBuilder.RenameTable(
                name: "Jobs",
                newName: "Job");

            migrationBuilder.RenameIndex(
                name: "IX_JobTriggers_JobId",
                table: "JobTrigger",
                newName: "IX_JobTrigger_JobId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JobTrigger",
                table: "JobTrigger",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Job",
                table: "Job",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JobTrigger_Job_JobId",
                table: "JobTrigger",
                column: "JobId",
                principalTable: "Job",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
