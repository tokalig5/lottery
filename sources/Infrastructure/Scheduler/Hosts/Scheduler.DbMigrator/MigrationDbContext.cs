﻿using Microsoft.EntityFrameworkCore;
using Scheduler.DataAccess;

namespace Scheduler.DbMigrator
{
    /// <summary>
    /// Контекст для миграций.
    /// </summary>
    public class MigrationDbContext : SchedulerBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="MigrationDbContext"/>.
        /// </summary>
        public MigrationDbContext(DbContextOptions<MigrationDbContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
    }
}
