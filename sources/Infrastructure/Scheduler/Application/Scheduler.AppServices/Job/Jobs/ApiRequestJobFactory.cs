﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Spi;
using System;

namespace Scheduler.AppServices.Job.Jobs
{
    /// <summary>
    /// Фабрика задачи планировщика.
    /// </summary>
    public class ApiRequestJobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// Инициализация экземпляра <see cref="ApiRequestJobFactory"/>.
        /// </summary>
        /// <param name="serviceProvider">Провайдер сервисов.</param>
        public ApiRequestJobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <inheritdoc/>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _serviceProvider.GetService<ApiRequestJob>();
        }

        /// <inheritdoc/>
        public void ReturnJob(IJob job)
        {
            var disposable = job as IDisposable;
            disposable?.Dispose();
        }
    }
}
