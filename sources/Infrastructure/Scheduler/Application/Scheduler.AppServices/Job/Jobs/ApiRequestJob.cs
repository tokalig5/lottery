﻿using Common.Infrastructure.Utils.Extensions;
using Quartz;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Scheduler.Exceptions;

namespace Scheduler.AppServices.Job.Jobs
{
    /// <summary>
    /// Задача на отправку запроса в API.
    /// </summary>
    [DisallowConcurrentExecution]
    [PersistJobDataAfterExecution]
    public class ApiRequestJob : IJob
    {
        private readonly HttpClient _httpClient;

        /// <summary>
        /// Инициализация экземпляра <see cref="ApiRequestJob"/>.
        /// </summary>
        /// <param name="httpClient">HTTP-клиент.</param>
        public ApiRequestJob(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <inheritdoc/>
        public Task Execute(IJobExecutionContext context)
        {
            context.JobDetail.JobDataMap.TryGetValue(nameof(Entities.Job.Url), out var value);
            var url = value?.ToString();

            return _httpClient.PostAsync(url,
            (statusCode, message, exception) => throw new SchedulerApiRequestException(statusCode, $"Ошибка отправки планировщиком запроса по адресу {url}. Ошибка: {message}", exception),
            CancellationToken.None);        }
    }
}
