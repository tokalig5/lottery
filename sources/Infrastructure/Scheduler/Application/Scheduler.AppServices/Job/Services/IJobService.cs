﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduler.AppServices.Job.Services
{
    /// <summary>
    /// Сервис по работе с задачами планировщика.
    /// </summary>
    public interface IJobService
    {
        /// <summary>
        /// Получение всех активных задач планировщика.
        /// </summary>
        /// <returns>Список активных задач планировщика.</returns>
        Task<List<Entities.Job>> GetAllEnabledAsync();
    }
}
