﻿using Scheduler.AppServices.Job.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduler.AppServices.Job.Services
{
    /// <inheritdoc cref="IJobService"/>
    public class JobService : IJobService
    {
        private readonly IJobRepository _jobRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="JobService"/>.
        /// </summary>
        /// <param name="jobRepository">Репозиторий по радоте с задачами планировщика.</param>
        public JobService(IJobRepository jobRepository)
        {
            _jobRepository = jobRepository;
        }

        /// <inheritdoc/>
        public Task<List<Entities.Job>> GetAllEnabledAsync()
        {
            return _jobRepository.GetAllEnabledAsync();
        }
    }
}
