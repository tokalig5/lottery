﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduler.AppServices.Job.Repositories
{
    /// <summary>
    /// Репозиторий по радоте с задачами планировщика.
    /// </summary>
    public interface IJobRepository
    {
        /// <summary>
        /// Получение всех активных задач планировщика.
        /// </summary>
        /// <returns>Список активных задач планировщика.</returns>
        Task<List<Entities.Job>> GetAllEnabledAsync();
    }
}
