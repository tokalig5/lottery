﻿using Quartz;

namespace Scheduler.AppServices.Scheduler.Services
{
    /// <summary>
    /// Сервис по работе планировщика.
    /// </summary>
    public interface ISchedulerService
    {
        /// <summary>
        /// Создание планировщика.
        /// </summary>
        /// <returns>Планировщик.</returns>
        IScheduler CreateScheduler();
    }
}
