﻿using Common.Contracts.Settings;
using Common.Infrastructure.Utils.Helpers;
using Quartz;
using Quartz.Spi;
using Scheduler.AppServices.Job.Jobs;
using Scheduler.AppServices.Job.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduler.AppServices.Scheduler.Services
{
    /// <inheritdoc cref="ISchedulerService"/>.
    public class SchedulerService : ISchedulerService
    {
        private readonly IJobService _jobService;
        private readonly IJobFactory _jobFactory;
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly ProxySettings _proxySettings;

        /// <summary>
        /// Инициализация экземпляра <see cref="SchedulerService"/>.
        /// </summary>
        /// <param name="jobService">Сервис по работе с задачами планировщика.</param>
        /// <param name="jobFactory">Фабрика задач планировщика.</param>
        /// <param name="schedulerFactory">Фабрика планировщика.</param>
        /// <param name="proxySettings">Настройки для работы с прокси.</param>
        public SchedulerService(IJobService jobService, IJobFactory jobFactory, ISchedulerFactory schedulerFactory, ProxySettings proxySettings)
        {
            _jobService = jobService;
            _jobFactory = jobFactory;
            _proxySettings = proxySettings;
            _schedulerFactory = schedulerFactory;
        }

        /// <inheritdoc/>
        public IScheduler CreateScheduler()
        {
            return TaskHelper.RunAsSync(() => CreateSchedulerAsync());
        }

        private async Task<IScheduler> CreateSchedulerAsync()
        {
            var scheduler = await _schedulerFactory.GetScheduler();
            scheduler.JobFactory = _jobFactory;

            var jobs = await GetJobsAsync();
            foreach (var job in jobs)
            {
                var data = new Dictionary<string, string> { { nameof(Entities.Job.Url), job.Url } };
                var jobDetail = JobBuilder.Create<ApiRequestJob>().WithIdentity(job.Name, job.Group).WithDescription(job.Description).StoreDurably().SetJobData(new JobDataMap(data)).Build();
                var trigger = TriggerBuilder.Create().WithIdentity(job.Trigger.Name).WithDescription(job.Trigger.Description).StartNow().WithCronSchedule(job.Trigger.Cron).Build();
                await scheduler.ScheduleJob(jobDetail, trigger);
            }

            await scheduler.Start();
            return scheduler;
        }

        private async Task<List<Entities.Job>> GetJobsAsync()
        {
            var jobs = await _jobService.GetAllEnabledAsync();
            jobs.ForEach(job => job.Url = GetFormattedUrl(job.Url).ToString());
            return jobs;
        }

        private Uri GetFormattedUrl(string sourceUrl)
        {
            var urlPrefix = sourceUrl[..sourceUrl.IndexOf('/')];
            var property = _proxySettings.GetType().GetProperty(urlPrefix);
            if (property == null)
            {
                return new Uri(sourceUrl);
            }

            var value = property.GetValue(_proxySettings, null)?.ToString();
            return new Uri(sourceUrl.Replace(urlPrefix, value));
        }
    }
}
