﻿using Common.Contracts.Settings;
using Common.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Scheduler.AppServices.Job.Jobs;
using Scheduler.AppServices.Job.Repositories;
using Scheduler.AppServices.Job.Services;
using Scheduler.AppServices.Scheduler.Services;
using Scheduler.Contracts.Constants;
using Scheduler.Contracts.Settings;
using Scheduler.DataAccess;
using Scheduler.DataAccess.Repositories;
using System.Collections.Specialized;

namespace Scheduler.ComponentRegistrar
{
    /// <summary>
    /// Регистратор сервиса планировщика.
    /// </summary>
    public static class SchedulerRegistrar
    {
        /// <summary>
        /// Добавление сервисов.
        /// </summary>
        /// <param name="services">Сервисы.</param>
        /// <param name="configuration">Конфигурации.</param>        
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                           .ConfigureServices(configuration)
                           .ConfigureDbConnections(configuration)
                           .ConfigureRepositories();
        }

        private static IServiceCollection ConfigureDbConnections(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbConfiguration<DataAccess.SchedulerContext, SchedulerReadOnlyContext>(configuration);
            return services;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var proxySettings = new ProxySettings();
            configuration.Bind(nameof(ProxySettings), proxySettings);
            services.AddSingleton(proxySettings);

            services.AddHttpClient<ApiRequestJob>();
            services.AddSingleton<IJobFactory, ApiRequestJobFactory>();
            services.AddSingleton<ISchedulerFactory>(sp =>
            {
                var settings = new SchedulerSettings();
                configuration.Bind(nameof(SchedulerSettings), settings);

                var properties = new NameValueCollection();
                properties.Set(SettingsNameConstants.InstanceName, settings.InstanceName);

                return new StdSchedulerFactory(properties);
            });



            services.AddTransient<ISchedulerService, SchedulerService>();
            services.AddTransient<IJobService, JobService>();
            return services;
        }

        private static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            services.AddTransient<IJobRepository, JobRepository>();
            return services;
        }
    }
}