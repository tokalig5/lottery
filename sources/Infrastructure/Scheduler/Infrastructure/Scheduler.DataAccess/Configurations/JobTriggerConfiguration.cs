﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Scheduler.Entities;

namespace Scheduler.DataAccess.Configurations
{
    /// <summary>
    /// Настройки сущности триггера задачи.
    /// </summary>
    public class JobTriggerConfiguration : IEntityTypeConfiguration<JobTrigger>
    {
        /// <inheritdoc/>
        public void Configure(EntityTypeBuilder<JobTrigger> builder)
        {
            builder.ToTable("JobTriggers");

            builder.HasKey(s => s.Id);

            builder.Property(s => s.Name).HasMaxLength(256).IsRequired();
            builder.Property(s => s.Description).HasMaxLength(512).IsRequired();
            builder.Property(s => s.Cron).HasMaxLength(128).IsRequired();

            builder.HasOne(s => s.Job).WithOne(s => s.Trigger).HasForeignKey<JobTrigger>(s => s.JobId).IsRequired().OnDelete(DeleteBehavior.Cascade);
        }
    }
}
