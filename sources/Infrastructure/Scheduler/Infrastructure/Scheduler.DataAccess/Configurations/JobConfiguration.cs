﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Scheduler.Entities;
using System;

namespace Scheduler.DataAccess.Configurations
{
    /// <summary>
    /// Настройки сущности задачи.
    /// </summary>
    public class JobConfiguration : IEntityTypeConfiguration<Job>
    {
        /// <inheritdoc/>
        public void Configure(EntityTypeBuilder<Job> builder)
        {
            builder.ToTable("Jobs");

            builder.HasKey(s => s.Id);

            builder.Property(s => s.Name).HasMaxLength(256).IsRequired();
            builder.Property(s => s.Group).HasMaxLength(256).IsRequired();
            builder.Property(s => s.Description).HasMaxLength(512).IsRequired();
            builder.Property(s => s.Url).HasMaxLength(1024).IsRequired();
            builder.Property(s => s.Created).HasConversion(s => s, s => DateTime.SpecifyKind(s, DateTimeKind.Utc));
        }
    }
}
