﻿using Microsoft.EntityFrameworkCore;

namespace Scheduler.DataAccess
{
    /// <summary>
    /// Контекст чтения-записи БД.
    /// </summary>
    public class SchedulerContext : SchedulerBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="SchedulerReadOnlyContext"/>
        /// </summary>
        /// <param name="options">Настройки контекста.</param>
        public SchedulerContext(DbContextOptions<SchedulerContext> options) : base(options)
        {
        }
    }
}
