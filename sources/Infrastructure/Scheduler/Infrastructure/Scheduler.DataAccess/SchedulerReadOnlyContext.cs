﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Scheduler.DataAccess
{
    /// <summary>
    /// Контекст чтения БД.
    /// </summary>
    public class SchedulerReadOnlyContext : SchedulerBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="SchedulerReadOnlyContext"/>
        /// </summary>
        /// <param name="options">Настройки контекста.</param>
        public SchedulerReadOnlyContext(DbContextOptions<SchedulerReadOnlyContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
    }
}
