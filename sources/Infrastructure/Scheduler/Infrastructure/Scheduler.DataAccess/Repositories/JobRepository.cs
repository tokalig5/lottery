﻿using Common.Infrastructure.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Scheduler.AppServices.Job.Repositories;
using Scheduler.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduler.DataAccess.Repositories
{
    /// <inheritdoc cref="IJobRepository"/>.
    public class JobRepository : IJobRepository
    {
        private readonly IReadOnlyRepository<Job, SchedulerReadOnlyContext> _readOnlyRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="JobRepository"/>.
        /// </summary>
        /// <param name="readOnlyRepository">Репозиторий чтения.</param>
        public JobRepository(IReadOnlyRepository<Job, SchedulerReadOnlyContext> readOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
        }

        /// <inheritdoc/>
        public Task<List<Job>> GetAllEnabledAsync()
        {
            return _readOnlyRepository.AsQueryable().Include(s => s.Trigger).Where(s => !s.Disabled).ToListAsync();
        }
    }
}
