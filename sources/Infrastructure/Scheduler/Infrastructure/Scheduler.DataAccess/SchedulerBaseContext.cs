﻿using Microsoft.EntityFrameworkCore;
using Scheduler.DataAccess.Configurations;

namespace Scheduler.DataAccess
{
    /// <summary>
    /// Базовый контекст для работы с БД.
    /// </summary>
    public class SchedulerBaseContext : DbContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="SchedulerBaseContext"/>
        /// </summary>
        /// <param name="options"></param>
        public SchedulerBaseContext(DbContextOptions options) : base(options)
        {
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new JobConfiguration());
            modelBuilder.ApplyConfiguration(new JobTriggerConfiguration());
        }
    }
}