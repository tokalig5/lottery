﻿namespace Scheduler.Contracts.Constants
{
    /// <summary>
    /// Наименования настроек планировщика.
    /// </summary>
    public static class SettingsNameConstants
    {
        /// <summary>
        /// Наименование сервиса планировщика.
        /// </summary>
        public const string InstanceName = "quartz.scheduler.instanceName";
    }
}
