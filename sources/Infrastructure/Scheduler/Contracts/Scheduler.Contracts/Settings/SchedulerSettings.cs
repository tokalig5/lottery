﻿namespace Scheduler.Contracts.Settings
{
    /// <summary>
    /// Настройки сервиса планировщика.
    /// </summary>
    public class SchedulerSettings
    {
        /// <summary>
        /// Наименование сервиса.
        /// </summary>
        public string InstanceName { get; set; }
    }
}
