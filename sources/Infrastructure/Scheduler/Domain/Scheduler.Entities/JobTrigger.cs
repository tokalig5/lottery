﻿using Common.Contracts.DataAccess;
using System;

namespace Scheduler.Entities
{
    /// <summary>
    /// Триггер выполнения задачи.
    /// </summary>
    public class JobTrigger : BaseEntity
    {
        /// <summary>
        /// Наименование триггера.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание триггера.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Выражение CRON.
        /// </summary>
        public string Cron { get; set; }

        /// <summary>
        /// Идентификатор задачи.
        /// </summary>
        public Guid JobId { get; set; }

        /// <summary>
        /// Задача.
        /// </summary>
        public virtual Job Job { get; set; }
    }
}
