﻿using Common.Contracts.DataAccess;
using System;

namespace Scheduler.Entities
{
    /// <summary>
    /// Задача.
    /// </summary>
    public class Job : BaseEntity
    {
        /// <summary>
        /// Наименование задачи.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Наименование группы.
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Адрес для обращения.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Описание задачи.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Время создания.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Показатель активности задачи.
        /// </summary>
        public bool Disabled { get; set; }

        /// <summary>
        /// Триггер задачи.
        /// </summary>
        public virtual JobTrigger Trigger { get; set; }
    }
}