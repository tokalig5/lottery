﻿using System;
using System.Net;

namespace Scheduler.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе планировщика задач.
    /// </summary>
    [Serializable]
    public class SchedulerApiRequestException : Exception
    {
        /// <summary>
        /// Статус-код ошибки.
        /// </summary>
        public HttpStatusCode StatusCode { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="ApiClientException"/>.
        /// </summary>
        /// <param name="statusCode">HTTP-код ошибки запроса.</param>
        /// <param name="message">Сообщение ошибки запроса.</param>
        /// <param name="innerException">Внутреннее исключение.</param>
        public SchedulerApiRequestException(HttpStatusCode statusCode, string message, Exception innerException) : base(message, innerException)
        {
            StatusCode = statusCode;
        }
    }
}