﻿using MediatR;
using System.IO;

namespace File.Handlers.File.Commands
{
    /// <summary>
    /// Команда на загрузку файла.
    /// </summary>
    public class FileUploadCommand : IRequest<string>
    {
        /// <summary>
        /// Наименование файла.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// ContentType файла.
        /// </summary>
        public string ContentType { get; }

        /// <summary>
        /// Поток загрузки файла.
        /// </summary>
        public Stream Stream { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="FileUploadCommand"/>.
        /// </summary>
        public FileUploadCommand(string fileName, string contentType, Stream stream)
        {
            FileName = fileName;
            ContentType = contentType;
            Stream = stream;
        }
    }
}
