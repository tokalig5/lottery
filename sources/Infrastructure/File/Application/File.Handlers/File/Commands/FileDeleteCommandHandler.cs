﻿using File.AppServices.File.Services;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace File.Handlers.File.Commands
{
    /// <summary>
    /// Обработчик команды на удаление файла по идентификатору.
    /// </summary>
    public class FileDeleteCommandHandler : IRequestHandler<FileDeleteCommand, bool>
    {
        private readonly IFileService _fileService;

        /// <summary>
        /// Инициализация экземпляра <see cref="FileDeleteCommandHandler"/>.
        /// </summary>
        public FileDeleteCommandHandler(IFileService fileService)
        {
            _fileService = fileService;
        }

        /// <inheritdoc/>
        public Task<bool> Handle(FileDeleteCommand request, CancellationToken cancellationToken)
        {
            return _fileService.DeleteAsync(request.Id, cancellationToken);
        }
    }
}
