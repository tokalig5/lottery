﻿using File.AppServices.File.Services;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace File.Handlers.File.Commands
{
    /// <summary>
    /// Оюработчик команды на загрузку файла.
    /// </summary>
    public class FileUploadCommandHandler : IRequestHandler<FileUploadCommand, string>
    {
        private readonly IFileService _fileService;

        /// <summary>
        /// Инициализация экземпляра <see cref="FileUploadCommandHandler"/>.
        /// </summary>
        public FileUploadCommandHandler(IFileService fileService)
        {
            _fileService = fileService;
        }

        /// <inheritdoc/>
        public Task<string> Handle(FileUploadCommand request, CancellationToken cancellationToken)
        {
            return _fileService.UploadFileAsync(request.FileName, request.ContentType, request.Stream, cancellationToken);
        }
    }
}
