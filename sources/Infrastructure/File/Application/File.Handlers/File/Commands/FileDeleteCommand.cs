﻿using MediatR;

namespace File.Handlers.File.Commands
{
    /// <summary>
    /// Команда на удаление файла по идентификатору.
    /// </summary>
    public class FileDeleteCommand : IRequest<bool>
    {
        /// <summary>
        /// Идентификатор файла.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="FileDeleteCommand"/>.
        /// </summary>
        public FileDeleteCommand(string id)
        {
            Id = id;
        }
    }
}
