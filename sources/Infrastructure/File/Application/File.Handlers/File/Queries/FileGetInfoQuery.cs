﻿using File.Contracts;
using MediatR;

namespace File.Handlers.File.Queries
{
    /// <summary>
    /// Получение информации о файле по его идентификатору.
    /// </summary>
    public class FileGetInfoQuery : IRequest<FileInfoDto>
    {
        /// <summary>
        /// Идентификатор файла.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="FileGetInfoQuery"/>.
        /// </summary>
        public FileGetInfoQuery(string id)
        {
            Id = id;
        }
    }
}
