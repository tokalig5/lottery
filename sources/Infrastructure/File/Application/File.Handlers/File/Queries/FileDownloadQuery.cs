﻿using File.Contracts;
using MediatR;

namespace File.Handlers.File.Queries
{
    /// <summary>
    /// Запрос на скачивание файла.
    /// </summary>
    public class FileDownloadQuery : IRequest<FileStreamModel>
    {
        /// <summary>
        /// Идентификатор файла.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="FileDownloadQuery"/>.
        /// </summary>
        public FileDownloadQuery(string id)
        {
            Id = id;
        }
    }
}
