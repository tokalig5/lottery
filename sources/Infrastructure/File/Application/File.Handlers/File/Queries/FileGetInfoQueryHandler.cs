﻿using File.AppServices.File.Services;
using File.Contracts;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace File.Handlers.File.Queries
{
    /// <summary>
    /// Оюработчик запроса на получение информации о файле по его идентификатору.
    /// </summary>
    public class FileGetInfoQueryHandler : IRequestHandler<FileGetInfoQuery, FileInfoDto>
    {
        private readonly IFileService _fileService;

        /// <summary>
        /// Инициализация экземпляра <see cref="FileGetInfoQueryHandler"/>.
        /// </summary>
        public FileGetInfoQueryHandler(IFileService fileService)
        {
            _fileService = fileService;
        }

        /// <inheritdoc/>
        public Task<FileInfoDto> Handle(FileGetInfoQuery request, CancellationToken cancellationToken)
        {
            return _fileService.GetFileInfoAsync(request.Id, cancellationToken);
        }
    }
}
