﻿using File.AppServices.File.Services;
using File.Contracts;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace File.Handlers.File.Queries
{
    /// <summary>
    /// Обработчик запроса на скачивание файла.
    /// </summary>
    public class FileDownloadQueryHandler : IRequestHandler<FileDownloadQuery, FileStreamModel>
    {
        private readonly IFileService _fileService;

        /// <summary>
        /// Инициализация экземпляра <see cref="FileDownloadQueryHandler"/>.
        /// </summary>
        public FileDownloadQueryHandler(IFileService fileService)
        {
            _fileService = fileService;
        }

        /// <inheritdoc/>
        public Task<FileStreamModel> Handle(FileDownloadQuery request, CancellationToken cancellationToken)
        {
            return _fileService.DownloadFileAsync(request.Id, cancellationToken);
        }
    }
}
