﻿using File.Contracts;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace File.AppServices.File.Services
{
    /// <summary>
    /// Сервис по работе с файлами.
    /// </summary>
    public interface IFileService
    {
        /// <summary>
        /// Загрузка файла.
        /// </summary>
        /// <param name="fileName">Наименование файла.</param>
        /// <param name="contentType">ContentType файла.</param>
        /// <param name="stream">Поток загрузки файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Идентификатор созданного файла.</returns>
        Task<string> UploadFileAsync(string fileName, string contentType, Stream stream, CancellationToken cancellationToken);

        /// <summary>
        /// Скачивание файла.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns> Модель потокового скачивания файла.</returns>
        Task<FileStreamModel> DownloadFileAsync(string id, CancellationToken cancellationToken);

        /// <summary>
        /// Удаление файла.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Результат удаления файла.</returns>
        Task<bool> DeleteAsync(string id, CancellationToken cancellationToken);

        /// <summary>
        /// Получение информации о файле.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Модель информации о файле.</returns>
        Task<FileInfoDto> GetFileInfoAsync(string id, CancellationToken cancellationToken);
    }
}
