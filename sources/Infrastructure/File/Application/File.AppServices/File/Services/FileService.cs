﻿using File.AppServices.File.Repositories;
using File.Contracts;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace File.AppServices.File.Services
{
    /// <summary>
    /// <inheritdoc cref="IFileService"/>.
    /// </summary>
    public class FileService : IFileService
    {
        private readonly IFileRepository _fileRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="FileService"/>.
        /// </summary>
        public FileService(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

        /// <inheritdoc/>
        public Task<string> UploadFileAsync(string fileName, string contentType, Stream stream, CancellationToken cancellationToken)
        {
            return _fileRepository.UploadFileAsync(fileName, contentType, stream, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<FileStreamModel> DownloadFileAsync(string id, CancellationToken cancellationToken)
        {
            return _fileRepository.DownloadFileAsync(id, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<bool> DeleteAsync(string id, CancellationToken cancellationToken)
        {
            return _fileRepository.DeleteAsync(id, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<FileInfoDto> GetFileInfoAsync(string id, CancellationToken cancellationToken)
        {
            return _fileRepository.GetFileInfoAsync(id, cancellationToken);
        }
    }
}
