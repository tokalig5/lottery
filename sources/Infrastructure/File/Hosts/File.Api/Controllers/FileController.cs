using Common.Contracts.WebApi.Constants;
using File.Handlers.File.Commands;
using File.Handlers.File.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace File.Api.Controllers
{
    /// <summary>
    /// ���������� ������ � �������.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class FileController : ControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// ������������� ���������� <see cref="FileController"/>.
        /// </summary>
        public FileController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// �������� ����� � �������.
        /// </summary>
        /// <param name="file">����.</param>
        /// <param name="cancellationToken">����� ������.</param>        
        [HttpPost]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = long.MaxValue)]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Upload(IFormFile file, CancellationToken cancellationToken)
        {
            using var stream = file.OpenReadStream();
            var command = new FileUploadCommand(file.FileName, file.ContentType, stream);
            var result = await _mediator.Send(command, cancellationToken);
            return StatusCode((int)HttpStatusCode.Created, result);
        }

        /// <summary>
        /// ���������� ����� �� ��������������.
        /// </summary>
        /// <param name="id">������������� �����.</param>
        /// <param name="cancellationToken">����� ������.</param>
        /// <returns>���� � ���� ������.</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Download(string id, CancellationToken cancellationToken)
        {
            var query = new FileDownloadQuery(id);
            var result = await _mediator.Send(query, cancellationToken);

            Response.ContentLength = result.Info.Length;
            Response.Headers.Add(HttpContextHeader.FileName, Uri.EscapeDataString(result.Info.Name));

            return File(result.Stream, result.Info.ContentType, result.Info.Name, true);
        }

        /// <summary>
        /// �������� ����� �� ��������������.
        /// </summary>
        /// <param name="id">������������� �����.</param>
        /// <param name="cancellationToken">����� ������.</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id, CancellationToken cancellationToken)
        {
            var command = new FileDeleteCommand(id);
            await _mediator.Send(command, cancellationToken);
            return NoContent();
        }

        /// <summary>
        /// ��������� ���������� � ����� �� ��������������.
        /// </summary>
        /// <param name="id">������������� �����.</param>
        /// <param name="cancellationToken">����� ������.</param>
        /// <returns>���������� � �����.</returns>
        [HttpGet("{id}/info")]
        public async Task<IActionResult> GetInfoById(string id, CancellationToken cancellationToken)
        {
            var query = new FileGetInfoQuery(id);
            var result = await _mediator.Send(query, cancellationToken);
            return Ok(result);
        }
    }
}