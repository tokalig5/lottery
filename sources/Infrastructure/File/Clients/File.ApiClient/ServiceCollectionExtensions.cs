﻿using Common.Contracts.Settings;
using File.ApiClient.Clients;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace File.ApiClient
{
    /// <summary>
    /// Методы расширения АПИ клиента сервиса лотереи
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление FileApiClient
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="settings">Настройки Proxy</param>
        /// <returns>Коллекция сервисов</returns>
        public static IServiceCollection AddFileApiClient(this IServiceCollection services, ProxySettings settings)
        {
            services.AddHttpClient<IFileApiClient, FileApiClient>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(settings.FileServiceUrl);
            });
            return services;
        }
    }
}
