﻿using File.Contracts;
using Microsoft.AspNetCore.Http;
using System.Threading;
using System.Threading.Tasks;

namespace File.ApiClient.Clients
{
    /// <summary>
    /// File service API Client
    /// </summary>
    public interface IFileApiClient
    {
        /// <summary>
        /// Загрузка файла в систему.
        /// </summary>
        /// <param name="file">Файл.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        Task<string> UploadAsync(IFormFile file, CancellationToken cancellationToken);

        /// <summary>
        /// Скачивание файла.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns> Модель потокового скачивания файла.</returns>
        Task<FileStreamModel> DownloadFileAsync(string id, CancellationToken cancellationToken);

        /// <summary>
        /// Удаление файла.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Результат удаления файла.</returns>
        Task DeleteAsync(string id, CancellationToken cancellationToken);

        /// <summary>
        /// Получение информации о файле.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Модель информации о файле.</returns>
        Task<FileInfoDto> GetFileInfoAsync(string id, CancellationToken cancellationToken);
    }
}