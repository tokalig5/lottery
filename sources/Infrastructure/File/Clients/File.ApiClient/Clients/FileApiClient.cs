﻿using Common.Infrastructure.Utils.Extensions;
using File.ApiClient.Exceptions;
using File.Contracts;
using Flurl;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace File.ApiClient.Clients
{
    /// <summary>
    /// File service API Client
    /// </summary>
    public class FileApiClient : IFileApiClient
    {
        private readonly HttpClient _httpClient;
        private const string ApiRoutePrefix = "api/v1";
        private const string FileApiPath = "file";

        /// <summary>
        /// Инициалазация экземпляра <see cref="FileApiClient"/>.
        /// </summary>        
        public FileApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <inheritdoc/>
        public Task<string> UploadAsync(IFormFile file, CancellationToken cancellationToken)
        {
            var form = new MultipartFormDataContent();
            var content = new StreamContent(file.OpenReadStream());
            content.Headers.ContentType = MediaTypeHeaderValue.Parse(file.ContentType);
            content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = file.Name,
                FileName = file.FileName
            };
            form.Add(content, file.Name, file.FileName);

            return _httpClient.PostFileAsync(GetFileApiUrl(), form,
            (statusCode, message, exception) => throw new FileApiClientException(statusCode, $"Ошибка загрузки файла {file.FileName}. Ошибка: {message}", exception),
            cancellationToken);
        }

        /// <inheritdoc/>
        public Task<FileStreamModel> DownloadFileAsync(string id, CancellationToken cancellationToken)
        {
            return _httpClient.GetFileStreamAsync(GetFileApiUrl(id),
            (statusCode, message, exception) => throw new FileApiClientException(statusCode, $"Ошибка скачивания файла {id}. Ошибка: {message}", exception),
            cancellationToken);
        }

        /// <inheritdoc/>
        public Task DeleteAsync(string id, CancellationToken cancellationToken)
        {
            return _httpClient.DeleteAsync(GetFileApiUrl(id), 
            (statusCode, message, exception) => throw new FileApiClientException(statusCode, $"Ошибка удаления файла {id}. Ошибка: {message}", exception),
            cancellationToken);
        }

        /// <inheritdoc/>
        public Task<FileInfoDto> GetFileInfoAsync(string id, CancellationToken cancellationToken)
        {
            return _httpClient.GetAsync<FileInfoDto>(GetFileApiUrl(id, "info"), 
            (statusCode, message, exception) => throw new FileApiClientException(statusCode, $"Ошибка получения информации о файле {id}. Ошибка: {message}", exception),
            cancellationToken);
        }

        private static string GetFileApiUrl(params string[] parts)
        {
            return Url.Combine(ApiRoutePrefix, FileApiPath, Url.Combine(parts));
        }
    }
}