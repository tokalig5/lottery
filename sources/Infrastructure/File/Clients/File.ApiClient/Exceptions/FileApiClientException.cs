﻿using Common.Contracts.WebApi.Exceptions;
using System;
using System.Net;

namespace File.ApiClient.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе FileApiClient.
    /// </summary>
    [Serializable]
    public class FileApiClientException : ApiClientException
    {
        /// <summary>
        /// Инициализация экземпляра <see cref="FileApiClientException"/>.
        /// </summary>
        /// <param name="statusCode">HTTP-код ошибки запроса.</param>
        /// <param name="message">Сообщение ошибки запроса.</param>
        /// <param name="innerException">Внутреннее исключение.</param>
        public FileApiClientException(HttpStatusCode statusCode, string message, Exception innerException) : base(statusCode, message, innerException)
        {
        }
    }
}
