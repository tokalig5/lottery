﻿using System.IO;

namespace File.Contracts
{
    /// <summary>
    /// Модель потокового скачивания файла.
    /// </summary>
    public class FileStreamModel
    {
        /// <summary>
        /// Информация о файле.
        /// </summary>
        public FileInfoDto Info { get; set; }

        /// <summary>
        /// Поток файла.
        /// </summary>
        public Stream Stream { get; set; }
    }
}
