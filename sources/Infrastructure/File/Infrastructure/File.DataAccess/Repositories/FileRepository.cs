﻿using Common.Contracts.Settings;
using Common.Contracts.WebApi.Exceptions;
using File.AppServices.File.Repositories;
using File.Contracts;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace File.DataAccess.Repositories
{
    /// <inheritdoc cref="IFileRepository"/>.
    public class FileRepository : IFileRepository
    {
        private readonly IGridFSBucket _bucket;

        /// <summary>
        /// Инициализация экземпляра <see cref="FileRepository"/>.
        /// </summary>
        public FileRepository(MongoClient mongoClient, MongoSettings mongoSettings)
        {
            var database = mongoClient.GetDatabase(mongoSettings.DatabaseName);
            _bucket = new GridFSBucket(database);
        }

        /// <inheritdoc/>
        public async Task<string> UploadFileAsync(string fileName, string contentType, Stream stream, CancellationToken cancellationToken)
        {
            var options = new GridFSUploadOptions { ContentType = contentType };
            var id = await _bucket.UploadFromStreamAsync(fileName, stream, options, cancellationToken);
            return id.ToString();
        }

        /// <inheritdoc/>
        public async Task<FileStreamModel> DownloadFileAsync(string id, CancellationToken cancellationToken)
        {
            var gridFSDownloadStream = await _bucket.OpenDownloadStreamAsync(ObjectId.Parse(id), null, cancellationToken);
            await CheckFileAsync(id, cancellationToken);

            var result = new FileStreamModel
            {
                Stream = gridFSDownloadStream,
                Info = new FileInfoDto
                {
                    Id = id,
                    Length = gridFSDownloadStream.Length,
                    ContentType = gridFSDownloadStream.FileInfo.ContentType,
                    Name = gridFSDownloadStream.FileInfo.Filename
                }
            };

            return result;
        }

        /// <inheritdoc/>
        public async Task<bool> DeleteAsync(string id, CancellationToken cancellationToken)
        {
            await CheckFileAsync(id, cancellationToken);
            await _bucket.DeleteAsync(ObjectId.Parse(id), cancellationToken);
            return true;
        }

        /// <inheritdoc/>
        public async Task<FileInfoDto> GetFileInfoAsync(string id, CancellationToken cancellationToken)
        {
            await CheckFileAsync(id, cancellationToken);

            var objId = ObjectId.Parse(id);
            var filter = Builders<GridFSFileInfo<ObjectId>>.Filter.Eq(info => info.Id, objId);
            var fileInfoCursor = await _bucket.FindAsync(filter, null, cancellationToken);
            var fileInfo = await fileInfoCursor.SingleOrDefaultAsync(cancellationToken);

            var result = new FileInfoDto
            {
                Id = id,
                Length = fileInfo.Length,
                ContentType = fileInfo.ContentType,
                Name = fileInfo.Filename
            };
            return result;
        }

        /// <summary>
        /// Проверка файла на наличие в системе.
        /// </summary>
        /// <param name="id">Идентификатор файла.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <exception cref="NotFoundException">Исключение, выбрасываемое при отсутствии файла в системе.</exception>
        private async Task CheckFileAsync(string id, CancellationToken cancellationToken)
        {
            var objId = ObjectId.Parse(id);
            var filter = Builders<GridFSFileInfo<ObjectId>>.Filter.Eq(info => info.Id, objId);
            var fileInfoCursor = await _bucket.FindAsync(filter, null, cancellationToken);

            var isExists = await fileInfoCursor.AnyAsync(cancellationToken);
            if (!isExists)
            {
                throw new NotFoundException($"Файл {id} не найден в системе.");
            }
        }
    }
}
