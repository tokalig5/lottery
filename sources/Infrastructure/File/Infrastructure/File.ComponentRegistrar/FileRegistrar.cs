﻿using Common.Contracts.Settings;
using Common.Infrastructure;
using File.AppServices.File.Repositories;
using File.AppServices.File.Services;
using File.DataAccess.Repositories;
using File.Handlers.File.Commands;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System.IO;
using System.Reflection;

namespace File.ComponentRegistrar
{
    /// <summary>
    /// Регистратор файлового сервиса.
    /// </summary>
    public static class FileRegistrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                           .ConfigureSwaggerDoc()
                           .ConfigureMongoDbStorage(configuration)
                           .ConfigureServices()
                           .ConfigureBusService(configuration)
                           .AddMediator()
                           .ConfigureRepositories();
        }

        private static IServiceCollection AddMediator(this IServiceCollection services)
        {
            services.AddMediator(typeof(FileUploadCommand).Assembly);
            return services;
        }

        private static IServiceCollection ConfigureBusService(this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = new RabbitSettings();
            configuration.Bind(nameof(RabbitSettings), rabbitSettings);

            services.AddBusService(rabbitSettings);
            return services;
        }

        private static IServiceCollection ConfigureSwaggerDoc(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                var xmlFile = $"{Assembly.GetEntryAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            return services;
        }

        private static IServiceCollection ConfigureMongoDbStorage(this IServiceCollection services, IConfiguration configuration)
        {
            var mongoSettings = new MongoSettings();
            configuration.Bind(nameof(MongoSettings), mongoSettings);
            services.AddSingleton(mongoSettings);

            services.AddSingleton(new MongoClient(mongoSettings.GetConnectionString()));
            return services;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddTransient<IFileService, FileService>();
            return services;
        }

        private static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            services.AddTransient<IFileRepository, FileRepository>();
            return services;
        }
    }
}
