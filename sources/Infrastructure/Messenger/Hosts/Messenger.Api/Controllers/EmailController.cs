﻿using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace Messenger.Api.Controllers
{
    /// <summary>
    /// Email controller
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmailController : ControllerBase
    {
        public EmailController()
        {            
        }

        [HttpGet]
        public IActionResult Get(CancellationToken cancellationToken)
        {
            return Ok($"{nameof(EmailController)} is working");
        }
    }
}