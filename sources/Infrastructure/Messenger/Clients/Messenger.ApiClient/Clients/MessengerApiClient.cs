﻿using System.Net.Http;

namespace Messenger.ApiClient.Clients
{
    /// <summary>
    /// Messenger service API Client
    /// </summary>
    public class MessengerApiClient : IMessengerApiClient
    {
        private readonly HttpClient _httpClient;

        public MessengerApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
    }
}