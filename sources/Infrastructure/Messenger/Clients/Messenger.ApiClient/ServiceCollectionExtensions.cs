﻿using Common.Contracts.Settings;
using Messenger.ApiClient.Clients;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Messenger.ApiClient
{
    /// <summary>
    /// Методы расширения АПИ клиента финансового сервиса
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление MessengerApiClient
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="settings">Настройки Proxy</param>
        /// <returns>Коллекция сервисов</returns>
        public static IServiceCollection AddMessengerApiClient(this IServiceCollection services, ProxySettings settings)
        {
            services.AddHttpClient<IMessengerApiClient, MessengerApiClient>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(settings.MessengerServiceUrl);
            });
            return services;
        }
    }
}
