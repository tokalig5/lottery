﻿using System;
using System.Runtime.Serialization;

namespace Messenger.ApiClient.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе MessengerApiClient
    /// </summary>
    [Serializable]
    public class MessengerApiClientException : Exception
    {
        private const string DefaultMessage = "Ошибка работы с MessengerApiClient: ";

        public MessengerApiClientException(string message) : base(string.Concat(DefaultMessage, message))
        {
        }

        public MessengerApiClientException(string message, Exception innerException) : base(string.Concat(DefaultMessage, message), innerException)
        {
        }

        protected MessengerApiClientException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
        }
    }
}
