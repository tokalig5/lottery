﻿using Common.Contracts.Settings;
using Microsoft.Extensions.DependencyInjection;
using SignalR.ApiClient.Clients;
using System;

namespace SignalR.ApiClient
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление SignalRApiClient
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="settings">Настройки Proxy</param>
        /// <returns>Коллекция сервисов</returns>
        public static IServiceCollection AddSignalRApiClient(this IServiceCollection services, ProxySettings settings)
        {
            services.AddHttpClient<ISignalRApiClient, SignalRApiClient>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(settings.SignalRServiceUrl);
            });
            return services;
        }
    }
}
