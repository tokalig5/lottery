﻿using System;
using System.Runtime.Serialization;

namespace SignalR.ApiClient.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе SignalRApiClient
    /// </summary>
    [Serializable]
    public class SignalRApiClientException : Exception
    {
        private const string DefaultMessage = "Ошибка работы с SignalRApiClient: ";

        public SignalRApiClientException(string message) : base(string.Concat(DefaultMessage, message))
        {
        }

        public SignalRApiClientException(string message, Exception innerException) : base(string.Concat(DefaultMessage, message), innerException)
        {
        }

        protected SignalRApiClientException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
        }
    }
}
