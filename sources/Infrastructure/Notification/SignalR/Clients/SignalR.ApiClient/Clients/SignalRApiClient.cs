﻿using System.Net.Http;

namespace SignalR.ApiClient.Clients
{
    /// <summary>
    /// SignalR service API Client
    /// </summary>
    public class SignalRApiClient : ISignalRApiClient
    {
        private readonly HttpClient _httpClient;

        public SignalRApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
    }
}