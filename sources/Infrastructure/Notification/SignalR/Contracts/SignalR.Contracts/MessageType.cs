﻿namespace SignalR.Contracts
{
    /// <summary>
    /// Тип 
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// Обновление серверного времени.
        /// </summary>
        Timer = 1
    }
}
