﻿using System;

namespace SignalR.Contracts
{
    /// <summary>
    /// Модель сообщения SignalR.
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Текст сообщения.
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Тип сообщения.
        /// </summary>
        public MessageType Type { get; private set; }

        /// <summary>
        /// Текущее время UTC.
        /// </summary>
        public DateTime DateTime { get; private set; }

        /// <summary>
        /// Инициализация экземпляра <see cref="Message"/>.
        /// </summary>
        /// <param name="messageType">Тип сообщения.</param>
        /// <param name="text">Текст сообщения.</param>
        public Message(MessageType messageType, string text = null)
        {
            Type = messageType;
            Text = text;
            DateTime = DateTime.UtcNow;
        }
    }
}
