﻿using Common.Contracts.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SignalR.Api.Services.Notification;

namespace SignalR.Api
{
    /// <summary>
    /// Добавление сервисов.
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление зависимостей сервисов.
        /// </summary>
        /// <param name="services">Сервисы.</param>
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureSignalR(configuration);
            services.AddTransient<IMessageService, MessageService>();
            return services;
        }

        private static IServiceCollection ConfigureSignalR(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = new RedisSettings();
            configuration.Bind(nameof(RedisSettings), settings);
            services.AddSignalR().AddStackExchangeRedis(settings.ConnectionString, options =>
            {
                options.Configuration.ChannelPrefix = settings.ChannelPrefix;
                options.Configuration.DefaultDatabase = settings.DefaultDatabase;
            });
            return services;
        }
    }
}
