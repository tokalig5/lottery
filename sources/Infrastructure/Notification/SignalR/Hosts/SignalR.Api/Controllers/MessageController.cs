﻿using Microsoft.AspNetCore.Mvc;
using SignalR.Api.Services.Notification;
using SignalR.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace SignalR.Api.Controllers
{
    /// <summary>
    /// SignalR message controller
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class MessageController : ControllerBase
    {
        private readonly IMessageService _messageService;

        /// <summary>
        /// Инициализация экземпляпа <see cref="MessageController"/>.
        /// </summary>
        /// <param name="messageService">Сервис по работе с сообщениями.</param>
        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        /// <summary>
        /// Отправка сообщения пользователю.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <param name="message">Модель сообщения.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        [HttpPost("user")]
        public async Task<IActionResult> SendToUser(string id, Message message, CancellationToken cancellationToken)
        {
            await _messageService.SendToUserAsync(id, message, cancellationToken);
            return Ok();
        }

        /// <summary>
        /// Отправка сообщения группе.
        /// </summary>
        /// <param name="id">Идентификатор группы.</param>
        /// <param name="message">Модель сообщения.</param>
        /// <param name="cancellationToken">Токен отмены.</param> 
        [HttpPost("group")]
        public async Task<IActionResult> SendToGroup(string id, Message message, CancellationToken cancellationToken)
        {
            await _messageService.SendToGroupAsync(id, message, cancellationToken);
            return Ok();
        }
    }
}