﻿using Microsoft.AspNetCore.Mvc;
using SignalR.Api.Services.Notification;
using System.Threading;
using System.Threading.Tasks;

namespace SignalR.Api.Controllers
{
    /// <summary>
    /// Контроллер для работы со временем.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class TimeController : ControllerBase
    {
        private readonly IMessageService _messageService;

        /// <summary>
        /// Инициализация экземпляпа <see cref="TimeController"/>.
        /// </summary>
        /// <param name="messageService">Сервис по работе с сообщениями.</param>
        public TimeController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpPost("notify")]
        public async Task<IActionResult> SendServerTime(CancellationToken cancellationToken)
        {
            await _messageService.SendServerTimeToUsersAsync(cancellationToken);
            return Ok();
        }
    }
}
