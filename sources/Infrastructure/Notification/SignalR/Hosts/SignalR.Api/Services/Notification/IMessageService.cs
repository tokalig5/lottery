﻿using SignalR.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace SignalR.Api.Services.Notification
{
    /// <summary>
    /// Сервис по работе с сообщениями.
    /// </summary>
    public interface IMessageService
    {
        /// <summary>
        /// Отправка сообщения пользователю.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <param name="message">Модель сообщения.</param>
        /// <param name="cancellationToken">Токен отмены.</param>   
        Task SendToUserAsync(string id, Message message, CancellationToken cancellationToken);

        /// <summary>
        /// Отправка сообщения группе.
        /// </summary>
        /// <param name="id">Идентификатор группы.</param>
        /// <param name="message">Модель сообщения.</param>
        /// <param name="cancellationToken">Токен отмены.</param> 
        Task SendToGroupAsync(string id, Message message, CancellationToken cancellationToken);

        /// <summary>
        /// Отправка серверного времени всем пользователям.
        /// </summary>
        /// <param name="cancellationToken">Токен отмены.</param>
        Task SendServerTimeToUsersAsync(CancellationToken cancellationToken);
    }
}
