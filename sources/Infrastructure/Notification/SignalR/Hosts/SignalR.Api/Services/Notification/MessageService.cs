﻿using Microsoft.AspNetCore.SignalR;
using SignalR.Api.Hubs;
using SignalR.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace SignalR.Api.Services.Notification
{
    /// <inheritdoc cref="IMessageService"/>.
    public class MessageService : IMessageService
    {
        private readonly IHubContext<SystemHub> _systemHubContext;
        private readonly IHubContext<UserHub> _userHubContext;

        /// <summary>
        /// Инициализация экзампляра <see cref="MessageService"/>.
        /// </summary>
        /// <param name="userHubContext">Контекст работы с хабом пользовательских сообщений сервиса.</param>
        /// <param name="systemHubContext">Контекст работы с хабом системных сообщений сервиса.</param>
        public MessageService(IHubContext<UserHub> userHubContext, IHubContext<SystemHub> systemHubContext)
        {
            _userHubContext = userHubContext;
            _systemHubContext = systemHubContext;
        }

        /// <inheritdoc/>
        public Task SendToGroupAsync(string id, Message message, CancellationToken cancellationToken)
        {
            return _systemHubContext.Clients.Group(id).SendAsync("group-info", id, message, cancellationToken);
        }

        /// <inheritdoc/>
        public Task SendToUserAsync(string id, Message message, CancellationToken cancellationToken)
        {
            return _userHubContext.Clients.User(id).SendAsync("user-info", id, message, cancellationToken);
        }

        /// <inheritdoc/>
        public Task SendServerTimeToUsersAsync(CancellationToken cancellationToken)
        {
            var message = new Message(MessageType.Timer);
            return _systemHubContext.Clients.All.SendAsync("server-time", message, cancellationToken);
        }
    }
}
