﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace SignalR.Api.Hubs
{
    /// <summary>
    /// Хаб пользовательских сообщений сервиса.
    /// </summary>
    [Authorize]
    public class UserHub : Hub
    {
    }
}
