﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SignalR.Api.Hubs
{
    /// <summary>
    /// Хаб системных сообщений сервиса.
    /// </summary>
    public class SystemHub : Hub
    {
        /// <summary>
        /// Присоединение к группе рассылки уведомлений.
        /// </summary>
        /// <param name="name">Имя группы.</param>
        public Task JoinGroup(string name)
        {
            return Groups.AddToGroupAsync(Context.ConnectionId, name);
        }

        /// <summary>
        /// Отсоединение от группы рассылки уведомлений.
        /// </summary>
        /// <param name="name">Имя группы.</param>
        public Task LeaveGroup(string name)
        {
            return Groups.RemoveFromGroupAsync(Context.ConnectionId, name);
        }
    }
}
