﻿using Common.Contracts.Bus.Abstraction;
using Notification.Contracts.Requests;

namespace Notification.Contracts.Events
{
    /// <summary>
    /// Событие запроса на отправку уведомления в конкретный канал.
    /// </summary>
    public class CreateChannelNotificationEvent : IMessageQueueEvent
    {
        /// <summary>
        /// Модель запроса на отправку уведомления в конкретный канал.
        /// </summary>
        public CreateChannelNotificationRequest Model { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="CreateChannelNotificationEvent"/>.
        /// </summary>
        public CreateChannelNotificationEvent(CreateChannelNotificationRequest model)
        {
            Model = model;
        }
    }
}
