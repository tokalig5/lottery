﻿using Notification.Contracts.Enums;
using System;

namespace Notification.Contracts.Requests
{
    /// <summary>
    /// Модель запроса на отправку уведомления.
    /// </summary>
    public sealed class CreateNotificationRequest
    {
        /// <summary>
        /// Идентификатор сущности по которой делается уведомление.
        /// </summary>
        public Guid EntityId { get; }

        /// <summary>
        /// Тип уведомления.
        /// </summary>
        public NotificationType NotificationType { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="CreateNotificationRequest"/>.
        /// </summary>
        public CreateNotificationRequest(Guid entityId, NotificationType notificationType)
        {
            EntityId = entityId;
            NotificationType = notificationType;
        }
    }
}
