﻿using Notification.Contracts.Enums;
using System;

namespace Notification.Contracts.Requests
{
    /// <summary>
    /// Модель запроса на отправку уведомления в конкретный канал.
    /// </summary>
    public sealed class CreateChannelNotificationRequest
    {
        /// <summary>
        /// Идентификатор сущности по которой делается уведомление.
        /// </summary>
        public Guid EntityId { get; }

        /// <summary>
        /// Тип уведомления.
        /// </summary>
        public NotificationType NotificationType { get; }

        /// <summary>
        /// Тип канала уведомления.
        /// </summary>
        public EventChannelType ChannelType { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="CreateChannelNotificationRequest"/>.
        /// </summary>
        public CreateChannelNotificationRequest(Guid entityId, NotificationType notificationType, EventChannelType channelType)
        {
            EntityId = entityId;
            NotificationType = notificationType;
            ChannelType = channelType;
        }
    }
}
