﻿namespace Notification.Contracts.Enums
{
    /// <summary>
    /// Тип канала события.
    /// </summary>
    public enum EventChannelType
    {
        /// <summary>
        /// Неизвестный тип или тип по умолчанию.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Личный кабинет.
        /// </summary>
        PersonalAccount = 1,

        /// <summary>
        /// Email.
        /// </summary>
        Email = 2
    }
}
