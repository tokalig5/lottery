﻿namespace Notification.Contracts.Enums
{
    /// <summary>
    /// Тип уведомления.
    /// </summary>
    public enum NotificationType
    {
        /// <summary>
        /// Неизвестный тип.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Лотерея была создана.
        /// </summary>
        LotCreated = 1,

        /// <summary>
        /// Заявка подана.
        /// </summary>
        ApplicationCreated = 2,
    }
}
