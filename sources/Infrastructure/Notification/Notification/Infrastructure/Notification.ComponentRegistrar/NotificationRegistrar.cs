﻿using AutoMapper;
using Common.Contracts.Settings;
using Common.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Notification.AppServices.Notifications.Builders;
using Notification.AppServices.Notifications.Factories.ChannelBuilder;
using Notification.AppServices.Notifications.Factories.ChannelProvider;
using Notification.AppServices.Notifications.Providers;
using Notification.AppServices.Notifications.Repositories;
using Notification.AppServices.Notifications.Services;
using Notification.AppServices.Templates.Repositories;
using Notification.AppServices.Templates.Services;
using Notification.DataAccess;
using Notification.DataAccess.Repositories;
using Notification.Handlers.Notifications.Commands;
using System;
using System.Linq;

namespace Notification.ComponentRegistrar
{
    public static class NotificationRegistrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                           .ConfigureAutoMapper()
                           .ConfigureBusService(configuration)
                           .ConfigureServices()
                           .ConfigureDbConnections(configuration)
                           .ConfigureRepositories()
                           .ConfigureBuildersAndFactories()
                           .AddMediator()
                           .AddContextLogger(configuration);
        }

        private static IServiceCollection AddMediator(this IServiceCollection services)
        {
            services.AddMediator(typeof(CreateNotificationCommand).Assembly);
            return services;
        }

        private static IServiceCollection ConfigureBusService(this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = new RabbitSettings();
            configuration.Bind(nameof(RabbitSettings), rabbitSettings);

            services.AddBusService(rabbitSettings);
            return services;
        }

        private static IServiceCollection ConfigureDbConnections(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbConfiguration<NotificationContext, NotificationReadOnlyContext>(configuration);
            return services;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<ITemplateService, TemplateService>();
            return services;
        }

        private static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            services.AddTransient<INotificationRepository, NotificationRepository>();
            services.AddTransient<ITemplateRepository, TemplateRepository>();
            services.AddTransient<INotificationSettingRepository, NotificationSettingRepository>();
            return services;
        }

        private static IServiceCollection ConfigureBuildersAndFactories(this IServiceCollection services)
        {
            services.AddTransient<IChannelProviderFactory, ChannelProviderFactory>();
            services.AddTransient<IChannelBuilderFactory, ChannelBuilderFactory>();
            services.RegisterAllImplementations(typeof(IChannelBuilder));
            services.RegisterAllImplementations(typeof(IChannelProvider));
            services.RegisterAllImplementations(typeof(INotificationBuilder));
            return services;
        }

        private static IServiceCollection ConfigureAutoMapper(this IServiceCollection services)
        {
            return services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg => { });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }

        private static IServiceCollection RegisterAllImplementations(this IServiceCollection services, Type interfaceType)
        {
            AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes())
                                                   .Where(item => !item.IsAbstract && !item.IsInterface && item.GetInterfaces().Any(i => i == interfaceType)).ToList()
                                                   .ForEach(assignedTypes =>
                                                   {
                                                       var serviceType = assignedTypes.GetInterfaces().First(i => i == interfaceType);
                                                       services.AddTransient(serviceType, assignedTypes);
                                                   });
            return services;
        }
    }
}