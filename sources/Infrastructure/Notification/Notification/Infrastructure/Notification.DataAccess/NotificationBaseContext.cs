﻿using Microsoft.EntityFrameworkCore;
using Notification.DataAccess.Configurations;

namespace Notification.DataAccess
{
    /// <summary>
    /// Базовый контекст для работы с БД.
    /// </summary>
    public class NotificationBaseContext : DbContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="NotificationBaseContext"/>
        /// </summary>
        /// <param name="options"></param>
        public NotificationBaseContext(DbContextOptions options) : base(options)
        {
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new NotificationConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationSettingConfiguration());
            modelBuilder.ApplyConfiguration(new TemplateConfiguration());
        }
    }
}