﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Notification.DataAccess.Configurations
{
    /// <summary>
    /// Настройки сущности уведомления пользователю.
    /// </summary>
    public class NotificationConfiguration : IEntityTypeConfiguration<Entities.Notification>
    {
        /// <inheritdoc/>
        public void Configure(EntityTypeBuilder<Entities.Notification> builder)
        {
            builder.ToTable("Notifications");

            builder.HasKey(s => s.Id);
            builder.HasIndex(s => s.UserId);

            builder.Property(s => s.UserId).HasMaxLength(36).IsRequired();
            builder.Property(s => s.Subject).HasMaxLength(256).IsRequired();
            builder.Property(s => s.Text).HasMaxLength(2048).IsRequired();
            builder.Property(s => s.Created).HasConversion(s => s, s => DateTime.SpecifyKind(s, DateTimeKind.Utc));
        }
    }
}
