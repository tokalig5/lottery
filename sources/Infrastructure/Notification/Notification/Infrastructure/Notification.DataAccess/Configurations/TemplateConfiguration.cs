﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Notification.Entities;

namespace Notification.DataAccess.Configurations
{
    /// <summary>
    /// Настройки сущности шаблона уведомления.
    /// </summary>
    public class TemplateConfiguration : IEntityTypeConfiguration<Template>
    {
        /// <inheritdoc/>
        public void Configure(EntityTypeBuilder<Template> builder)
        {
            builder.ToTable("Templates");

            builder.HasKey(s => s.Id);
            builder.HasIndex(s => s.NotificationType);
            builder.HasIndex(s => s.ChannelType);

            builder.Property(s => s.Description).HasMaxLength(256).IsRequired();
            builder.Property(s => s.Subject).HasMaxLength(256).IsRequired();
            builder.Property(s => s.Body).HasMaxLength(2048).IsRequired();
        }
    }
}
