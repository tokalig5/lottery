﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Notification.Entities;

namespace Notification.DataAccess.Configurations
{
    /// <summary>
    /// Настройки сущности настроек уведомления.
    /// </summary>
    public class NotificationSettingConfiguration : IEntityTypeConfiguration<NotificationSetting>
    {
        /// <inheritdoc/>
        public void Configure(EntityTypeBuilder<NotificationSetting> builder)
        {
            builder.ToTable("NotificationSettings");

            builder.HasKey(s => s.Id);
            builder.HasIndex(s => s.NotificationType);
        }
    }
}
