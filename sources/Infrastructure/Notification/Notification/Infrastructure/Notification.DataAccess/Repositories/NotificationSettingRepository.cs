﻿using Common.Infrastructure.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Notification.AppServices.Notifications.Repositories;
using Notification.Contracts.Enums;
using Notification.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.DataAccess.Repositories
{
    /// <inheritdoc cref="INotificationSettingRepository"/>
    public class NotificationSettingRepository : INotificationSettingRepository
    {
        private readonly IRepository<NotificationSetting, NotificationContext> _repository;
        private readonly IReadOnlyRepository<NotificationSetting, NotificationReadOnlyContext> _readOnlyRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="NotificationSettingRepository"/>.
        /// </summary>
        /// <param name="repository">Репозиторий чтения-записи.</param>
        /// <param name="readOnlyRepository">Репозиторий чтения.</param>
        public NotificationSettingRepository(IRepository<NotificationSetting, NotificationContext> repository, IReadOnlyRepository<NotificationSetting, NotificationReadOnlyContext> readOnlyRepository)
        {
            _repository = repository;
            _readOnlyRepository = readOnlyRepository;
        }

        /// <inheritdoc/>
        public Task<List<NotificationSetting>> GetActiveTypeSettingsAsync(NotificationType notificationType, CancellationToken cancellationToken)
        {
            return _readOnlyRepository.AsQueryable().Where(s => s.NotificationType == notificationType && s.IsActive).ToListAsync(cancellationToken);
        }
    }
}
