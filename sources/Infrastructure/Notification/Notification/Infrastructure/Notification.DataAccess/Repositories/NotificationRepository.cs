﻿using Common.Infrastructure.DataAccess.Repositories;
using Notification.AppServices.Notifications.Repositories;

namespace Notification.DataAccess.Repositories
{
    /// <inheritdoc cref="INotificationRepository"/>
    public class NotificationRepository : INotificationRepository
    {
        private readonly IRepository<Entities.Notification, NotificationContext> _repository;
        private readonly IReadOnlyRepository<Entities.Notification, NotificationReadOnlyContext> _readOnlyRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="NotificationRepository"/>.
        /// </summary>
        /// <param name="repository">Репозиторий чтения-записи.</param>
        /// <param name="readOnlyRepository">Репозиторий чтения.</param>
        public NotificationRepository(IRepository<Entities.Notification, NotificationContext> repository, IReadOnlyRepository<Entities.Notification, NotificationReadOnlyContext> readOnlyRepository)
        {
            _repository = repository;
            _readOnlyRepository = readOnlyRepository;
        }
    }
}
