﻿using Common.Contracts.WebApi.Exceptions;
using Common.Infrastructure.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Notification.AppServices.Templates.Repositories;
using Notification.Contracts.Enums;
using Notification.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.DataAccess.Repositories
{
    /// <inheritdoc cref="ITemplateRepository"/>
    public class TemplateRepository : ITemplateRepository
    {
        private readonly IRepository<Template, NotificationContext> _repository;
        private readonly IReadOnlyRepository<Template, NotificationReadOnlyContext> _readOnlyRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="TemplateRepository"/>.
        /// </summary>
        public TemplateRepository(IRepository<Template, NotificationContext> repository, IReadOnlyRepository<Template, NotificationReadOnlyContext> readOnlyRepository)
        {
            _repository = repository;
            _readOnlyRepository = readOnlyRepository;
        }

        /// <inheritdoc/>
        public async Task<Template> GetByChannelIdOrDefaultAsync(NotificationType notificationType, EventChannelType channelType, CancellationToken cancellationToken)
        {
            var template = await _readOnlyRepository.AsQueryable().FirstOrDefaultAsync(s => s.NotificationType == notificationType && s.ChannelType == channelType, cancellationToken);
            if (template == null)
            {
                template = await _readOnlyRepository.AsQueryable().FirstOrDefaultAsync(s => s.NotificationType == notificationType && s.ChannelType == EventChannelType.Unknown, cancellationToken);
            }

            return template ?? throw new NotFoundException($"Шаблон для типа уведомления {notificationType} и для канала {channelType} не найден в системе.");
        }
    }
}
