﻿using Microsoft.EntityFrameworkCore;

namespace Notification.DataAccess
{
    /// <summary>
    /// Контекст чтения-записи БД Notification.
    /// </summary>
    public class NotificationContext : NotificationBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="NotificationContext"/>
        /// </summary>
        /// <param name="options">Настройки контекста.</param>
        public NotificationContext(DbContextOptions<NotificationContext> options) : base(options)
        {
        }
    }
}
