﻿using Microsoft.EntityFrameworkCore;

namespace Notification.DataAccess
{
    /// <summary>
    /// Контекст чтения-записи БД Notification.
    /// </summary>
    public class NotificationReadOnlyContext : NotificationBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="NotificationReadOnlyContext"/>
        /// </summary>
        /// <param name="options">Настройки контекста.</param>
        public NotificationReadOnlyContext(DbContextOptions<NotificationReadOnlyContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
    }
}
