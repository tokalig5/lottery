﻿using Common.Contracts.WebApi.Exceptions;
using Moq;
using Notification.AppServices.Templates.Repositories;
using Notification.AppServices.Templates.Services;
using Notification.Contracts.Enums;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Notification.UnitTests.Template
{
    /// <summary>
    /// Тесты сервиса по работе с шаблонами уведомлений.
    /// </summary>
    public class TemplateServiceTests
    {
        private readonly Mock<ITemplateRepository> _templateRepository;
        private readonly ITemplateService _templateService;

        /// <summary>
        /// Инициализация экземпляра <see cref="TemplateServiceTests"/>.
        /// </summary>
        public TemplateServiceTests()
        {
            _templateRepository = new Mock<ITemplateRepository>();
            _templateService = new TemplateService(_templateRepository.Object);
        }

        /// <summary>
        /// Успешный рендеринг шаблона.
        /// </summary>
        [Fact]
        public async Task RenderAsync_Should_Be_Ok_All_Fields()
        {
            // Arrange
            var notificationType = NotificationType.LotCreated;
            var channelType = EventChannelType.PersonalAccount;
            var template = new Entities.Template
            {
                ChannelType = channelType,
                NotificationType = notificationType,
                Body = @"Меня зовут {{UserName}}. Мне {{Age}} лет"
            };
            var fields = new Dictionary<string, string>
            {
                {"UserName", "Алексей" },
                {"Age", "20" }
            };

            _templateRepository.Reset();
            _templateRepository.Setup(s => s.GetByChannelIdOrDefaultAsync(notificationType, channelType, CancellationToken.None)).ReturnsAsync(template);

            // Act
            var renderedText = await _templateService.RenderAsync(template.Body, fields, CancellationToken.None);

            // Assert
            Assert.Equal("Меня зовут Алексей. Мне 20 лет", renderedText);
        }

        /// <summary>
        /// Рендеринг шаблона при отсутствии части полей.
        /// </summary>
        [Fact]
        public async Task RenderAsync_Should_Be_Ok_Not_All_Fields()
        {
            // Arrange
            var notificationType = NotificationType.LotCreated;
            var channelType = EventChannelType.PersonalAccount;
            var template = new Entities.Template
            {
                ChannelType = channelType,
                NotificationType = notificationType,
                Body = @"Меня зовут {{UserName}}. Мне {{Age}} лет"
            };
            var fields = new Dictionary<string, string>
            {
                {"UserName", "Алексей" }
            };

            _templateRepository.Reset();
            _templateRepository.Setup(s => s.GetByChannelIdOrDefaultAsync(notificationType, channelType, CancellationToken.None)).ReturnsAsync(template);

            // Act
            var renderedText = await _templateService.RenderAsync(template.Body, fields, CancellationToken.None);

            // Assert
            Assert.Equal("Меня зовут Алексей. Мне  лет", renderedText);
        }

        /// <summary>
        /// Рендеринг щаблона со встроенным массивом данных.
        /// </summary>
        [Fact]
        public async Task RenderAsync_Should_Be_Ok_Array()
        {
            // Arrange
            var notificationType = NotificationType.LotCreated;
            var channelType = EventChannelType.PersonalAccount;
            var template = new Entities.Template
            {
                ChannelType = channelType,
                NotificationType = notificationType,
                Body = @"Меня зовут {{UserName}}. Купленные товары в корзине: {{for product in Products}} {{for.index+1}}. {{product.Name}} - {{product.Count}} - {{product.Sum}} {{end}}"
            };
            var fields = new Dictionary<string, string>
            {
                {"UserName", "Алексей" },
                {"Products", "[{\"Name\":\"Товар 1\",\"Count\":\"1 шт.\",\"Sum\":\"44 руб.\"}, {\"Name\":\"Товар 2\",\"Count\":\"2 шт.\",\"Sum\":\"88 руб.\"}]"}
            };

            _templateRepository.Reset();
            _templateRepository.Setup(s => s.GetByChannelIdOrDefaultAsync(notificationType, channelType, CancellationToken.None)).ReturnsAsync(template);

            // Act
            var renderedText = await _templateService.RenderAsync(template.Body, fields, CancellationToken.None);

            // Assert
            Assert.Equal("Меня зовут Алексей. Купленные товары в корзине:  1. Товар 1 - 1 шт. - 44 руб.  2. Товар 2 - 2 шт. - 88 руб. ", renderedText);
        }

        /// <summary>
        /// Ошибка парсинга шаблона.
        /// </summary>
        [Fact]
        public async Task RenderAsync_Should_Be_Throw_Parsing()
        {
            // Arrange
            var notificationType = NotificationType.LotCreated;
            var channelType = EventChannelType.PersonalAccount;
            var template = new Entities.Template
            {
                ChannelType = channelType,
                NotificationType = notificationType,
                Body = @"Меня зовут {{UserName}}. Купленные товары в корзине: {{for product in Products}} {{for.inex+1}}. {{product.Name}} - {{product.Count}} - {{product.Sum}} {{end}}"
            };
            var fields = new Dictionary<string, string>
            {
                {"UserName", "Алексей" },
                {"Products", "[{\"Name\":\"Товар 1\",\"Count\":\"1 шт.\",\"Sum\":\"44 руб.\"}, {\"Name\":\"Товар 2\",\"Count\":\"2 шт.\",\"Sum\":\"88 руб.\"}]"}
            };

            _templateRepository.Reset();
            _templateRepository.Setup(s => s.GetByChannelIdOrDefaultAsync(notificationType, channelType, CancellationToken.None)).ReturnsAsync(template);

            // Act
            var exception = await Assert.ThrowsAsync<UnprocessableEntityException>(() => _templateService.RenderAsync(template.Body, fields, CancellationToken.None));

            // Assert
            Assert.StartsWith("Сущность не может быть обработана. Ошибка парсинга шаблона", exception.Message);
        }
    }
}
