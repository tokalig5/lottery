﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Notification.DbMigrator.Migrations
{
    public partial class Rename_Exists_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Template",
                table: "Template");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NotificationSetting",
                table: "NotificationSetting");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Notification",
                table: "Notification");

            migrationBuilder.RenameTable(
                name: "Template",
                newName: "Templates");

            migrationBuilder.RenameTable(
                name: "NotificationSetting",
                newName: "NotificationSettings");

            migrationBuilder.RenameTable(
                name: "Notification",
                newName: "Notifications");

            migrationBuilder.RenameIndex(
                name: "IX_Template_NotificationType",
                table: "Templates",
                newName: "IX_Templates_NotificationType");

            migrationBuilder.RenameIndex(
                name: "IX_Template_ChannelType",
                table: "Templates",
                newName: "IX_Templates_ChannelType");

            migrationBuilder.RenameIndex(
                name: "IX_NotificationSetting_NotificationType",
                table: "NotificationSettings",
                newName: "IX_NotificationSettings_NotificationType");

            migrationBuilder.RenameIndex(
                name: "IX_Notification_UserId",
                table: "Notifications",
                newName: "IX_Notifications_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Templates",
                table: "Templates",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NotificationSettings",
                table: "NotificationSettings",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Notifications",
                table: "Notifications",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Templates",
                table: "Templates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NotificationSettings",
                table: "NotificationSettings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Notifications",
                table: "Notifications");

            migrationBuilder.RenameTable(
                name: "Templates",
                newName: "Template");

            migrationBuilder.RenameTable(
                name: "NotificationSettings",
                newName: "NotificationSetting");

            migrationBuilder.RenameTable(
                name: "Notifications",
                newName: "Notification");

            migrationBuilder.RenameIndex(
                name: "IX_Templates_NotificationType",
                table: "Template",
                newName: "IX_Template_NotificationType");

            migrationBuilder.RenameIndex(
                name: "IX_Templates_ChannelType",
                table: "Template",
                newName: "IX_Template_ChannelType");

            migrationBuilder.RenameIndex(
                name: "IX_NotificationSettings_NotificationType",
                table: "NotificationSetting",
                newName: "IX_NotificationSetting_NotificationType");

            migrationBuilder.RenameIndex(
                name: "IX_Notifications_UserId",
                table: "Notification",
                newName: "IX_Notification_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Template",
                table: "Template",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NotificationSetting",
                table: "NotificationSetting",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Notification",
                table: "Notification",
                column: "Id");
        }
    }
}
