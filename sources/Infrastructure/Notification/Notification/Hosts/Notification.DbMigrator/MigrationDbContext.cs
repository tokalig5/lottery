﻿using Microsoft.EntityFrameworkCore;
using Notification.DataAccess;

namespace Notification.DbMigrator
{
    /// <summary>
    /// Контекст для миграций.
    /// </summary>
    public class MigrationDbContext : NotificationBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="MigrationDbContext"/>.
        /// </summary>
        public MigrationDbContext(DbContextOptions<MigrationDbContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
    }
}
