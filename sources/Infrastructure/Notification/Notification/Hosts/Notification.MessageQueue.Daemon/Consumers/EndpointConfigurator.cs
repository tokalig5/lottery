﻿using Common.Infrastructure.Utils.Extensions;
using Lottery.Contracts.Events.Application;
using Lottery.Contracts.Events.Lot;
using MassTransit;
using MassTransit.RabbitMqTransport;
using Notification.Contracts.Events;
using Notification.MessageQueue.Daemon.Consumers.Application;
using Notification.MessageQueue.Daemon.Consumers.Lot;
using System;
using System.Collections.Generic;

namespace Notification.MessageQueue.Daemon.Consumers
{
    /// <summary>
    /// Message queue endpoint configurator.
    /// </summary>
    public static class EndpointConfigurator
    {
        /// <summary>
        /// Get endpoint configuratons.
        /// </summary>
        /// <returns></returns>
        public static IList<Action<IRabbitMqBusFactoryConfigurator, IBusRegistrationContext>> GetEndpointConfiguratons()
        {
            return new List<Action<IRabbitMqBusFactoryConfigurator, IBusRegistrationContext>>
            {
                (cfg, ctx) => cfg.AddPublishEndpoint<LotCreatedEvent, LotCreatedConsumer>(ctx),
                (cfg, ctx) => cfg.AddPublishEndpoint<ApplicationCreatedEvent, ApplicationCreatedConsumer>(ctx),
                (cfg, ctx) => cfg.AddPublishEndpoint<CreateChannelNotificationEvent, ChannelNotificationEventConsumer>(ctx)
            };
        }
    }
}
