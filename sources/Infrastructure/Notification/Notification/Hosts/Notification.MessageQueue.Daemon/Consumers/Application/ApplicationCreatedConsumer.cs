﻿using Lottery.Contracts.Events.Application;
using MassTransit;
using Notification.ApiClient.NotificationApiClient;
using Notification.Contracts.Enums;
using System.Threading.Tasks;

namespace Notification.MessageQueue.Daemon.Consumers.Application
{
    /// <summary>
    /// Обработчик события о подаче заявки на участие в лотерее.
    /// </summary>
    public class ApplicationCreatedConsumer : IConsumer<ApplicationCreatedEvent>
    {
        private readonly INotificationApiClient _apiClient;

        /// <summary>
        /// Инициализация экземпляра <see cref="ApplicationCreatedConsumer"/>.
        /// </summary>
        public ApplicationCreatedConsumer(INotificationApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        /// <inheritdoc/>
        public Task Consume(ConsumeContext<ApplicationCreatedEvent> context)
        {
            return _apiClient.CreateNotificationAsync(context.Message.Id, NotificationType.ApplicationCreated, context.CancellationToken);
        }
    }
}
