﻿using Lottery.Contracts.Events.Lot;
using MassTransit;
using Notification.ApiClient.NotificationApiClient;
using Notification.Contracts.Enums;
using System.Threading.Tasks;

namespace Notification.MessageQueue.Daemon.Consumers.Lot
{
    /// <summary>
    /// Обработчик события о создании лотереи.
    /// </summary>
    public class LotCreatedConsumer : IConsumer<LotCreatedEvent>
    {
        private readonly INotificationApiClient _apiClient;

        /// <summary>
        /// Инициализация экземпляра <see cref="LotCreatedConsumer"/>.
        /// </summary>
        public LotCreatedConsumer(INotificationApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        /// <inheritdoc/>
        public Task Consume(ConsumeContext<LotCreatedEvent> context)
        {
            return _apiClient.CreateNotificationAsync(context.Message.Id, NotificationType.LotCreated, context.CancellationToken);
        }
    }
}
