﻿using MassTransit;
using Notification.ApiClient.NotificationApiClient;
using Notification.Contracts.Events;
using System.Threading.Tasks;

namespace Notification.MessageQueue.Daemon.Consumers
{
    /// <summary>
    /// Обработчик команды на отправку уведомления в конкретный канал.
    /// </summary>
    public class ChannelNotificationEventConsumer : IConsumer<CreateChannelNotificationEvent>
    {
        private readonly INotificationApiClient _apiClient;

        /// <summary>
        /// Инициализация экземпляра <see cref="ChannelNotificationEventConsumer"/>.
        /// </summary>
        public ChannelNotificationEventConsumer(INotificationApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        /// <inheritdoc/>
        public Task Consume(ConsumeContext<CreateChannelNotificationEvent> context)
        {
            return _apiClient.CreateChannelNotificationAsync(context.Message.Model, context.CancellationToken);
        }
    }
}
