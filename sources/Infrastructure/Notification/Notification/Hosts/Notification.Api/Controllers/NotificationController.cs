using MediatR;
using Microsoft.AspNetCore.Mvc;
using Notification.Contracts.Requests;
using Notification.Handlers.Notifications.Commands;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.Api.Controllers
{
    /// <summary>
    /// Notification controller
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class NotificationController : ControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Инициализация экземпляра <see cref="NotificationController"/>
        /// </summary>
        public NotificationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Отправка уведомления.
        /// </summary>
        /// <param name="model">Модель отпрвки уведомления.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        [HttpPost]
        public async Task<IActionResult> Create([Required] CreateNotificationRequest model, CancellationToken cancellationToken)
        {
            var command = new CreateNotificationCommand(model);
            await _mediator.Send(command, cancellationToken);
            return Accepted();
        }

        /// <summary>
        /// Отправка уведомления в конкретный канал.
        /// </summary>
        /// <param name="model">Модель запроса на отправку уведомления.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        [HttpPost("channel")]
        public async Task<IActionResult> CreateByChannel([Required] CreateChannelNotificationRequest model, CancellationToken cancellationToken)
        {
            var command = new CreateChannelNotificationCommand(model);
            await _mediator.Send(command, cancellationToken);
            return StatusCode((int)HttpStatusCode.Created);
        }
    }
}