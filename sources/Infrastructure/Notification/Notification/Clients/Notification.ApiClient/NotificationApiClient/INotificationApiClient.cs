﻿using Notification.Contracts.Enums;
using Notification.Contracts.Requests;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.ApiClient.NotificationApiClient
{
    /// <summary>
    /// Notification service API Client
    /// </summary>
    public interface INotificationApiClient
    {
        /// <summary>
        /// Отправка уведомления.
        /// </summary>
        /// <param name="entityId">Идентификатор сущности.</param>
        /// <param name="notificationType">Тип уведомления.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        Task CreateNotificationAsync(Guid entityId, NotificationType notificationType, CancellationToken cancellationToken);

        /// <summary>
        /// Отправка уведомления в канал.
        /// </summary>
        /// <param name="entityId">Идентификатор сущности.</param>
        /// <param name="notificationType">Тип уведомления.</param>
        /// <param name="channelType">Тип канала.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        Task CreateChannelNotificationAsync(CreateChannelNotificationRequest model, CancellationToken cancellationToken);
    }
}