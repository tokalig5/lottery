﻿using Common.Infrastructure.Utils.Extensions;
using Flurl;
using Notification.ApiClient.Exceptions;
using Notification.Contracts.Enums;
using Notification.Contracts.Requests;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.ApiClient.NotificationApiClient
{
    /// <inheritdoc cref="INotificationApiClient"/>
    public partial class NotificationApiClient : INotificationApiClient
    {
        private readonly HttpClient _httpClient;
        private const string ApiRoutePrefix = "api/v1";
        private const string NotificationApiPath = "notification";

        /// <summary>
        /// Инициализация экземпляра <see cref="NotificationApiClient"/>.
        /// </summary>
        public NotificationApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <inheritdoc/>
        public Task CreateNotificationAsync(Guid entityId, NotificationType notificationType, CancellationToken cancellationToken)
        {
            var model = new CreateNotificationRequest(entityId, notificationType);
            return _httpClient.PostAsync(GetNotificationApiUrl(), model,
            (statusCode, message, exception) => throw new NotificationApiClientException(statusCode, $"Ошибка отправки уведомления типа {notificationType}. Ошибка: {message}", exception),
            cancellationToken);
        }

        /// <inheritdoc/>
        public Task CreateChannelNotificationAsync(CreateChannelNotificationRequest model, CancellationToken cancellationToken)
        {
            return _httpClient.PostAsync(GetNotificationApiUrl("channel"), model,
            (statusCode, message, exception) => throw new NotificationApiClientException(statusCode, $"Ошибка отправки уведомления типа {model.NotificationType} в канал {model.ChannelType}. Ошибка: {message}", exception),
            cancellationToken);
        }

        private static string GetNotificationApiUrl(params string[] parts)
        {
            return Url.Combine(ApiRoutePrefix, NotificationApiPath, Url.Combine(parts));
        }
    }
}