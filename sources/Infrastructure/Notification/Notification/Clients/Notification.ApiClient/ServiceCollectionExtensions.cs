﻿using Common.Contracts.Settings;
using Microsoft.Extensions.DependencyInjection;
using Notification.ApiClient.NotificationApiClient;
using System;

namespace Notification.ApiClient
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление NotificationApiClient
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="settings">Настройки Proxy</param>
        /// <returns>Коллекция сервисов</returns>
        public static IServiceCollection AddNotificationApiClient(this IServiceCollection services, ProxySettings settings)
        {
            services.AddHttpClient<INotificationApiClient, NotificationApiClient.NotificationApiClient>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(settings.NotificationServiceUrl);
            });
            return services;
        }
    }
}
