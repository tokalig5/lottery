﻿using System;
using System.Net;
using Common.Contracts.WebApi.Exceptions;

namespace Notification.ApiClient.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе NotificationApiClient
    /// </summary>
    [Serializable]
    public class NotificationApiClientException : ApiClientException
    {
        /// <summary>
        /// Инициализация экземпляра <see cref="NotificationApiClientException"/>.
        /// </summary>
        /// <param name="statusCode">HTTP-код ошибки запроса.</param>
        /// <param name="message">Сообщение ошибки запроса.</param>
        /// <param name="innerException">Внутреннее исключение.</param>
        public NotificationApiClientException(HttpStatusCode statusCode, string message, Exception innerException) : base(statusCode, message, innerException)
        {
        }
    }
}
