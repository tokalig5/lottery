﻿using MediatR;
using Notification.Contracts.Requests;

namespace Notification.Handlers.Notifications.Commands
{
    /// <summary>
    /// Команда на создание уведомления.
    /// </summary>
    public class CreateNotificationCommand : IRequest<bool>
    {
        /// <summary>
        /// Модель создания уведомления.
        /// </summary>
        public CreateNotificationRequest Model { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="CreateNotificationCommand"/>.
        /// </summary>
        public CreateNotificationCommand(CreateNotificationRequest model)
        {
            Model = model;
        }
    }
}
