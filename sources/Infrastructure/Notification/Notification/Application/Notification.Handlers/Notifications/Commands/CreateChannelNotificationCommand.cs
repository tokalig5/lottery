﻿using MediatR;
using Notification.Contracts.Requests;

namespace Notification.Handlers.Notifications.Commands
{
    /// <summary>
    /// Команда на создание уведомления в конкретный канал.
    /// </summary>
    public class CreateChannelNotificationCommand : IRequest<bool>
    {
        /// <summary>
        /// Модель уведомления в конкретный канал.
        /// </summary>
        public CreateChannelNotificationRequest Model { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="CreateChannelNotificationCommand"/>.
        /// </summary>
        public CreateChannelNotificationCommand(CreateChannelNotificationRequest model)
        {
            Model = model;
        }
    }
}
