﻿using MediatR;
using Notification.AppServices.Notifications.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.Handlers.Notifications.Commands
{
    /// <summary>
    /// Обработчик команды на создание уведомления.
    /// </summary>
    public class CreateNotificationCommandHandler : IRequestHandler<CreateNotificationCommand, bool>
    {
        private readonly INotificationService _notificationService;

        /// <summary>
        /// Инициализация экземпляра <see cref="CreateNotificationCommandHandler"/>.
        /// </summary>
        public CreateNotificationCommandHandler(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        /// <inheritdoc/>
        public Task<bool> Handle(CreateNotificationCommand request, CancellationToken cancellationToken)
        {
            return _notificationService.CreateNotificationsAsync(request.Model, cancellationToken);
        }
    }
}
