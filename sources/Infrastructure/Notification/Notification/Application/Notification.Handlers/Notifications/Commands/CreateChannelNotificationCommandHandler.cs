﻿using MediatR;
using Notification.AppServices.Notifications.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.Handlers.Notifications.Commands
{
    /// <summary>
    /// Обработчик команды на создание уведомления в конкретный канал.
    /// </summary>
    public class CreateChannelNotificationCommandHandler : IRequestHandler<CreateChannelNotificationCommand, bool>
    {
        private readonly INotificationService _notificationService;

        /// <summary>
        /// Инициализация экземпляра <see cref="CreateChannelNotificationCommandHandler"/>.
        /// </summary>
        public CreateChannelNotificationCommandHandler(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        /// <inheritdoc/>
        public Task<bool> Handle(CreateChannelNotificationCommand request, CancellationToken cancellationToken)
        {
            return _notificationService.CreateChannelNotificationAsync(request.Model, cancellationToken);
        }
    }
}
