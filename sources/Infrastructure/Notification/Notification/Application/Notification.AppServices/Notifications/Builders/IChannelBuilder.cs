﻿using Notification.Contracts.Enums;

namespace Notification.AppServices.Notifications.Builders
{
    /// <summary>
    /// Билдер уведомлений по каналам.
    /// </summary>
    public interface IChannelBuilder
    {
        /// <summary>
        /// Получение билдера уведомления для конкретного канала.
        /// </summary>
        /// <param name="notificationType">Тип уведомления.</param>
        /// <returns>Билдер уведомления.</returns>
        INotificationBuilder GetNotificationBuilder(NotificationType notificationType);
    }
}
