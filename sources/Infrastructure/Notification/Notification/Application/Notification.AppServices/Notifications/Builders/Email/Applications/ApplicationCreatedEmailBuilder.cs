﻿using Notification.AppServices.Notifications.Models;
using Notification.AppServices.Templates.Services;
using Notification.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Notifications.Builders.Email.Applications
{
    /// <summary>
    /// Заявка подана (в Email).
    /// </summary>
    public class ApplicationCreatedEmailBuilder : INotificationBuilder
    {
        private readonly ITemplateService _templateService;

        /// <summary>
        /// Инициализация экземпляра <see cref="ApplicationCreatedEmailBuilder"/>.
        /// </summary>
        /// <param name="templateService"></param>
        public ApplicationCreatedEmailBuilder(ITemplateService templateService)
        {
            _templateService = templateService;
        }

        /// <inheritdoc/>
        public async Task<BaseNotificationInfoModel> BuildAsync(Guid entityId, CancellationToken cancellation)
        {
            var template = await _templateService.GetByChannelIdOrDefaultAsync(NotificationType.ApplicationCreated, EventChannelType.Email, cancellation);
            var fields = new Dictionary<string, string>();
            var notification = new EmailInfoModel
            {
                Subject = await _templateService.RenderAsync(template.Subject, fields, cancellation),
                Message = await _templateService.RenderAsync(template.Body, fields, cancellation),
            };
            return notification;
        }
    }
}
