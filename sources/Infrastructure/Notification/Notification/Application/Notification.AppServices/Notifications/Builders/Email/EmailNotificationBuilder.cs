﻿using Common.Contracts.WebApi.Exceptions;
using Notification.AppServices.Notifications.Builders.Email.Applications;
using Notification.AppServices.Notifications.Builders.Email.Lots;
using Notification.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Notification.AppServices.Notifications.Builders.Email
{
    /// <summary>
    /// Билдер уведомлений емейла.
    /// </summary>
    public class EmailNotificationBuilder : IChannelBuilder
    {
        private readonly IEnumerable<INotificationBuilder> _notificationBuilders;

        /// <summary>
        /// Инициализация экземпляра <see cref="EmailNotificationBuilder"/>.
        /// </summary>        
        public EmailNotificationBuilder(IEnumerable<INotificationBuilder> notificationBuilders)
        {
            _notificationBuilders = notificationBuilders;
        }

        /// <inheritdoc/>
        public INotificationBuilder GetNotificationBuilder(NotificationType notificationType)
        {
            return notificationType switch
            {
                NotificationType.LotCreated => GetBuilder(typeof(LotCreatedEmailBuilder)),
                NotificationType.ApplicationCreated => GetBuilder(typeof(ApplicationCreatedEmailBuilder)),
                _ => throw new BusinessLogicException($"Ошибка построения уведомления типа {notificationType} для емейла")
            };
        }

        private INotificationBuilder GetBuilder(Type type)
        {
            return _notificationBuilders.First(s => s.GetType() == type);
        }
    }
}
