﻿using Common.Contracts.WebApi.Exceptions;
using Notification.AppServices.Notifications.Builders.PersonalAccount.Applications;
using Notification.AppServices.Notifications.Builders.PersonalAccount.Lots;
using Notification.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Notification.AppServices.Notifications.Builders.PersonalAccount
{
    /// <summary>
    /// Билдер уведомлений личного кабинета.
    /// </summary>
    public class PersonalAccountNotificationBuilder : IChannelBuilder
    {
        private readonly IEnumerable<INotificationBuilder> _notificationBuilders;

        /// <summary>
        /// Инициализация экземпляра <see cref="PersonalAccountNotificationBuilder"/>.
        /// </summary>        
        public PersonalAccountNotificationBuilder(IEnumerable<INotificationBuilder> notificationBuilders)
        {
            _notificationBuilders = notificationBuilders;
        }

        /// <inheritdoc/>
        public INotificationBuilder GetNotificationBuilder(NotificationType notificationType)
        {
            return notificationType switch
            {
                NotificationType.LotCreated => GetBuilder(typeof(LotCreatedPersonalAccountBuilder)),
                NotificationType.ApplicationCreated => GetBuilder(typeof(ApplicationCreatedPersonalAccountBuilder)),
                _ => throw new BusinessLogicException($"Ошибка построения уведомления типа {notificationType} для личного кабинета")
            };
        }

        private INotificationBuilder GetBuilder(Type type)
        {
            return _notificationBuilders.First(s => s.GetType() == type);
        }
    }
}
