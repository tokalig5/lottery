﻿using Notification.AppServices.Notifications.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Notifications.Builders
{
    /// <summary>
    /// Билдер уведомлений.
    /// </summary>
    public interface INotificationBuilder
    {
        /// <summary>
        /// Получение модели с информацией для уведомления.
        /// </summary>
        /// <param name="entityId">Идентификатор сущности, п окоторой создается уведомление.</param>
        /// <param name="cancellation">Токен отмены.</param>
        /// <returns>Модель с информацией для уведомления.</returns>
        Task<BaseNotificationInfoModel> BuildAsync(Guid entityId, CancellationToken cancellation);
    }
}
