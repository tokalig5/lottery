﻿using Notification.AppServices.Notifications.Models;
using Notification.AppServices.Templates.Services;
using Notification.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Notifications.Builders.Email.Lots
{
    /// <summary>
    /// Лотерея создана (в Email).
    /// </summary>
    public class LotCreatedEmailBuilder : INotificationBuilder
    {
        private readonly ITemplateService _templateService;

        /// <summary>
        /// Инициализация экземпляра <see cref="LotCreatedEmailBuilder"/>.
        /// </summary>
        /// <param name="templateService"></param>
        public LotCreatedEmailBuilder(ITemplateService templateService)
        {
            _templateService = templateService;
        }

        /// <inheritdoc/>
        public async Task<BaseNotificationInfoModel> BuildAsync(Guid entityId, CancellationToken cancellation)
        {
            var template = await _templateService.GetByChannelIdOrDefaultAsync(NotificationType.LotCreated, EventChannelType.Email, cancellation);
            var fields = new Dictionary<string, string>();
            var notification = new EmailInfoModel
            {
                Subject = await _templateService.RenderAsync(template.Subject, fields, cancellation),
                Message = await _templateService.RenderAsync(template.Body, fields, cancellation),
            };
            return notification;
        }
    }
}
