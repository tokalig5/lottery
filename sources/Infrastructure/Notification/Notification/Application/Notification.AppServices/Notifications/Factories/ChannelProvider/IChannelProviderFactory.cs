﻿using Notification.AppServices.Notifications.Providers;
using Notification.Contracts.Enums;

namespace Notification.AppServices.Notifications.Factories.ChannelProvider
{
    /// <summary>
    /// Фабрика провайдеров каналов.
    /// </summary>
    public interface IChannelProviderFactory
    {
        /// <summary>
        /// Получение необходимого канала связи.
        /// </summary>
        /// <param name="channelType">Тип канала.</param>
        /// <returns>Провайдер канала.</returns>
        IChannelProvider Create(EventChannelType channelType);
    }
}
