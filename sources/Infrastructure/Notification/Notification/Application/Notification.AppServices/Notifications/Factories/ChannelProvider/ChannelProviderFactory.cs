﻿using Common.Contracts.WebApi.Exceptions;
using Notification.AppServices.Notifications.Providers;
using Notification.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Notification.AppServices.Notifications.Factories.ChannelProvider
{
    /// <inheritdoc cref="IChannelProviderFactory"/>
    public class ChannelProviderFactory : IChannelProviderFactory
    {
        private readonly IEnumerable<IChannelProvider> _channels;

        /// <summary>
        /// Инициализация экземпляра <see cref="ChannelProviderFactory"/>.
        /// </summary>
        public ChannelProviderFactory(IEnumerable<IChannelProvider> channels)
        {
            _channels = channels;
        }

        /// <inheritdoc/>
        public IChannelProvider Create(EventChannelType channelType)
        {
            return channelType switch
            {
                EventChannelType.Email => GetProvider(typeof(EmailProvider)),
                EventChannelType.PersonalAccount => GetProvider(typeof(PersonalAccountProvider)),
                _ => throw new BusinessLogicException($"Ошибка построения провейдера канала уведомления {channelType}")
            };
        }

        private IChannelProvider GetProvider(Type type)
        {
            return _channels.First(s => s.GetType() == type);
        }
    }
}
