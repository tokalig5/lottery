﻿using Common.Contracts.WebApi.Exceptions;
using Notification.AppServices.Notifications.Builders;
using Notification.AppServices.Notifications.Builders.Email;
using Notification.AppServices.Notifications.Builders.PersonalAccount;
using Notification.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Notification.AppServices.Notifications.Factories.ChannelBuilder
{
    /// <inheritdoc cref="IChannelBuilderFactory"/>
    public class ChannelBuilderFactory : IChannelBuilderFactory
    {
        private readonly IEnumerable<IChannelBuilder> _channelBuilders;

        /// <summary>
        /// Инициализация экземпляра <see cref="ChannelBuilderFactory"/>.
        /// </summary>
        public ChannelBuilderFactory(IEnumerable<IChannelBuilder> channelBuilders)
        {
            _channelBuilders = channelBuilders;
        }

        /// <inheritdoc/>
        public IChannelBuilder Create(EventChannelType channelType)
        {
            return channelType switch
            {
                EventChannelType.Email => GetBuilder(typeof(EmailNotificationBuilder)),
                EventChannelType.PersonalAccount => GetBuilder(typeof(PersonalAccountNotificationBuilder)),
                _ => throw new BusinessLogicException($"Ошибка построения билдера уведомления для канала {channelType}")
            };
        }

        private IChannelBuilder GetBuilder(Type type)
        {
            return _channelBuilders.First(s => s.GetType() == type);
        }
    }
}
