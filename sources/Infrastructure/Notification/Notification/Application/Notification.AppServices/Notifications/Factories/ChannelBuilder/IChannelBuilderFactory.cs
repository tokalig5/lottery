﻿using Notification.AppServices.Notifications.Builders;
using Notification.Contracts.Enums;

namespace Notification.AppServices.Notifications.Factories.ChannelBuilder
{
    /// <summary>
    /// Фабрика билдеров каналов уведомлений.
    /// </summary>
    public interface IChannelBuilderFactory
    {
        /// <summary>
        /// Создание билдера по конкретному каналу.
        /// </summary>
        /// <param name="channelType">Тип канала.</param>
        /// <returns>Билдер уведомлений по каналам.</returns>
        IChannelBuilder Create(EventChannelType channelType);
    }
}
