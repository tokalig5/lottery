﻿using Notification.Contracts.Enums;
using Notification.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Notifications.Repositories
{
    /// <summary>
    /// Репозиторий по работе с настройками уведомлений.
    /// </summary>
    public interface INotificationSettingRepository
    {
        /// <summary>
        /// Получение активных настроек типа уведомления.
        /// </summary>
        /// <param name="notificationType">Тип уведомления.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Список активных настроек.</returns>
        Task<List<NotificationSetting>> GetActiveTypeSettingsAsync(NotificationType notificationType, CancellationToken cancellationToken);
    }
}
