﻿using Notification.Contracts.Requests;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Notifications.Services
{
    /// <summary>
    /// Сервис по работе с уведомлениями.
    /// </summary>
    public interface INotificationService
    {
        /// <summary>
        /// Создание уведомления.
        /// </summary>
        /// <param name="model">Модель создания уведомления.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Параметр успешности выполнения операции.</returns>
        Task<bool> CreateNotificationsAsync(CreateNotificationRequest model, CancellationToken cancellationToken);

        /// <summary>
        /// Отправка уведомления в канал.
        /// </summary>
        /// <param name="model">Модель отправки уведомления в канал.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Параметр успешности выполнения операции.</returns>
        Task<bool> CreateChannelNotificationAsync(CreateChannelNotificationRequest model, CancellationToken cancellationToken);
    }
}
