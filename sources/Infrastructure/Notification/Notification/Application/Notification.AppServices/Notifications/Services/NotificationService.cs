﻿using Common.Contracts.Bus.Enums;
using Common.Infrastructure.Bus.Services.BusEventContainer;
using Notification.AppServices.Notifications.Factories.ChannelBuilder;
using Notification.AppServices.Notifications.Factories.ChannelProvider;
using Notification.AppServices.Notifications.Repositories;
using Notification.Contracts.Events;
using Notification.Contracts.Requests;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Notifications.Services
{
    /// <inheritdoc cref="INotificationService"/>
    public class NotificationService : INotificationService
    {
        private readonly IBusEventContainer _busEventContainer;
        private readonly INotificationRepository _notificationRepository;
        private readonly INotificationSettingRepository _notificationSettingRepository;
        private readonly IChannelProviderFactory _channelProviderFactory;
        private readonly IChannelBuilderFactory _channelBuilderFactory;

        /// <summary>
        /// Инициализация экземпляра <see cref="NotificationService"/>.
        /// </summary>
        public NotificationService(IBusEventContainer busEventContainer, INotificationRepository notificationRepository, INotificationSettingRepository notificationSettingRepository, IChannelProviderFactory channelProviderFactory, IChannelBuilderFactory channelBuilderFactory)
        {
            _busEventContainer = busEventContainer;
            _notificationRepository = notificationRepository;
            _notificationSettingRepository = notificationSettingRepository;
            _channelProviderFactory = channelProviderFactory;
            _channelBuilderFactory = channelBuilderFactory;
        }

        /// <inheritdoc/>
        public async Task<bool> CreateNotificationsAsync(CreateNotificationRequest model, CancellationToken cancellationToken)
        {
            var activeChannels = await _notificationSettingRepository.GetActiveTypeSettingsAsync(model.NotificationType, cancellationToken);
            activeChannels.ForEach(channel =>
            {
                var command = new CreateChannelNotificationRequest(model.EntityId, model.NotificationType, channel.ChannelType);
                _busEventContainer.Add(new CreateChannelNotificationEvent(command), MessageEventType.Always);
            });

            return true;
        }

        /// <inheritdoc/>
        public async Task<bool> CreateChannelNotificationAsync(CreateChannelNotificationRequest model, CancellationToken cancellationToken)
        {
            var channelBuilder = _channelBuilderFactory.Create(model.ChannelType);
            var notificationBuilder = channelBuilder.GetNotificationBuilder(model.NotificationType);
            var notification = await notificationBuilder.BuildAsync(model.EntityId, cancellationToken);

            var provider = _channelProviderFactory.Create(model.ChannelType);
            return await provider.SendAsync(notification, cancellationToken);
        }
    }
}
