﻿using Notification.AppServices.Notifications.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Notifications.Providers
{
    /// <summary>
    /// Провайдер работы канала Email.
    /// </summary>
    public class EmailProvider : IChannelProvider
    {
        /// <inheritdoc/>
        public Task<bool> SendAsync(BaseNotificationInfoModel notification, CancellationToken cancellation)
        {
            if (notification is EmailInfoModel notificationModel)
            {

            }
            return Task.FromResult(true);
        }
    }
}
