﻿using Notification.AppServices.Notifications.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Notifications.Providers
{
    /// <summary>
    /// Првоайдер работы канала личного кабинета.
    /// </summary>
    public class PersonalAccountProvider : IChannelProvider
    {
        /// <inheritdoc/>
        public Task<bool> SendAsync(BaseNotificationInfoModel notification, CancellationToken cancellation)
        {
            if (notification is PersonalAccountInfoModel notificationModel)
            {

            }
            return Task.FromResult(true);
        }
    }
}
