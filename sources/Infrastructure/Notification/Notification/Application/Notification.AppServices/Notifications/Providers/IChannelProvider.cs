﻿using Notification.AppServices.Notifications.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Notifications.Providers
{
    /// <summary>
    /// Провайдер работы канала.
    /// </summary>
    public interface IChannelProvider
    {
        /// <summary>
        /// Отправка уведомления в канал.
        /// </summary>
        /// <param name="notification">Уведомление.</param>
        /// <param name="cancellation">Токен отмены.</param>        
        Task<bool> SendAsync(BaseNotificationInfoModel notification, CancellationToken cancellation);
    }
}
