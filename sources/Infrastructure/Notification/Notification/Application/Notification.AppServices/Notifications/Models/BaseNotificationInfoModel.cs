﻿namespace Notification.AppServices.Notifications.Models
{
    /// <summary>
    /// Базовая модель уведомлений.
    /// </summary>
    public abstract class BaseNotificationInfoModel
    {
        /// <summary>
        /// Тема сообщения.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Текст сообщения.
        /// </summary>
        public string Message { get; set; }
    }
}
