﻿using System.Collections.Generic;

namespace Notification.AppServices.Notifications.Models
{
    /// <summary>
    /// Модель информации для уведомления в ЛК.
    /// </summary>
    public class PersonalAccountInfoModel : BaseNotificationInfoModel
    {
        /// <summary>
        /// Список идентификаторов пользователей для отправки уведомления в ЛК.
        /// </summary>
        public IList<string> UserIds { get; set; } = new List<string>();
    }
}
