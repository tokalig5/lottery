﻿using System.Collections.Generic;

namespace Notification.AppServices.Notifications.Models
{
    /// <summary>
    /// Модель информации для уведомления на email.
    /// </summary>
    public class EmailInfoModel : BaseNotificationInfoModel
    {
        /// <summary>
        /// Список email адресов для отправки уведомления на почту.
        /// </summary>
        public IList<string> Emails { get; set; } = new List<string>();
    }
}
