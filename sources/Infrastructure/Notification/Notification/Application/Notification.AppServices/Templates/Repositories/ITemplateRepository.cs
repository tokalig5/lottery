﻿using Notification.Contracts.Enums;
using Notification.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Templates.Repositories
{
    /// <summary>
    /// Репозиторий по работе с шаблонами увдедомлений.
    /// </summary>
    public interface ITemplateRepository
    {
        /// <summary>
        /// Получение шаблона уведомления для канала или шаблона по умолчанию.
        /// </summary>
        /// <param name="notificationType">Тип уведомления.</param>
        /// <param name="channelType">Тип канала.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Шаблон.</returns>
        Task<Template> GetByChannelIdOrDefaultAsync(NotificationType notificationType, EventChannelType channelType, CancellationToken cancellationToken);
    }
}
