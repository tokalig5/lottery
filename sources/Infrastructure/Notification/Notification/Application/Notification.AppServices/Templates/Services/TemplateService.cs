﻿using Common.Contracts.WebApi.Exceptions;
using Newtonsoft.Json;
using Notification.AppServices.Templates.Repositories;
using Notification.Contracts.Enums;
using Notification.Entities;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Templates.Services
{
    /// <inheritdoc cref="ITemplateService"/>
    public class TemplateService : ITemplateService
    {
        private readonly ITemplateRepository _templateRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="TemplateService"/>.
        /// </summary>
        public TemplateService(ITemplateRepository templateRepository)
        {
            _templateRepository = templateRepository;
        }

        /// <inheritdoc/>
        public Task<Template> GetByChannelIdOrDefaultAsync(NotificationType notificationType, EventChannelType channelType, CancellationToken cancellationToken)
        {
            return _templateRepository.GetByChannelIdOrDefaultAsync(notificationType, channelType, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<string> RenderAsync(string template, IReadOnlyDictionary<string, string> fields, CancellationToken cancellationToken)
        {
            var parsedTemplate = Scriban.Template.Parse(template);
            if (parsedTemplate.HasErrors)
            {
                var messages = parsedTemplate.Messages.ToString();
                throw new UnprocessableEntityException($"Ошибка парсинга шаблона {template}. Ошибки: {messages}");
            }
            var parsedFields = fields.ToDictionary(pair => pair.Key, pair => ConvertModelValue(pair.Value));
            return parsedTemplate.RenderAsync(parsedFields).AsTask();
        }

        private static object ConvertModelValue(string value)
        {
            switch (value)
            {
                case { } when value.StartsWith("{"):
                    {
                        var item = JsonConvert.DeserializeObject<ExpandoObject>(value);
                        return GetDictionaryObject(item);
                    }
                case { } when value.StartsWith("["):
                    {
                        var items = JsonConvert.DeserializeObject<ExpandoObject[]>(value);
                        return items.Select(item => GetDictionaryObject(item));
                    }
                default: return value;
            };
        }

        private static IDictionary<string, string> GetDictionaryObject(ExpandoObject item)
        {
            return item.ToDictionary(kvp => kvp.Key, kvp => kvp.Value?.ToString());
        }
    }
}
