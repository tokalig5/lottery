﻿using Notification.Contracts.Enums;
using Notification.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Notification.AppServices.Templates.Services
{
    /// <summary>
    /// Сервис по работе с шаблонами уведомлений.
    /// </summary>
    public interface ITemplateService
    {
        /// <summary>
        /// Получение шаблона уведомления для канала или шаблона по умолчанию.
        /// </summary>
        /// <param name="notificationType">Тип уведомления.</param>
        /// <param name="channelType">Тип канала.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Шаблон.</returns>
        Task<Template> GetByChannelIdOrDefaultAsync(NotificationType notificationType, EventChannelType channelType, CancellationToken cancellationToken);

        /// <summary>
        /// Рендеринг сообщения из шаблона.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="fields">Поля для рендеринга.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Готовое сообщение.</returns>
        Task<string> RenderAsync(string template, IReadOnlyDictionary<string, string> fields, CancellationToken cancellationToken);
    }
}
