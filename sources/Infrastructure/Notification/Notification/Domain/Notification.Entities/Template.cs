﻿using Common.Contracts.DataAccess;
using Notification.Contracts.Enums;

namespace Notification.Entities
{
    /// <summary>
    /// Сущность шаблона уведомления.
    /// </summary>
    public class Template : BaseEntity
    {
        /// <summary>
        /// Описание шаблона.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Тема шаблона.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Тело шаблона.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Тип уведомления.
        /// </summary>
        public NotificationType NotificationType { get; set; }

        /// <summary>
        /// Тип канала уведомления.
        /// </summary>
        public EventChannelType ChannelType { get; set; }
    }
}
