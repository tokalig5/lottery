﻿using Common.Contracts.DataAccess;
using Notification.Contracts.Enums;

namespace Notification.Entities
{
    /// <summary>
    /// Настройка для уведомления конкретного типа.
    /// </summary>
    public class NotificationSetting : BaseEntity
    {
        /// <summary>
        /// Тип уведомления.
        /// </summary>
        public NotificationType NotificationType { get; set; }

        /// <summary>
        /// Тип канала уведомления.
        /// </summary>
        public EventChannelType ChannelType { get; set; }

        /// <summary>
        /// Параметр активности канала уведомления.
        /// </summary>
        public bool IsActive { get; set; }
    }
}