﻿using Common.Contracts.Settings;
using GridRegistry.ApiClient.GridRegistryApiClient;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GridRegistry.ApiClient
{
    /// <summary>
    /// Методы расширения АПИ клиента сервиса лотереи
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление FinanceApiClient
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="settings">Настройки Proxy</param>
        /// <returns>Коллекция сервисов</returns>
        public static IServiceCollection AddGridRegistryApiClient(this IServiceCollection services, ProxySettings settings)
        {
            services.AddHttpClient<IGridRegistryApiClient, GridRegistryApiClient.GridRegistryApiClient>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(settings.GridRegistryServiceUrl);
            });
            return services;
        }
    }
}
