﻿using Common.Infrastructure.Utils.Extensions;
using GridRegistry.ApiClient.Exceptions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GridRegistry.ApiClient.GridRegistryApiClient
{
    public partial class GridRegistryApiClient
    {
        private const string ApplicationApiPath = "application";

        /// <inheritdoc/>
        public Task CreateApplicationAsync(Guid id, CancellationToken cancellationToken)
        {
            return _httpClient.PostAsync(GetApplicationApiUrl(id.ToString(), "created"), 
            (statusCode, message, exception) => throw new GridRegistryApiClientException(statusCode, $"Ошибка создания заявки {id} в реестре. Ошибка: {message}", exception),
            cancellationToken);
        }
    }
}
