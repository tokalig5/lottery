﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace GridRegistry.ApiClient.GridRegistryApiClient
{
    /// <summary>
    /// GridRegistry service API Client
    /// </summary>
    public interface IGridRegistryApiClient
    {
        #region Lot

        /// <summary>
        /// Создание лотереи в реестре.
        /// </summary>
        /// <param name="id">Идентификатор лотереи.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        Task CreateLotAsync(Guid id, CancellationToken cancellationToken);

        #endregion

        #region Application

        /// <summary>
        /// Создание заявки на участие в реестре.
        /// </summary>
        /// <param name="id">Идентификатор заявки на участие.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        Task CreateApplicationAsync(Guid id, CancellationToken cancellationToken);

        #endregion
    }
}