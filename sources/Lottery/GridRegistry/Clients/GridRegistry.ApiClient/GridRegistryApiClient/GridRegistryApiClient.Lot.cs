﻿using Common.Infrastructure.Utils.Extensions;
using GridRegistry.ApiClient.Exceptions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GridRegistry.ApiClient.GridRegistryApiClient
{
    public partial class GridRegistryApiClient
    {
        private const string LotApiPath = "lot";

        /// <inheritdoc/>
        public Task CreateLotAsync(Guid id, CancellationToken cancellationToken)
        {
            return _httpClient.PostAsync(GetLotApiUrl(id.ToString(), "created"), 
            (statusCode, message, exception) => throw new GridRegistryApiClientException(statusCode, $"Ошибка создания лотереи {id} в реестре. Ошибка: {message}", exception),
            cancellationToken);
        }
    }
}
