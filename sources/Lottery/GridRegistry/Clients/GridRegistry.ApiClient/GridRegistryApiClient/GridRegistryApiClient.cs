﻿using Flurl;
using System.Net.Http;

namespace GridRegistry.ApiClient.GridRegistryApiClient
{
    /// <summary>
    /// GridRegistry service API Client
    /// </summary>
    public partial class GridRegistryApiClient : IGridRegistryApiClient
    {
        private readonly HttpClient _httpClient;
        private const string ApiRoutePrefix = "api/v1";

        public GridRegistryApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        private static string GetLotApiUrl(params string[] parts)
        {
            return Url.Combine(ApiRoutePrefix, LotApiPath, Url.Combine(parts));
        }

        private static string GetApplicationApiUrl(params string[] parts)
        {
            return Url.Combine(ApiRoutePrefix, ApplicationApiPath, Url.Combine(parts));
        }
    }
}