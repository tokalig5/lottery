﻿using System;
using System.Net;
using Common.Contracts.WebApi.Exceptions;

namespace GridRegistry.ApiClient.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе GridRegistryApiClient
    /// </summary>
    [Serializable]
    public class GridRegistryApiClientException : ApiClientException
    {
        /// <summary>
        /// Инициализация экземпляра <see cref="GridRegistryApiClientException"/>.
        /// </summary>
        /// <param name="statusCode">HTTP-код ошибки запроса.</param>
        /// <param name="message">Сообщение ошибки запроса.</param>
        /// <param name="innerException">Внутреннее исключение.</param>
        public GridRegistryApiClientException(HttpStatusCode statusCode, string message, Exception innerException) : base(statusCode, message, innerException)
        {
        }
    }
}
