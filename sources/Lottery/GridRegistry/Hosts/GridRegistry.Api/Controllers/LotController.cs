﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace GridRegistry.Api.Controllers
{
    /// <summary>
    /// Lot grid controller
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class LotController : ControllerBase
    {
        public LotController()
        {
        }

        /// <summary>
        /// Создание лотереи.
        /// </summary>
        /// <param name="id">Идентификатор лотереи.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        [HttpPost("{id}")]
        public IActionResult Create([Required] Guid id, CancellationToken cancellationToken)
        {
            Console.WriteLine($"Лот {id} создан");
            return Ok();
        }
    }
}