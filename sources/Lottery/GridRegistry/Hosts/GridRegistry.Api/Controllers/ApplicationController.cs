using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace GridRegistry.Api.Controllers
{
    /// <summary>
    /// Application grid controller
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ApplicationController : ControllerBase
    {
        public ApplicationController()
        {
        }

        /// <summary>
        /// �������� ������ �� ������� � �������.
        /// </summary>
        /// <param name="id">������������� ������.</param>
        /// <param name="cancellationToken">����� ������.</param>        
        [HttpPost("{id}")]
        public IActionResult Create([Required] Guid id, CancellationToken cancellationToken)
        {
            Console.WriteLine($"������ {id} �������");
            return Ok();
        }
    }
}