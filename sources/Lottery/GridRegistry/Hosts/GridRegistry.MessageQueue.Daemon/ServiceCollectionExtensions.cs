﻿using Common.Contracts.Settings;
using Common.Infrastructure;
using GridRegistry.ApiClient;
using GridRegistry.MessageQueue.Daemon.Consumers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GridRegistry.MessageQueue.Daemon
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureBusService(configuration);
            services.AddApiClient(configuration);
            return services;
        }

        private static IServiceCollection ConfigureBusService(this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = new RabbitSettings();
            configuration.Bind(nameof(RabbitSettings), rabbitSettings);

            services.AddBusService(rabbitSettings, EndpointConfigurator.GetEndpointConfiguratons());
            return services;
        }

        private static IServiceCollection AddApiClient(this IServiceCollection services, IConfiguration configuration)
        {
            var proxySettings = new ProxySettings();
            configuration.Bind(nameof(ProxySettings), proxySettings);
            services.AddGridRegistryApiClient(proxySettings);
            return services;
        }
    }
}
