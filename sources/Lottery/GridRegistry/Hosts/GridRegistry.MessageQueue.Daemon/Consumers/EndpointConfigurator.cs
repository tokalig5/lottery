﻿using Common.Infrastructure.Utils.Extensions;
using GridRegistry.MessageQueue.Daemon.Consumers.Application;
using GridRegistry.MessageQueue.Daemon.Consumers.Lot;
using Lottery.Contracts.Events.Application;
using Lottery.Contracts.Events.Lot;
using MassTransit;
using MassTransit.RabbitMqTransport;
using System;
using System.Collections.Generic;

namespace GridRegistry.MessageQueue.Daemon.Consumers
{
    public static class EndpointConfigurator
    {
        /// <summary>
        /// Get endpoint configuratons
        /// </summary>
        /// <returns></returns>
        public static IList<Action<IRabbitMqBusFactoryConfigurator, IBusRegistrationContext>> GetEndpointConfiguratons()
        {
            return new List<Action<IRabbitMqBusFactoryConfigurator, IBusRegistrationContext>>
            {
                (cfg, ctx) => cfg.AddPublishEndpoint<LotCreatedEvent, LotCreatedConsumer>(ctx),
                (cfg, ctx) => cfg.AddPublishEndpoint<ApplicationCreatedEvent, ApplicationCreatedConsumer>(ctx)
            };
        }
    }
}
