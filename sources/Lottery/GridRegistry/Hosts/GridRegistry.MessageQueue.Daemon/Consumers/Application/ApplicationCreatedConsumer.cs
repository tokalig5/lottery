﻿using GridRegistry.ApiClient.GridRegistryApiClient;
using Lottery.Contracts.Events.Application;
using MassTransit;
using System.Threading.Tasks;

namespace GridRegistry.MessageQueue.Daemon.Consumers.Application
{
    /// <summary>
    /// Обработчик события о подаче заявки на участие в лотерее.
    /// </summary>
    public class ApplicationCreatedConsumer : IConsumer<ApplicationCreatedEvent>
    {
        private readonly IGridRegistryApiClient _apiClient;

        public ApplicationCreatedConsumer(IGridRegistryApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        public Task Consume(ConsumeContext<ApplicationCreatedEvent> context)
        {
            return _apiClient.CreateApplicationAsync(context.Message.Id, context.CancellationToken);
        }
    }
}
