﻿using GridRegistry.ApiClient.GridRegistryApiClient;
using Lottery.Contracts.Events.Lot;
using MassTransit;
using System.Threading.Tasks;

namespace GridRegistry.MessageQueue.Daemon.Consumers.Lot
{
    /// <summary>
    /// Обработчик события о создании лотереи.
    /// </summary>
    public class LotCreatedConsumer : IConsumer<LotCreatedEvent>
    {
        private readonly IGridRegistryApiClient _apiClient;

        public LotCreatedConsumer(IGridRegistryApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        public Task Consume(ConsumeContext<LotCreatedEvent> context)
        {
            return _apiClient.CreateLotAsync(context.Message.Id, context.CancellationToken);
        }
    }
}
