﻿using Lottery.AppServices.Application.Services;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.Handlers.Application.Commands
{
    /// <summary>
    /// Обработчик команды на подачу заявки на участие в лотерее.
    /// </summary>
    public class ApplicationCreateCommandHandler : IRequestHandler<ApplicationCreateCommand, Guid>
    {
        private readonly IApplicationService _applicationService;

        public ApplicationCreateCommandHandler(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        public Task<Guid> Handle(ApplicationCreateCommand request, CancellationToken cancellationToken)
        {
            return _applicationService.CreateAsync(request.Model, cancellationToken);
        }
    }
}
