﻿using Lottery.Contracts.Models;
using System;
using MediatR;

namespace Lottery.Handlers.Application.Commands
{
    /// <summary>
    /// Команда на подачу заявки на участие в лотерее.
    /// </summary>
    public class ApplicationCreateCommand : IRequest<Guid>
    {
        /// <summary>
        /// Модель создания заявки.
        /// </summary>
        public ApplicationCreateModel Model { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="ApplicationCreateCommand"/>.
        /// </summary>
        public ApplicationCreateCommand(ApplicationCreateModel model)
        {
            Model = model;
        }
    }
}
