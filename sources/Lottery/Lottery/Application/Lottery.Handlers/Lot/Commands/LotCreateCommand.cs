﻿using Lottery.Contracts.Models;
using System;
using MediatR;

namespace Lottery.Handlers.Lot.Commands
{
    /// <summary>
    /// Команда на создание лота
    /// </summary>
    public class LotCreateCommand : IRequest<Guid>
    {
        /// <summary>
        /// Модель создания лота
        /// </summary>
        public LotCreateModel Model { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="LotCreateCommand"/>.
        /// </summary>
        public LotCreateCommand(LotCreateModel model)
        {
            Model = model;
        }
    }
}
