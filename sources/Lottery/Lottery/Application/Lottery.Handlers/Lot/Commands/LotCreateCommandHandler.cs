﻿using Lottery.AppServices.Lot.Services;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.Handlers.Lot.Commands
{
    /// <summary>
    /// Обработчик события на создание лота
    /// </summary>
    public class LotCreateCommandHandler : IRequestHandler<LotCreateCommand, Guid>
    {
        private readonly ILotService _lotService;

        /// <summary>
        /// Инициализация экземпляра <see cref="LotCreateCommandHandler"/>.
        /// </summary>
        public LotCreateCommandHandler(ILotService lotService)
        {
            _lotService = lotService;
        }

        /// <inheritdoc/>
        public Task<Guid> Handle(LotCreateCommand request, CancellationToken cancellationToken)
        {
            return _lotService.CreateAsync(request.Model, cancellationToken);
        }
    }
}
