﻿using Common.Contracts.Bus.Enums;
using Common.Contracts.Enums;
using Common.Contracts.Models;
using Common.Infrastructure.Bus.Services.BusEventContainer;
using Lottery.Contracts.Events.Application;
using Lottery.Contracts.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.AppServices.Application.Services
{
    /// <inheritdoc cref="IApplicationService"/>
    public class ApplicationService : IApplicationService
    {
        private readonly IBusEventContainer _busEventContainer;

        public ApplicationService(IBusEventContainer busEventContainer)
        {
            _busEventContainer = busEventContainer;
        }

        /// <inheritdoc/>
        public Task<Guid> CreateAsync(ApplicationCreateModel model, CancellationToken cancellationToken)
        {
            _busEventContainer.Add(new ApplicationCreatedEvent(Guid.NewGuid()), MessageEventType.OnSuccess);
            return Task.FromResult(Guid.NewGuid());
        }
    }
}
