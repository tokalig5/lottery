﻿using Lottery.Contracts.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.AppServices.Application.Services
{
    /// <summary>
    /// Сервис по работе с заявками.
    /// </summary>
    public interface IApplicationService
    {
        /// <summary>
        /// Создание заявки на участие в лотерее.
        /// </summary>
        /// <param name="model">Модель заявки на участие в лотерее.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Идентификатор созданной сущности.</returns>
        Task<Guid> CreateAsync(ApplicationCreateModel model, CancellationToken cancellationToken);
    }
}
