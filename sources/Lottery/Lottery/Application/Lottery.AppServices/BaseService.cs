﻿using Common.Infrastructure.Utils.Extensions;
using Microsoft.AspNetCore.Http;
using System;

namespace Lottery.AppServices
{
    /// <summary>
    /// Базовый класс для сервиса лотереи.
    /// </summary>
    public abstract class BaseService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        protected BaseService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Получение идентификатора текущего пользователя, кто делает запрос.
        /// Если пользователь не залогинен, то результат будет равняться Guid.Empty.
        /// </summary>
        /// <returns>Идентификатор текущего пользователя.</returns>
        protected Guid GetCurrentUserId()
        {
            return _httpContextAccessor.HttpContext.GetUserId();
        }
    }
}
