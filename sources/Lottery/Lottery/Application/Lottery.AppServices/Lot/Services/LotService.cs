﻿using AutoMapper;
using Common.Contracts.Bus.Enums;
using Common.Infrastructure.Bus.Services.BusEventContainer;
using Lottery.AppServices.Lot.Repositories;
using Lottery.Contracts.Events.Lot;
using Lottery.Contracts.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.AppServices.Lot.Services
{
    /// <inheritdoc cref="ILotService"/>
    public class LotService : BaseService, ILotService
    {
        private readonly IBusEventContainer _busEventContainer;
        private readonly ILotRepository _lotRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Инициализация экземпляра <see cref="LotService"/>
        /// </summary>
        /// <param name="busEventContainer">Накопитель сообщений.</param>
        public LotService(ILotRepository lotRepository, IBusEventContainer busEventContainer, IMapper mapper, IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _lotRepository = lotRepository;
            _busEventContainer = busEventContainer;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task<Guid> CreateAsync(LotCreateModel model, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Entities.Lot>(model);
            entity.CreatedUserId = GetCurrentUserId();

            await _lotRepository.AddAsync(entity, cancellationToken);
            _busEventContainer.Add(new LotCreatedEvent(entity.Id), MessageEventType.OnSuccess);
            return entity.Id;
        }
    }
}
