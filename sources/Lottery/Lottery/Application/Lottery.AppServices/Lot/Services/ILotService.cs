﻿using Common.Contracts.Models;
using Lottery.Contracts.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.AppServices.Lot.Services
{
    /// <summary>
    /// Сервис по работе с лотереями.
    /// </summary>
    public interface ILotService
    {
        /// <summary>
        /// Создание лотереи.
        /// </summary>
        /// <param name="model">Модель лотереи.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Идентификатор созданной сущности.</returns>
        Task<Guid> CreateAsync(LotCreateModel model, CancellationToken cancellationToken);
    }
}
