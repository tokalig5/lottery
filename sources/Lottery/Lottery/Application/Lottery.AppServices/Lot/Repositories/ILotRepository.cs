﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.AppServices.Lot.Repositories
{
    /// <summary>
    /// Репозиторий для работы с сущностью лота.
    /// </summary>
    public interface ILotRepository
    {
        /// <summary>
        /// Добавление сущности лотереи.
        /// </summary>
        /// <param name="lot">Сущность лотереи.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        Task AddAsync(Entities.Lot lot, CancellationToken cancellationToken);

        /// <summary>
        /// Получение лотереи по ее идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор лотереи.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Сущность лотереи.</returns>
        Task<Entities.Lot> GetByIdAsync(Guid id, CancellationToken cancellationToken);
    }
}
