﻿using Common.Infrastructure.Utils.Extensions;
using Lottery.ApiClient.Exceptions;
using Lottery.Contracts.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.ApiClient.Clients
{
    public partial class LotteryApiClient
    {
        private const string LotApiPath = "lot";

        /// <inheritdoc/>
        public Task<Guid> CreateLotAsync(LotCreateModel model, CancellationToken cancellationToken)
        {
            return _httpClient.PostAsync<LotCreateModel, Guid>(GetLotApiUrl(), model, 
            (statusCode, message, exception) => throw new LotteryApiClientException(statusCode, $"Ошибка создания лотереи {model.Name}. Ошибка: {message}", exception),
            cancellationToken);
        }
    }
}
