﻿using Common.Infrastructure.Utils.Extensions;
using Lottery.ApiClient.Exceptions;
using Lottery.Contracts.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.ApiClient.Clients
{
    public partial class LotteryApiClient
    {
        private const string ApplicationApiPath = "application";

        /// <inheritdoc/>
        public Task CreateApplicationAsync(ApplicationCreateModel model, CancellationToken cancellationToken)
        {
            return _httpClient.PostAsync(GetApplicationApiUrl(), model, 
            (statusCode, message, exception) => throw new LotteryApiClientException(statusCode, $"Ошибка создания заявки на участие в лотерее {model.LotId}. {message}", exception),
            cancellationToken);
        }
    }
}
