﻿using Flurl;
using System.Net.Http;

namespace Lottery.ApiClient.Clients
{
    /// <inheritdoc cref="ILotteryApiClient"/>
    public partial class LotteryApiClient : ILotteryApiClient
    {
        private readonly HttpClient _httpClient;
        private const string ApiRoutePrefix = "api/v1";

        public LotteryApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        private static string GetLotApiUrl(params string[] parts)
        {
            return Url.Combine(ApiRoutePrefix, LotApiPath, Url.Combine(parts));
        }

        private static string GetApplicationApiUrl(params string[] parts)
        {
            return Url.Combine(ApiRoutePrefix, ApplicationApiPath, Url.Combine(parts));
        }
    }
}