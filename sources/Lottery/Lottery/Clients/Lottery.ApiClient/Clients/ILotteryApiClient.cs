﻿using Lottery.Contracts.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.ApiClient.Clients
{
    /// <summary>
    /// API клиент для работы с сервисом лотереи.
    /// </summary>
    public interface ILotteryApiClient
    {
        #region Lot

        /// <summary>
        /// Создание лотереи.
        /// </summary>
        /// <param name="model">Модель создания лотереи.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        Task<Guid> CreateLotAsync(LotCreateModel model, CancellationToken cancellationToken);

        #endregion

        #region Application

        /// <summary>
        /// Создание заявки на участие.
        /// </summary>
        /// <param name="model">Модель создания заявки на участие.</param>
        /// <param name="cancellationToken">Токен отмены.</param>        
        Task CreateApplicationAsync(ApplicationCreateModel model, CancellationToken cancellationToken);

        #endregion
    }
}