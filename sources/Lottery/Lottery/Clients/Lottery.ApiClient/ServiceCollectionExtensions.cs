﻿using Common.Contracts.Settings;
using Common.Infrastructure.Utils.Middlewares;
using Lottery.ApiClient.Clients;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net;

namespace Lottery.ApiClient
{
    /// <summary>
    /// Методы расширения АПИ клиента сервиса лотереи
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление FinanceApiClient
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="settings">Настройки Proxy</param>
        /// <returns>Коллекция сервисов</returns>
        public static IServiceCollection AddLotteryApiClient(this IServiceCollection services, ProxySettings settings)
        {
            services.AddHttpClient<ILotteryApiClient, LotteryApiClient>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(settings.LotteryServiceUrl);
                configureClient.DefaultRequestVersion = HttpVersion.Version20;
            }).AddHttpMessageHandler<HttpRequestHandler>();
            return services;
        }
    }
}
