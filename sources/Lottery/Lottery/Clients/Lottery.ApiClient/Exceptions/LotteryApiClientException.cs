﻿using System;
using System.Net;
using Common.Contracts.WebApi.Exceptions;

namespace Lottery.ApiClient.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе LotteryApiClient
    /// </summary>
    [Serializable]
    public class LotteryApiClientException : ApiClientException
    {
        /// <summary>
        /// Инициализация экземпляра <see cref="LotteryApiClientException"/>.
        /// </summary>
        /// <param name="statusCode">HTTP-код ошибки запроса.</param>
        /// <param name="message">Сообщение ошибки запроса.</param>
        /// <param name="innerException">Внутреннее исключение.</param>
        public LotteryApiClientException(HttpStatusCode statusCode, string message, Exception innerException) : base(statusCode, message, innerException)
        {
        }
    }
}
