﻿using Lottery.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Lottery.DbMigrator
{
    /// <summary>
    /// Контекст для миграций.
    /// </summary>
    public class MigrationDbContext : LotteryBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="MigrationDbContext"/>.
        /// </summary>
        public MigrationDbContext(DbContextOptions<MigrationDbContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
    }
}
