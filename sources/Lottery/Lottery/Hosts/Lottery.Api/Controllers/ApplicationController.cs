﻿using Lottery.Contracts.Models;
using Lottery.Handlers.Application.Commands;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Common.Contracts.WebApi.Models;

namespace Lottery.Api.Controllers
{
    /// <summary>
    /// Application controller
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ApplicationController : ControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Инициализация экземпляра <see cref="ApplicationController"/>.
        /// </summary>
        public ApplicationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Create Application
        /// </summary>
        /// <param name="applicationCreateModel">Model</param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([Required] ApplicationCreateModel applicationCreateModel, CancellationToken cancellationToken)
        {
            var command = new ApplicationCreateCommand(applicationCreateModel);
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
    }
}
