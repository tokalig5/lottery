﻿using Lottery.Contracts.Models;
using Lottery.Handlers.Lot.Commands;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.Api.Controllers
{
    /// <summary>
    /// Lot controller
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class LotController : ControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Инициализация экземпляра <see cref="LotController"/>.
        /// </summary>
        public LotController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Create lot
        /// </summary>
        /// <param name="lotCreateModel">Model</param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status201Created)]
        public async Task<IActionResult> Post([Required] LotCreateModel lotCreateModel, CancellationToken cancellationToken)
        {
            var command = new LotCreateCommand(lotCreateModel);
            var result = await _mediator.Send(command, cancellationToken);
            return StatusCode((int)HttpStatusCode.Created, result);
        }
    }
}