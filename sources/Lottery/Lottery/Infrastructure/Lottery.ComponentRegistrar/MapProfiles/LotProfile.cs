﻿using AutoMapper;
using Lottery.Contracts.Models;
using Lottery.Entities;
using System;

namespace Lottery.ComponentRegistrar.MapProfiles
{
    public class LotProfile : Profile
    {
        public LotProfile()
        {
            CreateMap<LotCreateModel, Lot>(MemberList.None)
                .ForMember(s => s.Created, map => map.MapFrom(s => DateTime.UtcNow));
        }
    }
}
