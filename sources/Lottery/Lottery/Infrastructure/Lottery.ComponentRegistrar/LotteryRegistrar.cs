﻿using AutoMapper;
using Common.Contracts.Settings;
using Common.Infrastructure;
using Lottery.AppServices.Application.Services;
using Lottery.AppServices.Lot.Repositories;
using Lottery.AppServices.Lot.Services;
using Lottery.ComponentRegistrar.MapProfiles;
using Lottery.DataAccess;
using Lottery.DataAccess.Repositories;
using Lottery.Handlers.Lot.Commands;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lottery.ComponentRegistrar
{
    public static class LotteryRegistrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                           .ConfigureAutoMapper()
                           .ConfigureBusService(configuration)
                           .ConfigureServices()
                           .ConfigureDbConnections(configuration)
                           .ConfigureRepositories()
                           .AddMediator()
                           .AddContextLogger(configuration);
        }

        private static IServiceCollection AddMediator(this IServiceCollection services)
        {
            services.AddMediator(typeof(LotCreateCommand).Assembly);
            return services;
        }

        private static IServiceCollection ConfigureBusService(this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = new RabbitSettings();
            configuration.Bind(nameof(RabbitSettings), rabbitSettings);

            services.AddBusService(rabbitSettings);
            return services;
        }

        private static IServiceCollection ConfigureDbConnections(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbConfiguration<LotteryContext, LotteryReadOnlyContext>(configuration);
            return services;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddTransient<IApplicationService, ApplicationService>();
            services.AddTransient<ILotService, LotService>();
            return services;
        }

        private static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            services.AddTransient<ILotRepository, LotRepository>();
            return services;
        }

        private static IServiceCollection ConfigureAutoMapper(this IServiceCollection services)
        {
            return services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<LotProfile>();
                cfg.AddProfile<ApplicationProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}