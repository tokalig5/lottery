﻿using Lottery.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Lottery.DataAccess.Configurations
{
    /// <summary>
    /// Настройки сущности заявки на участие в лотерее.
    /// </summary>
    public class ApplicationConfiguration : IEntityTypeConfiguration<Application>
    {
        /// <inheritdoc/>
        public void Configure(EntityTypeBuilder<Application> builder)
        {
            builder.HasKey(s => s.Id);
            builder.HasIndex(s => s.CreatedUserId);

            builder.Property(s => s.Id).ValueGeneratedOnAdd();
            builder.Property(s => s.Created).HasConversion(s => s, s => DateTime.SpecifyKind(s, DateTimeKind.Utc));
            builder.Property(s => s.Published).HasConversion(s => s, s => s.HasValue ? DateTime.SpecifyKind(s.Value, DateTimeKind.Utc) : null);
        }
    }
}
