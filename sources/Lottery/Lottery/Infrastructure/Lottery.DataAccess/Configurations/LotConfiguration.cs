﻿using Lottery.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Lottery.DataAccess.Configurations
{
    /// <summary>
    /// Настройки сущности лотереи.
    /// </summary>
    public class LotConfiguration : IEntityTypeConfiguration<Lot>
    {
        /// <inheritdoc/>
        public void Configure(EntityTypeBuilder<Lot> builder)
        {
            builder.HasKey(s => s.Id);
            builder.HasIndex(s => s.CreatedUserId);

            builder.Property(s => s.Name).HasMaxLength(256).IsRequired();
            builder.Property(s => s.Created).HasConversion(s => s, s => DateTime.SpecifyKind(s, DateTimeKind.Utc));
            builder.Property(s => s.Published).HasConversion(s => s, s => s.HasValue ? DateTime.SpecifyKind(s.Value, DateTimeKind.Utc) : null);

            builder.HasMany(s => s.Applications).WithOne(s => s.Lot).HasForeignKey(s => s.LotId).IsRequired().OnDelete(DeleteBehavior.Cascade);
        }
    }
}
