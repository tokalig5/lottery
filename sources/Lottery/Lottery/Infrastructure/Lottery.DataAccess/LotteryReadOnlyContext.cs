﻿using Microsoft.EntityFrameworkCore;

namespace Lottery.DataAccess
{
    /// <summary>
    /// Контекст чтения-записи БД Lottery.
    /// </summary>
    public class LotteryReadOnlyContext : LotteryBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="LotteryReadOnlyContext"/>
        /// </summary>
        /// <param name="options">Настройки контекста.</param>
        public LotteryReadOnlyContext(DbContextOptions<LotteryReadOnlyContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
    }
}
