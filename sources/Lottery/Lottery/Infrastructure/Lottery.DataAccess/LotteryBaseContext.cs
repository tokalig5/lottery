﻿using Lottery.DataAccess.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Lottery.DataAccess
{
    /// <summary>
    /// Базовый контекст для работы с БД.
    /// </summary>
    public class LotteryBaseContext : DbContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="LotteryBaseContext"/>
        /// </summary>
        /// <param name="options"></param>
        public LotteryBaseContext(DbContextOptions options) : base(options)
        {
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new LotConfiguration());
            modelBuilder.ApplyConfiguration(new ApplicationConfiguration());
        }
    }
}
