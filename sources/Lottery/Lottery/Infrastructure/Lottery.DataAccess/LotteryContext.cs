﻿using Microsoft.EntityFrameworkCore;

namespace Lottery.DataAccess
{
    /// <summary>
    /// Контекст чтения-записи БД Lottery.
    /// </summary>
    public class LotteryContext : LotteryBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="LotteryContext"/>
        /// </summary>
        /// <param name="options">Настройки контекста.</param>
        public LotteryContext(DbContextOptions<LotteryContext> options) : base(options)
        {
        }
    }
}
