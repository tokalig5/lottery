﻿using Common.Infrastructure.DataAccess.Repositories;
using Lottery.AppServices.Lot.Repositories;
using Lottery.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lottery.DataAccess.Repositories
{
    /// <inheritdoc cref="ILotRepository"/>
    public class LotRepository : ILotRepository
    {
        private readonly IRepository<Lot, LotteryContext> _repository;
        private readonly IReadOnlyRepository<Lot, LotteryReadOnlyContext> _readOnlyRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="LotRepository"/>.
        /// </summary>
        /// <param name="repository">Репозиторий чтения-записи.</param>
        /// <param name="readOnlyRepository">Репозиторий чтения.</param>
        public LotRepository(IRepository<Lot, LotteryContext> repository, IReadOnlyRepository<Lot, LotteryReadOnlyContext> readOnlyRepository)
        {
            _repository = repository;
            _readOnlyRepository = readOnlyRepository;
        }

        /// <inheritdoc/>
        public Task AddAsync(Lot lot, CancellationToken cancellationToken)
        {
            return _repository.AddAsync(lot, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<Lot> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return _readOnlyRepository.GetByIdAsync(id, cancellationToken);
        }
    }
}
