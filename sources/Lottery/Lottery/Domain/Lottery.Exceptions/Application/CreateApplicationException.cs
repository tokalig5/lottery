﻿using System;
using System.Runtime.Serialization;

namespace Lottery.Exceptions.Application
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе с заявками на участие в лотерее.
    /// </summary>
    [Serializable]
    public class CreateApplicationException : Exception
    {
        private const string DefaultMessage = "Ошибка подачи заявки на участие в лотерее {0} пользователем {1}.";

        public CreateApplicationException(Guid lotId, string userId) : base(string.Format(DefaultMessage, lotId, userId))
        {
        }

        public CreateApplicationException(Guid lotId, string userId, Exception innerException) : base(string.Format(DefaultMessage, lotId, userId), innerException)
        {
        }

        protected CreateApplicationException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
        }
    }
}