﻿using Common.Contracts.DataAccess;
using Lottery.Contracts.Contracts;
using System;

namespace Lottery.Entities
{
    /// <summary>
    /// Заявки на участие в лотерее.
    /// </summary>
    public class Application : BaseEntity
    {
        /// <summary>
        /// Идентификатор создавшего пользователя.
        /// </summary>
        public Guid CreatedUserId { get; set; }

        /// <summary>
        /// Время создания.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Время публикации.
        /// </summary>
        public DateTime? Published { get; set; }

        /// <summary>
        /// Состояние.
        /// </summary>
        public ApplicationState State { get; set; }

        /// <summary>
        /// Итоговое место по результатам лотереи.
        /// </summary>
        public int? Place { get; set; }

        /// <summary>
        /// Идентификатор лотереи.
        /// </summary>
        public Guid LotId { get; set; }

        /// <summary>
        /// Лотерея.
        /// </summary>
        public virtual Lot Lot { get; set; }
    }
}
