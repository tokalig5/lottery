﻿using Common.Contracts.DataAccess;
using Lottery.Contracts.Contracts;
using System;
using System.Collections.Generic;

namespace Lottery.Entities
{
    /// <summary>
    /// Лотерея.
    /// </summary>
    public class Lot : BaseEntity
    {
        /// <summary>
        /// Наименование лотереи.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Идентификатор создавшего пользователя.
        /// </summary>
        public Guid CreatedUserId { get; set; }

        /// <summary>
        /// Время создания.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Время публикации.
        /// </summary>
        public DateTime? Published { get; set; }

        /// <summary>
        /// Состояние.
        /// </summary>
        public LotState State { get; set; }

        /// <summary>
        /// Заявки на участие в лотерее.
        /// </summary>
        public virtual IList<Application> Applications { get; set; }
    }
}