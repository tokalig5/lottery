﻿using System.ComponentModel;

namespace Lottery.Contracts.Contracts
{
    /// <summary>
    /// Состояния заявки.
    /// </summary>
    public enum ApplicationState
    {
        /// <summary>
        /// Черновик.
        /// </summary>
        [Description("Черновик")]
        Draft = 0,

        /// <summary>
        /// Рассмотрение оператором.
        /// </summary>
        [Description("Рассмотрение оператором")]
        OperatorReview = 1,

        /// <summary>
        /// Возвращена оператором.
        /// </summary>
        [Description("Возвращена оператором")]
        ReturnedByOperator = 2,

        /// <summary>
        /// Подана.
        /// </summary>
        [Description("Подана")]
        Published = 3,

        /// <summary>
        /// Розыгрыш.
        /// </summary>
        [Description("Розыгрыш")]
        Lottery = 4,

        /// <summary>
        /// Ожидает результата.
        /// </summary>
        [Description("Ожидает результата")]
        WaitingForResult = 5,

        /// <summary>
        /// Завершена.
        /// </summary>
        [Description("Завершена")]
        Completed = 6,

        /// <summary>
        /// Отозвана.
        /// </summary>
        [Description("Отозвана")]
        Recalled = 7,

        /// <summary>
        /// Отменена.
        /// </summary>
        [Description("Отменена")]
        Cancelled = 8
    }
}
