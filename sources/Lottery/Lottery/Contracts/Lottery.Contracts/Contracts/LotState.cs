﻿using System.ComponentModel;

namespace Lottery.Contracts.Contracts
{
    /// <summary>
    /// Состояние лотереи.
    /// </summary>
    public enum LotState
    {
        /// <summary>
        /// Черновик.
        /// </summary>
        [Description("Черновик")]
        Draft = 0,

        /// <summary>
        /// Ожидание публикации.
        /// </summary>
        [Description("Ожидание публикации")]
        Created = 1,

        /// <summary>
        /// Опубликован.
        /// </summary>
        [Description("Опубликован")]
        Published = 2,

        /// <summary>
        /// Подача заявок.
        /// </summary>
        [Description("Подача заявок")]
        FillingApplications = 3,

        /// <summary>
        /// Ожидание розыгрыша.
        /// </summary>
        [Description("Ожидание розыгрыша")]
        LotteryWaiting = 4,

        /// <summary>
        /// Розыгрыш.
        /// </summary>
        [Description("Розыгрыш")]
        Lottery = 5,

        /// <summary>
        /// Подведение итогов.
        /// </summary>
        [Description("Подведение итогов")]
        Summarizing = 6,

        /// <summary>
        /// Завершен.
        /// </summary>
        [Description("Завершен")]
        Completed = 7,

        /// <summary>
        /// Отменен.
        /// </summary>
        [Description("Отменен")]
        Cancelled = 8
    }
}
