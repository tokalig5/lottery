﻿using Common.Contracts.Bus.Abstraction;
using System;

namespace Lottery.Contracts.Events.Application
{
    /// <summary>
    /// Событие о подаче заявки на участие в лотерее.
    /// </summary>
    public sealed class ApplicationCreatedEvent : IMessageQueueEvent
    {
        /// <summary>
        /// Идентификатор заявки на участие.
        /// </summary>
        public Guid Id { get; private set; }

        public ApplicationCreatedEvent(Guid id)
        {
            Id = id;
        }
    }
}
