﻿using Common.Contracts.Bus.Abstraction;
using System;

namespace Lottery.Contracts.Events.Lot
{
    /// <summary>
    /// Событие о создании лотереи.
    /// </summary>
    public sealed class LotCreatedEvent : IMessageQueueEvent
    {
        /// <summary>
        /// Идентификатор лотереи.
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// Инициализация экземпляра <see cref="LotCreatedEvent"/>.
        /// </summary>
        /// <param name="id">Идентификатор созданной лотереи.</param>
        public LotCreatedEvent(Guid id)
        {
            Id = id;
        }
    }
}
