﻿using System.ComponentModel.DataAnnotations;

namespace Lottery.Contracts.Models
{
    /// <summary>
    /// Lot create model
    /// </summary>
    public sealed class LotCreateModel
    {
        /// <summary>
        /// Наименование лотереи.
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
