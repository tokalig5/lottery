﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lottery.Contracts.Models
{
    /// <summary>
    /// Модель создания заявки на участие в лотерее.
    /// </summary>
    public sealed class ApplicationCreateModel
    {
        /// <summary>
        /// Идентификатор лотереи.
        /// </summary>
        [Required]
        public Guid LotId { get; set; }
    }
}
