﻿using Common.Contracts.Settings;
using Finance.ApiClient.Clients;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Finance.ApiClient
{
    /// <summary>
    /// Методы расширения АПИ клиента финансового сервиса
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление FinanceApiClient
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="settings">Настройки Proxy</param>
        /// <returns>Коллекция сервисов</returns>
        public static IServiceCollection AddFinanceApiClient(this IServiceCollection services, ProxySettings settings)
        {
            services.AddHttpClient<IFinanceApiClient, FinanceApiClient>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(settings.FinanceServiceUrl);
            });
            return services;
        }
    }
}
