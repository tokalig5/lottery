﻿using System.Net.Http;

namespace Finance.ApiClient.Clients
{
    /// <summary>
    /// Finance service API Client
    /// </summary>
    public class FinanceApiClient : IFinanceApiClient
    {
        private readonly HttpClient _httpClient;

        public FinanceApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
    }
}