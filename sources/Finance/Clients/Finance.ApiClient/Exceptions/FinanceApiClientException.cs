﻿using System;
using System.Runtime.Serialization;

namespace Finance.ApiClient.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе FinanceApiClient
    /// </summary>
    [Serializable]
    public class FinanceApiClientException : Exception
    {
        private const string DefaultMessage = "Ошибка работы с FinanceApiClient: ";

        public FinanceApiClientException(string message) : base(string.Concat(DefaultMessage, message))
        {
        }

        public FinanceApiClientException(string message, Exception innerException) : base(string.Concat(DefaultMessage, message), innerException)
        {
        }

        protected FinanceApiClientException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
        }
    }
}
