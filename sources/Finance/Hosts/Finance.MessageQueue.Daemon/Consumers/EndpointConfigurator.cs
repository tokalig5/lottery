﻿using Common.Infrastructure.Utils.Extensions;
using Finance.Contracts;
using MassTransit;
using MassTransit.RabbitMqTransport;
using System;
using System.Collections.Generic;

namespace Finance.MessageQueue.Daemon.Consumers
{
    /// <summary>
    /// Message queue endpoint configurator
    /// </summary>
    public static class EndpointConfigurator
    {
        /// <summary>
        /// Get endpoint configuratons
        /// </summary>
        /// <returns></returns>
        public static IList<Action<IRabbitMqBusFactoryConfigurator, IBusRegistrationContext>> GetEndpointConfiguratons()
        {
            return new List<Action<IRabbitMqBusFactoryConfigurator, IBusRegistrationContext>>
            {
                (cfg, ctx) => cfg.AddPublishEndpoint<TestEvent, TestEventConsumer>(ctx)
            };
        }
    }
}