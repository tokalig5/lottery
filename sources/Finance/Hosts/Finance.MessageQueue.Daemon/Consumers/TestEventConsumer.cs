﻿using System;
using System.IO;
using System.Threading.Tasks;
using Finance.Contracts;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Finance.MessageQueue.Daemon.Consumers
{
    public class TestEventConsumer : IConsumer<TestEvent>
    {
        private readonly ILogger<TestEventConsumer> _logger;

        public TestEventConsumer(ILogger<TestEventConsumer> logger)
        {
            _logger = logger;
        }

        public Task Consume(ConsumeContext<TestEvent> context)
        {
            _logger.LogInformation($"Value: {context.Message.TestData}");
            return Task.CompletedTask;
        }
    }
}