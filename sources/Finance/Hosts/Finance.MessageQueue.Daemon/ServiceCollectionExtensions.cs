﻿using Common.Contracts.Settings;
using Common.Infrastructure;
using Finance.MessageQueue.Daemon.Consumers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Finance.MessageQueue.Daemon
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureBusService(configuration);
            return services;
        }

        private static IServiceCollection ConfigureBusService(this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = new RabbitSettings();
            configuration.Bind(nameof(RabbitSettings), rabbitSettings);

            services.AddBusService(rabbitSettings, EndpointConfigurator.GetEndpointConfiguratons());
            return services;
        }
    }
}