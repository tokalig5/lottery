﻿using Common.Contracts.Bus.Abstraction;

namespace Finance.Contracts
{
    public class TestEvent : IMessageQueueEvent
    {
        public string TestData { get; set; }
    }
}
