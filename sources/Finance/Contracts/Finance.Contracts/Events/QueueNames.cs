﻿namespace Finance.Contracts.Events
{
    /// <summary>
    /// Queue names for events
    /// </summary>
    public static class QueueNames
    {
        /// <summary>
        /// Test event queue
        /// </summary>
        public static string FinanceTestEventQueue => nameof(FinanceTestEventQueue);
    }
}