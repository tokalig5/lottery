﻿namespace Authorization.Contracts.Settings
{
    /// <summary>
    /// Настройки для конфигурации Identity server 4.
    /// </summary>
    public class IdentityServerSettings
    {
        /// <summary>
        /// Параметр необходимости использования тестовых данных для подиписи.
        /// </summary>
        public bool UseDevelopmentCredentials { get; set; }

        /// <summary>
        /// Данные о сертификате подписи.
        /// </summary>
        public Certificate Certificate { get; set; }
    }

    /// <summary>
    /// Данные о сертификате для подписи.
    /// </summary>
    public class Certificate
    {
        /// <summary>
        /// Контент сертификата в Base64.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Пароль для сертификата.
        /// </summary>
        public string Password { get; set; }
    }
}