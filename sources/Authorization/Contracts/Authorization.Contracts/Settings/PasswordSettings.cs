﻿namespace Authorization.Contracts.Settings
{
    /// <summary>
    /// Настройки для работы с паролями пользователей.
    /// </summary>
    public class PasswordSettings
    {
        /// <summary>
        /// Секрет сервера, который используется как составная часть алгоритма хэширования пароля.
        /// </summary>
        public string Secret { get; set; }
    }
}
