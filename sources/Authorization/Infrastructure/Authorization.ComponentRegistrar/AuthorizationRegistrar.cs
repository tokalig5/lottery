﻿using Authorization.AppServices.IdentityServer.Services;
using Authorization.AppServices.Password.Services;
using Authorization.AppServices.User.Services;
using Authorization.Contracts.Settings;
using Common.Contracts.Settings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Cryptography.X509Certificates;
using User.ApiClient;

namespace Authorization.ComponentRegistrar
{
    public static class AuthorizationRegistrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                           .ConfigureIdentityServer(configuration)
                           .ConfigureServices(configuration)
                           .AddApiClients(configuration);
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var passwordSettings = new PasswordSettings();
            configuration.Bind(nameof(PasswordSettings), passwordSettings);
            services.AddSingleton(passwordSettings);

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IPasswordService, PasswordService>();
            return services;
        }

        private static IServiceCollection ConfigureIdentityServer(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("ConnectionString");

            services.AddIdentityServer(o =>
            {
                o.Discovery.ShowClaims = false;
                o.Discovery.ShowGrantTypes = false;
                o.Discovery.ShowIdentityScopes = false;
            })
            .AddSigningCredentials(configuration)
            .AddProfileService<ProfileService>()
            .AddConfigurationStore(options => { options.ConfigureDbContext = b => b.UseNpgsql(connectionString); })
            .AddOperationalStore(options => { options.ConfigureDbContext = b => b.UseNpgsql(connectionString); });

            return services;
        }

        private static IServiceCollection AddApiClients(this IServiceCollection services, IConfiguration configuration)
        {
            var proxySettings = new ProxySettings();
            configuration.Bind(nameof(ProxySettings), proxySettings);

            services.AddUserApiClient(proxySettings);
            return services;
        }

        private static IIdentityServerBuilder AddSigningCredentials(this IIdentityServerBuilder builder, IConfiguration configuration)
        {
            var settings = new IdentityServerSettings();
            configuration.Bind(nameof(IdentityServerSettings), settings);

            if (settings.UseDevelopmentCredentials)
            {
                builder.AddDeveloperSigningCredential();
            }
            else
            {
                var content = Convert.FromBase64String(settings.Certificate.Content);
                var password = settings.Certificate.Password;
                var certificate = new X509Certificate2(content, password);

                builder.AddSigningCredential(certificate);
            }

            return builder;
        }
    }
}