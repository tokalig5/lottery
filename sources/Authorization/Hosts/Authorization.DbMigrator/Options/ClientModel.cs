﻿namespace Authorization.DbMigrator.Options
{
    /// <summary>
    /// Модель информации о клиенте.
    /// </summary>
    public class ClientModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Секрет.
        /// </summary>
        public string Secret { get; set; }

        /// <summary>
        /// Базовый адрес клиента.
        /// </summary>
        public string BaseUrl { get; set; }
    }
}
