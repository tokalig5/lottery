﻿namespace Authorization.DbMigrator.Options
{
    /// <summary>
    /// Модель информации об апи ресурсе.
    /// </summary>
    public class ApiResourceModel
    {
        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Секрет.
        /// </summary>
        public string Secret { get; set; }
    }
}
