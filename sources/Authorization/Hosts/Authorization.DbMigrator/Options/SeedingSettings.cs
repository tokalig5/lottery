﻿namespace Authorization.DbMigrator.Options
{
    /// <summary>
    /// Модель информации о сидинге данных.
    /// </summary>
    public class SeedingSettings
    {
        /// <summary>
        /// Клиенты.
        /// </summary>
        public List<ClientModel> Clients { get; set; } = new();

        /// <summary>
        /// Апи ресурсы.
        /// </summary>
        public List<ApiResourceModel> ApiResources { get; set; } = new();
    }
}
