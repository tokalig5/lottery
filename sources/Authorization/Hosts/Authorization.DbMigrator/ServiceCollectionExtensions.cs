﻿using Authorization.DbMigrator.Options;
using Authorization.DbMigrator.Services;
using Microsoft.EntityFrameworkCore;

namespace Authorization.DbMigrator
{
    /// <summary>
    /// Методы расширения для добавления сервисов.
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление сервисов.
        /// </summary>
        /// <param name="services">Сервисы.</param>
        /// <param name="configuration">Конфигурация.</param>
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureServices(configuration);
            services.ConfigureDbConnections(configuration);
            return services;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var seedingSettings = new SeedingSettings();
            configuration.Bind("Seeding", seedingSettings);
            services.AddSingleton(seedingSettings);

            services.AddTransient<ISeeder, Seeder>();
            return services;
        }

        private static IServiceCollection ConfigureDbConnections(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("ConnectionString");
            var assemblyName = typeof(Program).Assembly.GetName().Name;

            services.AddIdentityServer().AddConfigurationStore(options =>
            {
                options.ConfigureDbContext = b => b.UseNpgsql(connectionString, b => b.MigrationsAssembly(assemblyName));
            }).AddOperationalStore(options =>
            {
                options.ConfigureDbContext = b => b.UseNpgsql(connectionString, b => b.MigrationsAssembly(assemblyName));
            });

            return services;
        }
    }
}
