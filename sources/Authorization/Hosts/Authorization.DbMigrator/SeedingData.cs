﻿using IdentityServer4;
using IdentityServer4.Models;

namespace Authorization.DbMigrator
{
    /// <summary>
    /// Данные для сидинга.
    /// </summary>
    public static class SeedingData
    {
        public static IEnumerable<IdentityResource> IdentityResources => new List<IdentityResource>
        {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile(),
        };

        public static IEnumerable<ApiScope> ApiScopes => new List<ApiScope>
        {
            new ApiScope(ApiScopeNames.LotteryApi, "Lottery API")
        };
    }

    /// <summary>
    /// Наименования АПИ скоупов.
    /// </summary>
    public static class ApiScopeNames
    {
        /// <summary>
        /// АПИ лотереи (Gateway).
        /// </summary>
        public const string LotteryApi = "LotteryApi";
    }

    /// <summary>
    /// Идентификаторы клиентов.
    /// </summary>
    public static class ClientIds
    {
        /// <summary>
        /// WebSPA лотереи (UI приложение).
        /// </summary>
        public const string LotteryWebSPA = "lp_web_spa";
    }
    
    /// <summary>
    /// Наименования апи ресурсов.
    /// </summary>
    public static class ApiResourceNames
    {
        /// <summary>
        /// АПИ лотереи (Gateway).
        /// </summary>
        public const string LotteryWebApi = "lp_gateway_api";
    }
}
