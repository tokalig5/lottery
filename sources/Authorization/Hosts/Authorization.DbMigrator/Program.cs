using Authorization.DbMigrator.Services;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace Authorization.DbMigrator
{
    public class Program
    {
        public static async Task<int> Main(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args).ConfigureServices((hostContext, services) =>
            {
                services.AddServices(hostContext.Configuration);
            }).Build();
            await MigrateDataBaseAsync(host.Services);
            return 0;
        }

        private static async Task MigrateDataBaseAsync(IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.CreateScope();
            var configurationDbContext = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
            await configurationDbContext!.Database.MigrateAsync();

            var persistedGrantDbContext = scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>();
            await persistedGrantDbContext!.Database.MigrateAsync();

            var seeder = scope.ServiceProvider.GetRequiredService<ISeeder>();
            await seeder.SeedDataAsync();
        }
    }
}