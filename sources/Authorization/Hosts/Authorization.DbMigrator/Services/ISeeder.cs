﻿namespace Authorization.DbMigrator.Services
{
    /// <summary>
    /// Сервис заполнения данными.
    /// </summary>
    public interface ISeeder
    {
        /// <summary>
        /// Заполнение данных.
        /// </summary>
        Task SeedDataAsync();
    }
}
