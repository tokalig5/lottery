﻿using Authorization.DbMigrator.Options;
using Common.Contracts.WebApi.Exceptions;
using Flurl;
using IdentityServer4;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Models;
using Microsoft.EntityFrameworkCore;

namespace Authorization.DbMigrator.Services
{
    /// <inheritdoc cref="ISeeder"/>
    public class Seeder : ISeeder
    {
        private readonly SeedingSettings _seedingSettings;
        private readonly ConfigurationDbContext _context;

        /// <summary>
        /// Инициализация экземпляра <see cref="Seeder"/>.
        /// </summary>
        public Seeder(SeedingSettings seedingSettings, ConfigurationDbContext context)
        {
            _seedingSettings = seedingSettings;
            _context = context;
        }

        /// <inheritdoc/>
        public async Task SeedDataAsync()
        {
            await SeedClientsAsync();
            await SeedApiResourcesAsync();
            await SeedIdentityResourcesAsync();
            await SeedApiScopesAsync();
        }

        private async Task SeedClientsAsync()
        {
            if (_seedingSettings.Clients.Any())
            {
                var existsClientIds = await _context.Clients.Select(s => s.ClientId).ToListAsync();
                var seedingClients = GetClients();

                foreach (var clientData in _seedingSettings.Clients)
                {
                    if (existsClientIds.Contains(clientData.Id))
                    {
                        continue;
                    }

                    var client = seedingClients.FirstOrDefault(s => s.ClientId == clientData.Id);
                    if (client != null)
                    {
                        //client.ClientSecrets = new[] { new Secret(clientData.Secret.Sha256()) }; //Перенести в GetClients
                        await _context.Clients.AddAsync(client.ToEntity());
                    }
                }

                await _context.SaveChangesAsync();
            }
        }

        private async Task SeedApiResourcesAsync()
        {
            if (_seedingSettings.ApiResources.Any())
            {
                var existsApiResourceNames = await _context.ApiResources.Select(s => s.Name).ToListAsync();
                var seedingApiResources = GetApiResources();

                foreach (var apiResourceData in _seedingSettings.ApiResources)
                {
                    if (existsApiResourceNames.Contains(apiResourceData.Name))
                    {
                        continue;
                    }

                    var apiResource = seedingApiResources.FirstOrDefault(s => s.Name == apiResourceData.Name);
                    if (apiResource != null)
                    {
                        //apiResource.ApiSecrets = new[] { new Secret(apiResourceData.Secret.Sha256()) }; //Перенести в GetapiResources
                        await _context.ApiResources.AddAsync(apiResource.ToEntity());
                    }
                }

                await _context.SaveChangesAsync();
            }
        }

        private async Task SeedIdentityResourcesAsync()
        {
            if (!await _context.IdentityResources.AnyAsync())
            {
                await _context.IdentityResources.AddRangeAsync(SeedingData.IdentityResources.Select(s => s.ToEntity()));
                await _context.SaveChangesAsync();
            }
        }
        private async Task SeedApiScopesAsync()
        {
            if (!await _context.ApiScopes.AnyAsync())
            {
                await _context.ApiScopes.AddRangeAsync(SeedingData.ApiScopes.Select(s => s.ToEntity()));
                await _context.SaveChangesAsync();
            }
        }

        private IEnumerable<Client> GetClients()
        {
            var clients = new List<Client>
            {
                new Client
                {
                    ClientId = ClientIds.LotteryWebSPA,
                    ClientName = "Lottery WebSPA",
                    ClientSecrets =
                    {
                        new Secret(GetClientSecret(ClientIds.LotteryWebSPA).Sha256())
                    },
                    Description = "Lottery WebSPA",
                    RequireClientSecret = true,
                    AllowedGrantTypes = GrantTypes.Code,
                    AllowedCorsOrigins =
                    {
                        Url.Combine(GetClientBaseUrl(ClientIds.LotteryWebSPA))
                    },
                    RedirectUris =
                    {
                        Url.Combine(GetClientBaseUrl(ClientIds.LotteryWebSPA), "signin-oidc"),
                        Url.Combine(GetClientBaseUrl(ClientIds.LotteryWebSPA), "callback.html"),
                        Url.Combine(GetClientBaseUrl(ClientIds.LotteryWebSPA), "silent.html")
                    },
                    PostLogoutRedirectUris =
                    {
                        Url.Combine(GetClientBaseUrl(ClientIds.LotteryWebSPA), "signout-callback-oidc")
                    },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        ApiScopeNames.LotteryApi
                    }
                }
            };

            return clients;
        }

        private IEnumerable<ApiResource> GetApiResources()
        {
            var apiResources = new List<ApiResource>
            {
                new ApiResource
                {
                    Name = ApiResourceNames.LotteryWebApi,
                    ApiSecrets =
                    {
                        new Secret(GetApiResourceSecret(ApiResourceNames.LotteryWebApi).Sha256())
                    },
                    DisplayName = "Lottery API Gateway",
                    Description = "Lottery API Gateway"
                }
            };

            return apiResources;
        }

        private string GetClientSecret(string clientId)
        {
            var client = GetClientInfo(clientId);
            return client.Secret;
        }

        private string GetClientBaseUrl(string clientId)
        {
            var client = GetClientInfo(clientId);
            return client.BaseUrl;
        }

        private ClientModel GetClientInfo(string clientId)
        {
            var client = _seedingSettings.Clients.FirstOrDefault(s => s.Id == clientId);
            return client ?? throw new NotFoundException($"Не обнаружена конфигурация клиента с идентификатором {clientId}");
        }

        private string GetApiResourceSecret(string apiResourceName)
        {
            var client = GetApiResourceInfo(apiResourceName);
            return client.Secret;
        }

        private ApiResourceModel GetApiResourceInfo(string apiResourceName)
        {
            var apiResource = _seedingSettings.ApiResources.FirstOrDefault(s => s.Name == apiResourceName);
            return apiResource ?? throw new NotFoundException($"Не обнаружена конфигурация апи ресурса с наименованием {apiResourceName}");
        }
    }
}
