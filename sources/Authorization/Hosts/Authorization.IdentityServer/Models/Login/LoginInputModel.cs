﻿using System.ComponentModel.DataAnnotations;

namespace Authorization.IdentityServer.Models.Login
{
    /// <summary>
    /// Модель для логина пользователя.
    /// </summary>
    public class LoginInputModel
    {
        /// <summary>
        /// Логин.
        /// </summary>
        [Required]
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Запоминать ли вход.
        /// </summary>
        public bool RememberLogin { get; set; }

        /// <summary>
        /// Return Url.
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}
