﻿using System.ComponentModel.DataAnnotations;

namespace Authorization.IdentityServer.Models.Register
{
    /// <summary>
    /// Модель для регистрации пользователя.
    /// </summary>
    public class RegisterInputModel
    {
        /// <summary>
        /// Имя.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 6)]
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// Подтверждённый пароль.
        /// </summary>
        [Required]        
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Емейл.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Телефон.
        /// </summary>
        [Required]
        [Phone]
        public string Phone { get; set; }

        /// <summary>
        /// Return Url.
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}
