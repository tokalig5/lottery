﻿namespace Authorization.IdentityServer.Models.ResetPassword
{
    /// <summary>
    /// Модель представления для смены пароля.
    /// </summary>
    public class ResetPasswordViewModel : ResetPasswordInputModel
    {
    }
}
