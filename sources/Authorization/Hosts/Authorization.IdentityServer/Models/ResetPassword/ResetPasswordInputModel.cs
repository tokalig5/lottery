﻿using System.ComponentModel.DataAnnotations;

namespace Authorization.IdentityServer.Models.ResetPassword
{
    /// <summary>
    /// Модель для сброса пароля пользователя.
    /// </summary>
    public class ResetPasswordInputModel
    {
        /// <summary>
        /// Пароль.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// Подтверждённый пароль.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Return Url.
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}
