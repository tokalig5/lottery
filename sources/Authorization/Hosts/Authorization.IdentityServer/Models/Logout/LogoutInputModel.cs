﻿namespace Authorization.IdentityServer.Models.Logout
{
    /// <summary>
    /// Модель для разлогина пользователя.
    /// </summary>
    public class LogoutInputModel
    {
        /// <summary>
        /// Идентификатор контекста разлогина.
        /// </summary>
        public string LogoutId { get; set; }
    }
}
