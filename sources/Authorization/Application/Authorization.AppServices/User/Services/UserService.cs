﻿using Authorization.AppServices.Password.Services;
using Authorization.AppServices.User.Models;
using Common.Contracts.WebApi.Exceptions;
using System.Security.Cryptography;
using User.ApiClient.Clients;
using User.Contracts.Requests;
using User.Contracts.Responses;

namespace Authorization.AppServices.User.Services
{
    /// <inheritdoc cref="IUserService"/>
    public class UserService : IUserService
    {
        private readonly IUserApiClient _userApiClient;
        private readonly IPasswordService _passwordService;

        /// <summary>
        /// Инициализация экземпляра <see cref="UserService"/>.
        /// </summary>
        public UserService(IUserApiClient userApiClient, IPasswordService passwordService)
        {
            _userApiClient = userApiClient;
            _passwordService = passwordService;
        }

        /// <inheritdoc/>
        public Task<UserInfo> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return _userApiClient.GetByIdAsync(id, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<UserInfo> GetByLoginAsync(string login, CancellationToken cancellationToken)
        {
            return _userApiClient.GetByLoginAsync(login, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<Guid> CreateAsync(RegisterUserModel model, CancellationToken cancellationToken)
        {
            var request = GetCreateUserRequest(model);
            return _userApiClient.CreateAsync(request, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<bool> SetNewPasswordAsync(Guid userId, string oldPassword, string newPassword, CancellationToken cancellationToken)
        {
            var user = await GetByIdAsync(userId, cancellationToken);
            if (user == null)
            {
                throw new NotFoundException($"Пользователь {userId} не найден в системе при попытке смены пароля");
            }

            if (string.IsNullOrWhiteSpace(oldPassword) || string.IsNullOrWhiteSpace(newPassword))
            {
                throw new BusinessLogicException($"Пароли пользователя {userId} введены некорректно");
            }

            if (!_passwordService.CheckPassword(oldPassword, userId, user.Password))
            {
                throw new BusinessLogicConflictException($"Пользователь {userId} ввёл некорректный старый пароль.");
            }

            var encryptedPassword = _passwordService.EncryptPassword(userId, newPassword);
            var request = new SetNewPasswordRequest { Id = userId, Password = encryptedPassword };
            return await _userApiClient.SetNewPasswordAsync(request, cancellationToken);
        }

        private CreateUserRequest GetCreateUserRequest(RegisterUserModel model)
        {
            var userId = GenerateUserId();
            var result = new CreateUserRequest
            {
                Id = userId,
                Email = model.Email,
                FirstName = model.Name,
                LastName = model.LastName,
                Login = model.Login,
                Password = _passwordService.EncryptPassword(userId, model.Password),
                Phone = model.Phone
            };

            return result;
        }

        private static Guid GenerateUserId()
        {
            using var generator = RandomNumberGenerator.Create();
            var buffer = new byte[16];
            generator.GetBytes(buffer);
            return new Guid(buffer);
        }
    }
}
