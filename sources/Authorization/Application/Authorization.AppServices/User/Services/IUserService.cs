﻿using Authorization.AppServices.User.Models;
using User.Contracts.Responses;

namespace Authorization.AppServices.User.Services
{
    /// <summary>
    /// Сервис по работе с пользователями.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Получение информации о пользователе по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Информация о пользователе.</returns>
        Task<UserInfo> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        /// <summary>
        /// Получение пользователя по логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Информация о пользователе.</returns>
        Task<UserInfo> GetByLoginAsync(string login, CancellationToken cancellationToken);

        /// <summary>
        /// Создание пользователя.
        /// </summary>
        /// <param name="model">Модель создания пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Идентификатор созданного пользователя.</returns>
        Task<Guid> CreateAsync(RegisterUserModel model, CancellationToken cancellationToken);

        /// <summary>
        /// Установка нового пароля для пользователя.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <param name="oldPassword">Старый пароль пользователя.</param>
        /// <param name="newPassword">Новый пароль пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Результат смены пароля пользователя.</returns>
        Task<bool> SetNewPasswordAsync(Guid userId, string oldPassword, string newPassword, CancellationToken cancellationToken);
    }
}
