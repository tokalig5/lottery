﻿using Authorization.AppServices.User.Services;
using Common.Infrastructure.Utils.Extensions;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using System.Security.Claims;
using User.Contracts.Responses;

namespace Authorization.AppServices.IdentityServer.Services
{
    /// <inheritdoc cref="IProfileService"/>
    public class ProfileService : IProfileService
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Инициализация экземпляра <see cref="ProfileService"/>.
        /// </summary>
        public ProfileService(IUserService userService)
        {
            _userService = userService;
        }

        /// <inheritdoc/>
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var subjectId = context.Subject.GetSubjectId();
            var user = await GetUserInfoBySubjectIdAsync(subjectId);

            var claim = new Claim(ClaimTypes.Role, user.Role.GetDescription());
            context.IssuedClaims.Add(claim);
        }

        /// <inheritdoc/>
        public async Task IsActiveAsync(IsActiveContext context)
        {
            var subjectId = context.Subject.GetSubjectId();
            var user = await GetUserInfoBySubjectIdAsync(subjectId);
            context.IsActive = user != null;
        }

        private Task<UserInfo> GetUserInfoBySubjectIdAsync(string subjectId)
        {
            var userId = Guid.Parse(subjectId);
            return _userService.GetByIdAsync(userId, CancellationToken.None);
        }
    }
}
