﻿using Authorization.Contracts.Settings;
using System.Security.Cryptography;
using System.Text;

namespace Authorization.AppServices.Password.Services
{
    /// <inheritdoc cref="IPasswordService"/>
    public class PasswordService : IPasswordService
    {
        private readonly PasswordSettings _passwordSettings;

        /// <summary>
        /// Инициализация экземпляра <see cref="PasswordService"/>.
        /// </summary>        
        public PasswordService(PasswordSettings passwordSettings)
        {
            _passwordSettings = passwordSettings;
        }

        /// <inheritdoc/>
        public Guid EncryptPassword(Guid userId, string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentNullException(nameof(password), $"Отсутствует введённый пароль для пользователя {userId}");
            }

            var encryptedUserId = EncryptUserId(userId);
            var saltedPassword = SaltPassword(encryptedUserId, password);
            return GetSaltedPasswordGuid(saltedPassword);
        }

        /// <inheritdoc/>
        public bool CheckPassword(string enteredPassword, Guid userId, Guid encryptedPassword)
        {
            if (string.IsNullOrWhiteSpace(enteredPassword))
            {
                throw new ArgumentNullException(nameof(enteredPassword), $"Отсутствует введённый пароль для пользователя {userId}");
            }

            var password = EncryptPassword(userId, enteredPassword);
            return encryptedPassword != Guid.Empty && encryptedPassword.Equals(password);
        }

        private static Guid EncryptUserId(Guid userId)
        {
            var bytes = userId.ToByteArray();
            var hash = GetMD5HashBytes(bytes);
            return new Guid(hash);
        }

        private string SaltPassword(Guid encryptedUserId, string password)
        {
            var secret = _passwordSettings.Secret;
            var saltedPassword = string.Concat(password, encryptedUserId, secret);
            return saltedPassword;
        }

        private static Guid GetSaltedPasswordGuid(string saltedPassword)
        {
            var bytes = Encoding.UTF8.GetBytes(saltedPassword);
            var sha256HashBytes = GetSHA256HashBytes(bytes);
            var md5HashBytes = GetMD5HashBytes(sha256HashBytes);
            return new Guid(md5HashBytes);
        }

        private static byte[] GetSHA256HashBytes(byte[] bytes)
        {
            using var hashProvider = SHA256.Create();
            return hashProvider.ComputeHash(bytes);
        }

        private static byte[] GetMD5HashBytes(byte[] bytes)
        {
            using var hashProvider = new MD5CryptoServiceProvider();
            return hashProvider.ComputeHash(bytes);
        }
    }
}
