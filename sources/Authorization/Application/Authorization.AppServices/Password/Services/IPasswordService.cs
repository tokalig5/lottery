﻿namespace Authorization.AppServices.Password.Services
{
    /// <summary>
    /// Сервис по работе с паролями.
    /// </summary>
    public interface IPasswordService
    {
        /// <summary>
        /// Шифрование пароля пользователя.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя, для которого создаётся пароль.</param>
        /// <param name="password">Введённый пароль пользователя.</param>
        /// <returns>Преобразованный пароль.</returns>
        Guid EncryptPassword(Guid userId, string password);

        /// <summary>
        /// Проверка корректности введённого пароля.
        /// </summary>
        /// <param name="enteredPassword">Введённый пароль.</param>
        /// <param name="userId">Идентификатор пользователя, пароль которого проверяется.</param>
        /// <param name="encryptedPassword">Зашифрованный пароль, с которым будет происходить сравнение.</param>
        /// <returns>Результат проверки пароля.</returns>
        bool CheckPassword(string enteredPassword, Guid userId, Guid encryptedPassword);
    }
}
