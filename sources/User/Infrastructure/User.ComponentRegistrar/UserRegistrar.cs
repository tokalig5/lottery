﻿using AutoMapper;
using Common.Contracts.Settings;
using Common.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using User.AppServices.Users.Repositories;
using User.AppServices.Users.Services;
using User.ComponentRegistrar.MapProfiles;
using User.DataAccess;
using User.DataAccess.Repositories;
using User.Handlers.Users.Commands;

namespace User.ComponentRegistrar
{
    /// <summary>
    /// Регистратор сервиса пользователей.
    /// </summary>
    public static class UserRegistrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                           .ConfigureAutoMapper()
                           .ConfigureBusService(configuration)
                           .ConfigureServices()
                           .ConfigureDbConnections(configuration)
                           .ConfigureRepositories()
                           .AddMediator()
                           .AddContextLogger(configuration);
        }

        private static IServiceCollection AddMediator(this IServiceCollection services)
        {
            services.AddMediator(typeof(CreateUserCommand).Assembly);
            return services;
        }

        private static IServiceCollection ConfigureBusService(this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = new RabbitSettings();
            configuration.Bind(nameof(RabbitSettings), rabbitSettings);

            services.AddBusService(rabbitSettings);
            return services;
        }

        private static IServiceCollection ConfigureDbConnections(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbConfiguration<UserContext, UserReadOnlyContext>(configuration);
            return services;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            return services;
        }

        private static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>();
            return services;
        }

        private static IServiceCollection ConfigureAutoMapper(this IServiceCollection services)
        {
            return services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<UserProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}