﻿using AutoMapper;
using System;
using User.Contracts;
using User.Contracts.Requests;
using User.Contracts.Responses;

namespace User.ComponentRegistrar.MapProfiles
{
    /// <summary>
    /// Профиль пользователя.
    /// </summary>
    public class UserProfile : Profile
    {
        /// <summary>
        /// Инициализация экземпляра <see cref="UserProfile"/>.
        /// </summary>
        public UserProfile()
        {
            CreateMap<CreateUserRequest, Entities.User>()
                .ForMember(s => s.Role, map => map.MapFrom(s => UserRoles.Default))
                .ForMember(s => s.PhotoId, map => map.Ignore())
                .ForMember(s => s.IsBlocked, map => map.MapFrom(s => false))
                .ForMember(s => s.Created, map => map.MapFrom(s => DateTime.UtcNow));

            CreateMap<Entities.User, UserInfo>()
                .ForMember(s => s.Name, map => map.MapFrom(s => s.FirstName));
        }
    }
}
