﻿using Microsoft.EntityFrameworkCore;

namespace User.DataAccess
{
    /// <summary>
    /// Контекст чтения-записи БД пользователей.
    /// </summary>
    public class UserContext : UserBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="UserContext"/>.
        /// </summary>
        /// <param name="options">Настройки контекста.</param>
        public UserContext(DbContextOptions<UserContext> options) : base(options)
        {
        }
    }
}
