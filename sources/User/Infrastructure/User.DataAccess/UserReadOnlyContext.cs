﻿using Microsoft.EntityFrameworkCore;

namespace User.DataAccess
{
    /// <summary>
    /// Контекст чтения-записи БД пользователя.
    /// </summary>
    public class UserReadOnlyContext : UserBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="UserReadOnlyContext"/>.
        /// </summary>
        /// <param name="options">Настройки контекста.</param>
        public UserReadOnlyContext(DbContextOptions<UserReadOnlyContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
    }
}
