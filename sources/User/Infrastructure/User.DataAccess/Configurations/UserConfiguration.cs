﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace User.DataAccess.Configurations
{
    /// <summary>
    /// Настройки сущности пользователя.
    /// </summary>
    public class UserConfiguration : IEntityTypeConfiguration<Entities.User>
    {
        /// <inheritdoc/>
        public void Configure(EntityTypeBuilder<Entities.User> builder)
        {
            builder.HasKey(s => s.Id);
            builder.HasIndex(s => s.Login).IsUnique();

            builder.Property(s => s.FirstName).HasMaxLength(250).IsRequired();
            builder.Property(s => s.LastName).HasMaxLength(250).IsRequired();
            builder.Property(s => s.Phone).HasMaxLength(30);
            builder.Property(s => s.Email).HasMaxLength(250).IsRequired();
            builder.Property(s => s.Login).HasMaxLength(250).IsRequired();
            builder.Property(s => s.Created).HasConversion(s => s, s => DateTime.SpecifyKind(s, DateTimeKind.Utc));
        }
    }
}
