﻿using Microsoft.EntityFrameworkCore;
using User.DataAccess.Configurations;

namespace User.DataAccess
{
    public class UserBaseContext : DbContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="UserBaseContext"/>
        /// </summary>
        /// <param name="options"></param>
        public UserBaseContext(DbContextOptions options) : base(options)
        {
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }
}