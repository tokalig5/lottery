﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Common.Contracts.WebApi.Exceptions;
using Common.Infrastructure.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using User.AppServices.Users.Repositories;
using User.Contracts.Responses;

namespace User.DataAccess.Repositories
{
    /// <inheritdoc cref="IUserRepository"/>
    public class UserRepository : IUserRepository
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Entities.User, UserContext> _repository;
        private readonly IReadOnlyRepository<Entities.User, UserReadOnlyContext> _readOnlyRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="UserRepository"/>.
        /// </summary>
        public UserRepository(IRepository<Entities.User, UserContext> repository, IReadOnlyRepository<Entities.User, UserReadOnlyContext> readOnlyRepository, IMapper mapper)
        {
            _repository = repository;
            _readOnlyRepository = readOnlyRepository;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public Task AddAsync(Entities.User model, CancellationToken cancellationToken)
        {
            return _repository.AddAsync(model, cancellationToken);
        }
        
        /// <inheritdoc/>
        public Task UpdateAsync(Entities.User model, CancellationToken cancellationToken)
        {
            return _repository.UpdateAsync(model, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<Entities.User> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var user = await _repository.GetByIdAsync(id, cancellationToken);
            return user ?? throw new NotFoundException($"Пользователь с идентификатором {id} не найден в системе.");
        }

        /// <inheritdoc/>
        public Task<UserInfo> GetInfoByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return _readOnlyRepository.AsQueryable().Where(s => s.Id == id)
                                      .ProjectTo<UserInfo>(_mapper.ConfigurationProvider)
                                      .SingleOrDefaultAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public Task<UserInfo> GetInfoByLoginAsync(string login, CancellationToken cancellationToken)
        {
            return _readOnlyRepository.AsQueryable().Where(s => s.Login == login)
                                      .ProjectTo<UserInfo>(_mapper.ConfigurationProvider)
                                      .SingleOrDefaultAsync(cancellationToken);
        }
    }
}
