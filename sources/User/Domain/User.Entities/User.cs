﻿using Common.Contracts.DataAccess;
using System;
using User.Contracts;

namespace User.Entities
{
    /// <summary>
    /// Пользователь.
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// Имя.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Емейл.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>        
        public Guid Password { get; set; }

        /// <summary>
        /// Роль.
        /// </summary>
        public UserRoles Role { get; set; }

        /// <summary>
        /// Идентификатор фотографии аватара.
        /// </summary>
        public Guid? PhotoId { get; set; }

        /// <summary>
        /// Признак блокировки пользователя.
        /// </summary>
        public bool IsBlocked { get; set; }

        /// <summary>
        /// Время создания.
        /// </summary>
        public DateTime Created { get; set; }
    }
}