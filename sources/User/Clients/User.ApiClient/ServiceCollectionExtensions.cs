﻿using Common.Contracts.Settings;
using Microsoft.Extensions.DependencyInjection;
using System;
using User.ApiClient.Clients;

namespace User.ApiClient
{
    /// <summary>
    /// Методы расширения АПИ клиента сервиса пользователей
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление FinanceApiClient
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="settings">Настройки Proxy</param>
        /// <returns>Коллекция сервисов</returns>
        public static IServiceCollection AddUserApiClient(this IServiceCollection services, ProxySettings settings)
        {
            services.AddHttpClient<IUserApiClient, UserApiClient>(configureClient =>
            {
                configureClient.BaseAddress = new Uri(settings.UserServiceUrl);
            });
            return services;
        }
    }
}
