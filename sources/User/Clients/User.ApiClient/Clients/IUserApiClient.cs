﻿using System;
using System.Threading;
using System.Threading.Tasks;
using User.Contracts.Requests;
using User.Contracts.Responses;

namespace User.ApiClient.Clients
{
    /// <summary>
    /// User service API Client
    /// </summary>
    public interface IUserApiClient
    {
        /// <summary>
        /// Получение информации о пользователе по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Информация о пользователе.</returns>
        Task<UserInfo> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        /// <summary>
        /// Получение информации о пользователе по его логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Информация о пользователе.</returns>
        Task<UserInfo> GetByLoginAsync(string login, CancellationToken cancellationToken);

        /// <summary>
        /// Создание пользователя.
        /// </summary>
        /// <param name="model">Модель создания пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Идентификатор созданного пользователя.</returns>
        Task<Guid> CreateAsync(CreateUserRequest model, CancellationToken cancellationToken);

        /// <summary>
        /// Установка нового пароля для пользователя.
        /// </summary>
        /// <param name="model">Модель установки нового пароля для пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Результат смены пароля пользователя.</returns>
        Task<bool> SetNewPasswordAsync(SetNewPasswordRequest model, CancellationToken cancellationToken);
    }
}