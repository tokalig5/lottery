﻿using Common.Infrastructure.Utils.Extensions;
using Flurl;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using User.ApiClient.Exceptions;
using User.Contracts.Requests;
using User.Contracts.Responses;

namespace User.ApiClient.Clients
{
    /// <inheritdoc cref="IUserApiClient"/>
    public class UserApiClient : IUserApiClient
    {
        private readonly HttpClient _httpClient;

        private const string ApiRoutePrefix = "api/v1";
        private const string UserApiPath = "user";

        /// <summary>
        /// Инициалазация экземпляра <see cref="UserApiClient"/>.
        /// </summary>  
        public UserApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <inheritdoc/>
        public Task<Guid> CreateAsync(CreateUserRequest model, CancellationToken cancellationToken)
        {
            return _httpClient.PostAsync<CreateUserRequest, Guid>(GetUserApiUrl(), model,
            (statusCode, message, exception) => throw new UserApiClientException(statusCode, $"Ошибка создания пользователя с логином {model.Login}. Ошибка: {message}", exception),
            cancellationToken);
        }

        /// <inheritdoc/>
        public Task<UserInfo> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return _httpClient.GetAsync<UserInfo>(GetUserApiUrl(id.ToString()),
            (statusCode, message, exception) => throw new UserApiClientException(statusCode, $"Ошибка получения информации о пользователе с идентификатором {id}. Ошибка: {message}", exception),
            cancellationToken);
        }

        /// <inheritdoc/>
        public Task<UserInfo> GetByLoginAsync(string login, CancellationToken cancellationToken)
        {
            return _httpClient.GetAsync<UserInfo>(GetUserApiUrl("login", login),
            (statusCode, message, exception) => throw new UserApiClientException(statusCode, $"Ошибка получения информации о пользователе с логином {login}. Ошибка: {message}", exception),
            cancellationToken);
        }

        /// <inheritdoc/>
        public Task<bool> SetNewPasswordAsync(SetNewPasswordRequest model, CancellationToken cancellationToken)
        {
            return _httpClient.PostAsync<SetNewPasswordRequest, bool>(GetUserApiUrl("new-password"), model,
            (statusCode, message, exception) => throw new UserApiClientException(statusCode, $"Ошибка замены пароля пользователем {model.Id}. Ошибка: {message}", exception),
            cancellationToken);
        }

        private static string GetUserApiUrl(params string[] parts)
        {
            return Url.Combine(ApiRoutePrefix, UserApiPath, Url.Combine(parts));
        }
    }
}