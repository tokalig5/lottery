﻿using Common.Contracts.WebApi.Exceptions;
using System;
using System.Net;

namespace User.ApiClient.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при некорректной работе UserApiClient
    /// </summary>
    [Serializable]
    public class UserApiClientException : ApiClientException
    {
        /// <summary>
        /// Инициализация экземпляра <see cref="UserApiClientException"/>.
        /// </summary>
        /// <param name="statusCode">HTTP-код ошибки запроса.</param>
        /// <param name="message">Сообщение ошибки запроса.</param>
        /// <param name="innerException">Внутреннее исключение.</param>
        public UserApiClientException(HttpStatusCode statusCode, string message, Exception innerException) : base(statusCode, message, innerException)
        {
        }
    }
}
