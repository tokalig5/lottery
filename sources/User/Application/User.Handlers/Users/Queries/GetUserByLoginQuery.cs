﻿using MediatR;
using User.Contracts.Responses;

namespace User.Handlers.Users.Queries
{
    /// <summary>
    /// Запрос на получение пользователя по его логину.
    /// </summary>
    public class GetUserByLoginQuery : IRequest<UserInfo>
    {
        /// <summary>
        /// Логин пользователя.
        /// </summary>
        public string Login { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="GetUserByLoginQuery"/>.
        /// </summary>
        public GetUserByLoginQuery(string login)
        {
            Login = login;
        }
    }
}
