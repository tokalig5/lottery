﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using User.AppServices.Users.Services;
using User.Contracts.Responses;

namespace User.Handlers.Users.Queries
{
    /// <summary>
    /// Обработчик запроса на получение пользователя по его логину.
    /// </summary>
    public class GetUserByLoginQueryHandler : IRequestHandler<GetUserByLoginQuery, UserInfo>
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Инициализация экземпляра <see cref="GetUserByLoginQueryHandler"/>.
        /// </summary>
        public GetUserByLoginQueryHandler(IUserService userService)
        {
            _userService = userService;
        }

        /// <inheritdoc/>
        public Task<UserInfo> Handle(GetUserByLoginQuery request, CancellationToken cancellationToken)
        {
            return _userService.GetByLoginAsync(request.Login, cancellationToken);
        }
    }
}
