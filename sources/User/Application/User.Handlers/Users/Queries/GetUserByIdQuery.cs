﻿using MediatR;
using System;
using User.Contracts.Responses;

namespace User.Handlers.Users.Queries
{
    /// <summary>
    /// Запрос на получение пользователя по его идентификатору.
    /// </summary>
    public class GetUserByIdQuery : IRequest<UserInfo>
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="GetUserByIdQuery"/>.
        /// </summary>
        public GetUserByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
