﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using User.AppServices.Users.Services;
using User.Contracts.Responses;

namespace User.Handlers.Users.Queries
{
    /// <summary>
    /// Обработчик запроса на получение пользователя по его идентификатору.
    /// </summary>
    public class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, UserInfo>
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Инициализация экземпляра <see cref="GetUserByIdQueryHandler"/>.
        /// </summary>
        public GetUserByIdQueryHandler(IUserService userService)
        {
            _userService = userService;
        }

        /// <inheritdoc/>
        public Task<UserInfo> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
        {
            return _userService.GetByIdAsync(request.Id, cancellationToken);
        }
    }
}
