﻿using MediatR;
using System;
using User.Contracts.Requests;

namespace User.Handlers.Users.Commands
{
    /// <summary>
    /// Запрос на создание пользователя.
    /// </summary>
    public class CreateUserCommand : IRequest<Guid>
    {
        /// <summary>
        /// Модель запроса на создание пользователя.
        /// </summary>
        public CreateUserRequest Model { get; }

        /// <summary>
        /// Инициализация экземпляра <see cref="CreateUserCommand"/>.
        /// </summary>    
        public CreateUserCommand(CreateUserRequest model)
        {
            Model = model;
        }
    }
}
