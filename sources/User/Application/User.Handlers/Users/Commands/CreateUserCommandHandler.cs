﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using User.AppServices.Users.Services;

namespace User.Handlers.Users.Commands
{
    /// <summary>
    /// Обработчик запроса на создание пользователя.
    /// </summary>
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Guid>
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Инициализация экземпляра <see cref="CreateUserCommandHandler"/>.
        /// </summary>
        public CreateUserCommandHandler(IUserService userService)
        {
            _userService = userService;
        }

        /// <inheritdoc/>
        public Task<Guid> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            return _userService.CreateAsync(request.Model, cancellationToken);
        }
    }
}
