﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using User.AppServices.Users.Services;

namespace User.Handlers.Users.Commands
{
    /// <summary>
    /// Обработчик запроса на смену пароля пользователю.
    /// </summary>
    public class SetNewPasswordCommandHandler : IRequestHandler<SetNewPasswordCommand, bool>
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Инициализация экземпляра <see cref="SetNewPasswordCommandHandler"/>.
        /// </summary>
        public SetNewPasswordCommandHandler(IUserService userService)
        {
            _userService = userService;
        }

        /// <inheritdoc/>
        public Task<bool> Handle(SetNewPasswordCommand request, CancellationToken cancellationToken)
        {
            return _userService.SetNewPasswordAsync(request.Model, cancellationToken);
        }
    }
}
