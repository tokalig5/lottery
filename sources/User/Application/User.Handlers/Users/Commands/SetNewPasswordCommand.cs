﻿using MediatR;
using User.Contracts.Requests;

namespace User.Handlers.Users.Commands
{
    /// <summary>
    /// Запрос на смену пароля пользователя. 
    /// </summary>
    public class SetNewPasswordCommand : IRequest<bool>
    {
        /// <summary>
        /// Модель запроса на смену пароля пользователя.
        /// </summary>
        public SetNewPasswordRequest Model { get; }


        /// <summary>
        /// Инициализация экземпляра <see cref="SetNewPasswordCommand"/>.
        /// </summary>        
        public SetNewPasswordCommand(SetNewPasswordRequest model)
        {
            Model = model;
        }
    }
}
