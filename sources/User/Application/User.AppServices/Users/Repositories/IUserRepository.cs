﻿using System;
using System.Threading;
using System.Threading.Tasks;
using User.Contracts.Responses;

namespace User.AppServices.Users.Repositories
{
    /// <summary>
    /// Репозиторий для работы с пользователями.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Получение сущности по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор сущности.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Сущность пользователя.</returns>
        Task<Entities.User> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        /// <summary>
        /// Получение информации о пользователе по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Информация о пользователе.</returns>
        Task<UserInfo> GetInfoByIdAsync(Guid id, CancellationToken cancellationToken);

        /// <summary>
        /// Получение информации о пользователе по его логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Информация о пользователе.</returns>
        Task<UserInfo> GetInfoByLoginAsync(string login, CancellationToken cancellationToken);

        /// <summary>
        /// Создание пользователя.
        /// </summary>
        /// <param name="model">Модель пользователя</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        Task AddAsync(Entities.User model, CancellationToken cancellationToken);

        /// <summary>
        /// Обновление пользователя.
        /// </summary>
        /// <param name="model">Модель пользователя</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        Task UpdateAsync(Entities.User model, CancellationToken cancellationToken);
    }
}
