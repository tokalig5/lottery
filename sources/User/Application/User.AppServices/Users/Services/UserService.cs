﻿using AutoMapper;
using System;
using System.Threading;
using System.Threading.Tasks;
using User.AppServices.Users.Repositories;
using User.Contracts.Requests;
using User.Contracts.Responses;

namespace User.AppServices.Users.Services
{
    /// <inheritdoc cref="IUserService"/>
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        /// <summary>
        /// Инициализация экземпляра <see cref="UserService"/>.
        /// </summary>
        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task<Guid> CreateAsync(CreateUserRequest model, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<CreateUserRequest, Entities.User>(model);
            await _userRepository.AddAsync(user, cancellationToken);
            return user.Id;
        }

        /// <inheritdoc/>
        public Task<UserInfo> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return _userRepository.GetInfoByIdAsync(id, cancellationToken);
        }

        /// <inheritdoc/>
        public Task<UserInfo> GetByLoginAsync(string login, CancellationToken cancellationToken)
        {
            return _userRepository.GetInfoByLoginAsync(login, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<bool> SetNewPasswordAsync(SetNewPasswordRequest model, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetByIdAsync(model.Id, cancellationToken);
            user.Password = model.Password;

            await _userRepository.UpdateAsync(user, cancellationToken);
            return true;
        }
    }
}
