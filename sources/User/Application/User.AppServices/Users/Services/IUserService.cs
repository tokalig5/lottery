﻿using System;
using System.Threading;
using System.Threading.Tasks;
using User.Contracts.Requests;
using User.Contracts.Responses;

namespace User.AppServices.Users.Services
{
    /// <summary>
    /// Сервис для работы с пользователями.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Получение пользователя по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Информация о пользователе.</returns>
        Task<UserInfo> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        /// <summary>
        /// Получение пользователя по его логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Информация о пользователе.</returns>
        Task<UserInfo> GetByLoginAsync(string login, CancellationToken cancellationToken);

        /// <summary>
        /// Установка нового пароля пользователя.
        /// </summary>
        /// <param name="model">Модель установки нового пароля пользователю</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Результат смены пароля.</returns>
        Task<bool> SetNewPasswordAsync(SetNewPasswordRequest model, CancellationToken cancellationToken);

        /// <summary>
        /// Создание пользователя.
        /// </summary>
        /// <param name="model">Модель создания пользователя</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <returns>Идентификатор созданного пользователя.</returns>
        Task<Guid> CreateAsync(CreateUserRequest model, CancellationToken cancellationToken);
    }
}
