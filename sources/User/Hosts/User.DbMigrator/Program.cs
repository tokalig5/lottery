using User.DbMigrator;
using Microsoft.EntityFrameworkCore;

public class Program
{
    public static async Task Main(string[] args)
    {
        var host = Host.CreateDefaultBuilder(args).ConfigureServices((hostContext, services) =>
        {
            services.AddServices(hostContext.Configuration);
        }).Build();
        await MigrateDataBaseAsync(host.Services);
        await host.RunAsync();
    }

    private static async Task MigrateDataBaseAsync(IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();
        var context = scope.ServiceProvider.GetService<MigrationDbContext>();
        await context!.Database.MigrateAsync();
    }
}