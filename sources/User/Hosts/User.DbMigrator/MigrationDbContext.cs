﻿using Microsoft.EntityFrameworkCore;
using User.DataAccess;

namespace User.DbMigrator
{
    /// <summary>
    /// Контекст для миграций.
    /// </summary>
    public class MigrationDbContext : UserBaseContext
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="MigrationDbContext"/>.
        /// </summary>
        public MigrationDbContext(DbContextOptions<MigrationDbContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
    }
}
