﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using User.Contracts.Requests;
using User.Handlers.Users.Commands;
using User.Handlers.Users.Queries;

namespace User.Api.Controllers
{
    /// <summary>
    /// User controller.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Инициализация экземпляра <see cref="UserController"/>.
        /// </summary>
        public UserController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Создание пользователя.
        /// </summary>
        /// <param name="model">Модель создания пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        [HttpPost]
        public async Task<IActionResult> Create([Required] CreateUserRequest model, CancellationToken cancellationToken)
        {
            var command = new CreateUserCommand(model);
            var result = await _mediator.Send(command, cancellationToken);
            return StatusCode((int)HttpStatusCode.Created, result);
        }

        /// <summary>
        /// Получение информации о пользователе по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById([Required] Guid id, CancellationToken cancellationToken)
        {
            var query = new GetUserByIdQuery(id);
            var result = await _mediator.Send(query, cancellationToken);
            return Ok(result);
        }

        /// <summary>
        /// Получение информации о пользователе по логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        [HttpGet("login/{login}")]
        public async Task<IActionResult> GetByLogin([Required] string login, CancellationToken cancellationToken)
        {
            var query = new GetUserByLoginQuery(login);
            var result = await _mediator.Send(query, cancellationToken);
            return Ok(result);
        }

        /// <summary>
        /// Установка нового пароля.
        /// </summary>
        /// <param name="model">Модель установки нового пароля.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        [HttpPost("new-password")]
        public async Task<IActionResult> SetNewPassword([Required] SetNewPasswordRequest model, CancellationToken cancellationToken)
        {
            var command = new SetNewPasswordCommand(model);
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
    }
}