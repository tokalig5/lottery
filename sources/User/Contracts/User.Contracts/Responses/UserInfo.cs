﻿using System;

namespace User.Contracts.Responses
{
    /// <summary>
    /// Информация о пользователе.
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Логин пользователя.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public Guid Password { get; set; }

        /// <summary>
        /// Роль пользователя.
        /// </summary>
        public UserRoles Role { get; set; }

        /// <summary>
        /// Признак блокировки пользователя.
        /// </summary>
        public bool IsBlocked { get; set; }
    }
}
