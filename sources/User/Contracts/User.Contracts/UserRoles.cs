﻿using System.ComponentModel;

namespace User.Contracts
{
    /// <summary>
    /// Роли пользователя.
    /// </summary>
    public enum UserRoles
    {
        /// <summary>
        /// Роль по умолчанию (обычный пользователь).
        /// </summary>
        [Description(UserRoleNames.Default)]
        Default = 0,

        /// <summary>
        /// Администратор.
        /// </summary>
        [Description(UserRoleNames.Admin)]
        Admin = 1
    }
}
