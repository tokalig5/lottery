﻿using System;

namespace User.Contracts.Requests
{
    /// <summary>
    /// Модедль запроса на создание пользователя.
    /// </summary>
    public class CreateUserRequest
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Емейл.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>        
        public Guid Password { get; set; }
    }
}
