﻿using System;

namespace User.Contracts.Requests
{
    /// <summary>
    /// Модель запроса на установку нового пароля пользователю.
    /// </summary>
    public class SetNewPasswordRequest
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public Guid Password { get; set; }
    }
}
