﻿namespace User.Contracts
{
    /// <summary>
    /// Наименования ролей.
    /// </summary>
    public static class UserRoleNames
    {
        /// <summary>
        /// Роль по умолчанию (обычный пользователь).
        /// </summary>
        public const string Default = nameof(Default);

        /// <summary>
        /// Администратор.
        /// </summary>
        public const string Admin = nameof(Admin);
    }
}
